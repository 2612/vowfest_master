(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/account/account.component.css":
/*!***********************************************!*\
  !*** ./src/app/account/account.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.jumbotron{\r\n    background-color: transparent;\r\n    padding-bottom: 0%;\r\n}\r\nhr{\r\n    width:10%;\r\n    background-color: brown;\r\n}\r\n.jumbo{\r\n    margin-top: 25%;\r\n}\r\n.fa-plus-circle{\r\n    color: orange;\r\n    font-size: 20px;\r\n}\r\n.topnav {\r\noverflow: hidden;\r\nmargin-left: 5%;\r\ntext-align: center;\r\n}\r\n.topnav a {\r\nfloat: left;\r\ndisplay: block;\r\ncolor: black;\r\ntext-align: center;\r\npadding: 14px 16px;\r\ntext-decoration: none;\r\nfont-size: 17px;\r\nborder-bottom: 3px solid transparent;\r\n}\r\n.topnav a:hover {\r\nborder-bottom: 3px solid red;\r\n}\r\n.topnav a.active {\r\nborder-bottom: 3px solid red;\r\n}\r\n.jumbotron_text{\r\n    margin-top: 50px;\r\n    padding: 0;\r\n}\r\n.user_mail{\r\n    color: #ccc;\r\n    font-size: 18px;\r\n    font-weight: 800;\r\n    border-bottom: 1px solid #ccc;\r\n    padding-bottom: 30px;\r\n}\r\n.user_mail a{\r\n    font-size: 12px;\r\n    color: #ccc;\r\n}\r\n.manager_box{\r\n    margin: 30px 0px;\r\n    border-bottom: 1px solid #ccc;\r\n    padding-bottom: 15px;\r\n}\r\n.manager_box img{\r\n    border-radius: 100px;\r\n}\r\n.manager_box .name{\r\n    font-size: 20px;\r\n    margin-bottom: 0;\r\n}\r\n.manager_box .add{\r\n    color: #ccc;\r\n}\r\nhr{\r\n    width: 100%;\r\n    background-color: #ccc\r\n}"

/***/ }),

/***/ "./src/app/account/account.component.html":
/*!************************************************!*\
  !*** ./src/app/account/account.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-12\">\r\n      <app-navbar></app-navbar>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"topnav\">\r\n      <a routerLink=\"/invitation\" [ngClass] = \"{ 'inactive': !shouldShow_invi }\">Invitation</a>\r\n      <a routerLink=\"/guest-list\" [ngClass] = \"{ 'inactive': !shouldShow_guest }\">Guest list</a>\r\n      <a routerLink=\"/account\"    [ngClass] = \"{ 'active'  : shouldShow_account, \r\n                                                 'inactive': !shouldShow_account  }\">Account</a>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-12\">\r\n      <div class=\"jumbotron_text\">\r\n          <p style=\"color:orange\">The couple  \r\n          <img src=\"../../assets/edit.png\"  style=\"float:right\" alt=\"\">\r\n              <!-- <i class=\"fa fa-plus-circle\" style=\"float:right\" aria-hidden=\"true\"></i>--> </p>\r\n          <p class=\"user_mail\">Natasha sharma <br><a href=\"mailto:test@gmail.com?Subject=Hello%20again\" target=\"_top\"> test@gmail.com </a> </p>\r\n          <p style=\"color:rgb(124, 195, 243);margin-top: 30px;text-align: center;\">\r\n              <i class=\"fa fa-camera\" aria-hidden=\"true\"></i>\r\n              Choose a VOWfest managers \r\n          </p>   \r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-12\">\r\n      <hr>\r\n    </div>\r\n  </div>    \r\n  <!-- <div class=\"row manager_box\">\r\n    <div class=\"col-2 nopadd\">\r\n      <img src=\"../../assets/user.jpeg\" alt=\"\">\r\n    </div>\r\n    <div class=\"col-4 \">\r\n      <p class=\"name\">Natasha </p>\r\n      <p class=\"add\">Indore</p>\r\n    </div>\r\n    <div class=\"col-6 text-right nopadd\">\r\n      <p style=\"color:blue\">Quote requested</p>\r\n    </div>\r\n  </div> -->\r\n  <p class=\"text-center\" style=\"color:orange\" > \r\n    <i class=\"fas fa-plus-circle\"  (click)=addpic()></i> \r\n    Add your own event manager \r\n  </p> \r\n</div>\r\n"

/***/ }),

/***/ "./src/app/account/account.component.ts":
/*!**********************************************!*\
  !*** ./src/app/account/account.component.ts ***!
  \**********************************************/
/*! exports provided: AccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountComponent", function() { return AccountComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AccountComponent = /** @class */ (function () {
    function AccountComponent() {
        this.shouldShow_invi = true;
        this.shouldShow_guest = true;
        this.shouldShow_account = true;
    }
    AccountComponent.prototype.ngOnInit = function () {
    };
    AccountComponent.prototype.addpic = function () { };
    AccountComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-account',
            template: __webpack_require__(/*! ./account.component.html */ "./src/app/account/account.component.html"),
            styles: [__webpack_require__(/*! ./account.component.css */ "./src/app/account/account.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AccountComponent);
    return AccountComponent;
}());



/***/ }),

/***/ "./src/app/add-event/add-event.component.css":
/*!***************************************************!*\
  !*** ./src/app/add-event/add-event.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".arrow{\r\n    float: left;\r\n    font-size: 30px;\r\n    line-height: 80px;\r\n}\r\n.button{\r\n    float: right;\r\n    height: 40px;\r\n    margin-top: 2vh;\r\n    width: 100px;\r\n    background-color: orange;\r\n    text-align: center;\r\n    color: white;\r\n    line-height: 40px;\r\n}\r\n.button:active,.fa-arrow-left:active,.fa-plus-circle:active{\r\n    text-decoration: none;\r\n    color: orange;\r\n}\r\n.jumbotron{\r\n    background-color: transparent;\r\n}\r\n.fa-arrow-left{\r\n    color: orange;\r\n}\r\n.fa-plus-circle{\r\n    color: orange;\r\n    font-size: 30px;\r\n    margin-left: 3%;\r\n}\r\n#program,#time{\r\n    width: 100%;\r\n}\r\nbutton{\r\n    background-color: transparent;\r\n    outline: none;\r\n    border: none;\r\n}\r\n"

/***/ }),

/***/ "./src/app/add-event/add-event.component.html":
/*!****************************************************!*\
  !*** ./src/app/add-event/add-event.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\" style=\"height:80px;\">\r\n    <div class=\"col-3 arrow\">\r\n\r\n        <a routerLink = \"/event-list\">\r\n          <i class=\"fa fa-arrow-left\"></i>\r\n        </a>\r\n    </div>\r\n    <div class=\"col-6\"></div>\r\n    <div class=\"col-3\">\r\n      <button (click)=addevent() class=\"button\" type=\"submit\">Done</button> &nbsp;&nbsp;  \r\n    </div>\r\n  </div>\r\n  <div class=\"row\" style=\"height:auto;\">\r\n    <div class=\"jumbotron ml-auto mr-auto col-12\">\r\n      <form >\r\n        <div class=\"form-group row\">\r\n          <div class=\"col-12\">\r\n            <input type=\"text\" class=\"form-control\" id=\"Inputname\" aria-describedby=\"emailHelp\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Event name\" [(ngModel)]=\"eve['name']\">\r\n          </div>\r\n        </div>\r\n        <div class=\"form-group row\">\r\n          <div class=\"col-12\">\r\n            <input type=\"date\" class=\"form-control\" placeholder=\"Event date\" [(ngModel)]=\"eve['date']\" [ngModelOptions]=\"{standalone: true}\">\r\n          </div>\r\n        </div>\r\n        <div class=\"form-group row\">\r\n          <div class=\"col-12\">\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Venue\" [(ngModel)]=\"eve['location']\" [ngModelOptions]=\"{standalone: true}\">\r\n          </div>\r\n        </div>\r\n        <div class=\"form-group row\">\r\n          <div class=\"col-4\">\r\n            <input type=\"text\" class=\"form-control\" id=\"program\" placeholder=\"Programme\" [(ngModel)]=\"eve.programe[0].name\" [ngModelOptions]=\"{standalone: true}\">\r\n          </div>\r\n          <div class=\"col-4\">\r\n            <input type=\"time\" class=\"form-control\" id=\"time\" placeholder=\"Time\" [(ngModel)]=\"eve.programe[0].time\" [ngModelOptions]=\"{standalone: true}\">\r\n          </div>\r\n          <div class=\"col-4 \">\r\n            <button (click)=addprogram() data-toggle=\"collapse\" data-target=\"#program\" class=\"fas fa-plus-circle\"></button>\r\n          </div>\r\n        </div>\r\n        <div class=\"form-group row\" *ngFor=\"let item of programe_array; let i = index \">\r\n          <div class=\"col-5\">\r\n            <input type=\"text\" class=\"form-control\" id=\"program+{{i}}\" placeholder=\"Programme\" [(ngModel)]=\"item.name\" [ngModelOptions]=\"{standalone: true}\">\r\n          </div>\r\n          <div class=\"col-5\">\r\n            <input type=\"time\" class=\"form-control\" id=\"time+{{i}}\" placeholder=\"Time\" [(ngModel)]=\"item.time\" [ngModelOptions]=\"{standalone: true}\">\r\n          </div>\r\n        </div>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/add-event/add-event.component.ts":
/*!**************************************************!*\
  !*** ./src/app/add-event/add-event.component.ts ***!
  \**************************************************/
/*! exports provided: AddEventComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddEventComponent", function() { return AddEventComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddEventComponent = /** @class */ (function () {
    function AddEventComponent(http, router) {
        var _this = this;
        this.http = http;
        this.router = router;
        this.EventArray = [];
        this.eve = { programe: [{ name: '', time: '' }] };
        this.data1 = [];
        this.add = [];
        this.count = 1;
        this.add_programe = { name: '', time: '' };
        this.programe_array = [];
        this.addprogram = function () {
            console.log(_this.count);
            for (var _i = 0; _i < _this.count; _i++) {
                _this.programe_array.push(_this.add_programe);
                console.log(_this.programe_array);
                _this.eve.programe.push(_this.add_programe);
                console.log(_this.eve);
            }
        };
    }
    AddEventComponent.prototype.ngOnInit = function () {
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        if (!authToken) {
            this.router.navigate(['../login']);
        }
    };
    AddEventComponent.prototype.addevent = function () {
        var _this = this;
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_wedding_id');
        var headers = new Headers();
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Authorization': authToken });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', authToken);
        console.log(this.eve);
        this.http.post('https://api.vowfest.com/event/add', this.eve, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) { return response; })).subscribe(function (data) {
            console.log("DATA: ", data);
            _this.data1.push(data);
            _this.router.navigate(['../event-list']);
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    AddEventComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-event',
            template: __webpack_require__(/*! ./add-event.component.html */ "./src/app/add-event/add-event.component.html"),
            styles: [__webpack_require__(/*! ./add-event.component.css */ "./src/app/add-event/add-event.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AddEventComponent);
    return AddEventComponent;
}());



/***/ }),

/***/ "./src/app/add-stories/add-stories.component.css":
/*!*******************************************************!*\
  !*** ./src/app/add-stories/add-stories.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n    background: black;\n}\n.text-line {\n    background-color: transparent;\n    color: black;\n    outline: none;\n    outline-style: none;\n    width: 80%;\n    outline-offset: 0;\n    margin-top: 50px;\n    border-top: none;\n    border-left: none;\n    border-right: none;\n    border-bottom: solid #ff1556 1px;\n    padding: 3px 10px;\n}\n.image_size{\n    height: 100%;\n    width: 100%;\n}\n.fa-arrow-left,.fa-arrow-left:active{\n    color: orange;\n    text-decoration: none;\n    font-size: 20px;\n    line-height: 50px;\n    }\nh2{\n    font-weight: bold;\n    margin-top: 10px;\n}\n#done{\n    background-color: #ff1556;\n    color: white;\n    border: #ff1556;\n    width: 100%;\n    height: 50px;\n    font-size: 25px;\n  \n    position: absolute;\n    bottom: 0px;\n    vertical-align: middle;\n    left: 0px;\n    text-decoration: none;\n}\n#spinner {\n\t-webkit-animation: frames 1s infinite linear;\n\tanimation: frames 1s infinite linear;\n\tbackground: transparent;\n\tborder: 1.75vw solid #FFF;\n\tborder-radius: 100%;\n\tborder-top-color: #DF691A;\n\twidth: 20vw;\n\theight: 20vw;\n\topacity: .6;\n\tpadding: 0;\n\tposition: absolute;\n\tz-index: 999;\n}\n@-webkit-keyframes frames {\n  0% {\n\t-webkit-transform: rotate(0deg);\n\ttransform: rotate(0deg);\n  }\n  100% {\n\t-webkit-transform: rotate(359deg);\n\ttransform: rotate(359deg);\n  }\n}\n@keyframes frames {\n  0% {\n\t-webkit-transform: rotate(0deg);\n\ttransform: rotate(0deg);\n  }\n  100% {\n\t-webkit-transform: rotate(359deg);\n\ttransform: rotate(359deg);\n  }\n}\n#pause {\n\tdisplay: block;\n\tbackground:\n\t\trgba(0, 0, 0, 0.66)\n\t\tno-repeat\n\t\t0 0;\n\twidth: 100%;\n\theight: 100%;\n\tposition: fixed;\n\tbottom: 0;\n\tleft: 0;\n\tz-index: 1000;\n}\n"

/***/ }),

/***/ "./src/app/add-stories/add-stories.component.html":
/*!********************************************************!*\
  !*** ./src/app/add-stories/add-stories.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-2\">\n      <a routerLink=\"/stories\" class=\"fa fa-arrow-left\"></a>\n    </div>\n    <div class=\"col-10\">\n      <h2 align= \"center\">\n        New Story\n      </h2>\n    </div>\n  </div>\n \n\n  <div class=\"row \">\n    <div class=\"col-12\">\n      <img src={{story_data.base64}} class=\"image_size\" >\n    </div>\n  </div>\n  <div  class=\"row \">\n    <div class=\"col-12\" align=\"center\">\n      <input type=\"text\" placeholder=\"Tell the story behind the picture\" class=\"text-line\" [(ngModel)]=\"story.story\"/>\n    </div>\n  </div>\n  <div class=\"row \" style=\"margin-bottom: 38%;\">\n    <div class=\"col-12\"  align=\"center\">\n      <input type=\"text\" placeholder=\"Tag people from your guestlist\" class=\"text-line\"/> \n    </div>\n  </div>\n  <div  class=\"row \" >\n    <div class=\"col-12\" >\n      <a type=\"button\" (click)=addstory() id=\"done\" align=\"center\"  >PUBLISH</a>\n    </div>\n  </div>\n\t<div id=\"pause\" class=\"d-flex align-items-center justify-content-center\" *ngIf = \"spinner == true\">\n    <div id=\"spinner\"></div>\n  </div>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/add-stories/add-stories.component.ts":
/*!******************************************************!*\
  !*** ./src/app/add-stories/add-stories.component.ts ***!
  \******************************************************/
/*! exports provided: AddStoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddStoriesComponent", function() { return AddStoriesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _camera_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../camera.service */ "./src/app/camera.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddStoriesComponent = /** @class */ (function () {
    function AddStoriesComponent(http, router, activatedroute, camera) {
        this.http = http;
        this.router = router;
        this.activatedroute = activatedroute;
        this.camera = camera;
        this.spinner = false;
        this.image = { file: {}, base64: '' };
        this.story_data = {};
        this.story = { photo_data: '', wedding_id: '', story: '' };
        this.formData = new FormData();
    }
    AddStoriesComponent.prototype.ngOnInit = function () {
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': authToken,
            'Access-Control-Allow-Origin': '*'
        });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: this.headers });
        this.wid = localStorage.getItem("weddingID") || localStorage.getItem("guest_wedding_id");
        this.story.wedding_id = this.wid;
        this.story_data = this.camera.getimage();
        console.log("FILE: ", this.story_data);
    };
    AddStoriesComponent.prototype.addstory = function () {
        this.formData.append("photo_data", this.story_data.file);
        this.formData.append("wedding_id", this.wid);
        this.formData.append("story", this.story.story);
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.api();
        this.spinner = true;
    };
    AddStoriesComponent.prototype.api = function () {
        var _this = this;
        console.log(this.formData);
        this.http.post('https://api.vowfest.com/story/create', this.formData, this.options).subscribe(function (res) {
            console.log(res);
            if (localStorage.getItem('guest_invites') == 'true') {
                _this.router.navigate(['../stories']);
                _this.spinner = false;
            }
            else {
                if (localStorage.getItem('login_type') == 'guest') {
                    _this.router.navigate(['../Guest_Dashboard']);
                }
                else {
                    _this.router.navigate(['../dashboard']);
                }
            }
        }, function (error) {
            console.log(error);
        });
    };
    AddStoriesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-stories',
            template: __webpack_require__(/*! ./add-stories.component.html */ "./src/app/add-stories/add-stories.component.html"),
            styles: [__webpack_require__(/*! ./add-stories.component.css */ "./src/app/add-stories/add-stories.component.css")],
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _camera_service__WEBPACK_IMPORTED_MODULE_4__["CameraService"]])
    ], AddStoriesComponent);
    return AddStoriesComponent;
}());



/***/ }),

/***/ "./src/app/allpost/allpost.component.html":
/*!************************************************!*\
  !*** ./src/app/allpost/allpost.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-3 arrow\">\r\n        <a routerLink=\"/stories\" class=\"fa fa-arrow-left\"> Back </a>\r\n    </div>\r\n</div>\r\n    <!-- <div *ngIf = \"image_data.status == 'true'\"> -->\r\n        <div  *ngFor=\"let i of image_data2 ;let i=index\">\r\n            <div class=\"row col-12 propic\">\r\n                    <div class=\"col-2\">\r\n                        <img src=\"{{i.userphoto}}\" alt=\"\" srcset=\"\" id=\"pro\">\r\n                        \r\n                    </div>\r\n                    <div class=\"col-8 para\">\r\n                        <p> <b> {{i.username}}'s </b> added a story - \"{{i.story}}\"</p>\r\n                    </div>\r\n                    <div class=\"col-2 icon\">\r\n                        <div class=\"col-1 share\">\r\n                            <a class=\"\"><img src=\"../../assets/share.png\" alt=\"\"></a>\r\n                        </div>\r\n                        <div class=\"col-1 w3-dropdown-hover select\">\r\n                            <!-- <button class=\"w3-button\">&#8942;</button> -->\r\n                            <a><img src=\"../../assets/select.png\"></a>\r\n                            <div class=\"w3-dropdown-content w3-bar-block w3-border w3-card-4\">\r\n                                <a class=\"w3-bar-item w3-button\">Edit</a>\r\n                                <a  class=\"w3-bar-item w3-button\">Delete</a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div  class=\" col-12\">\r\n                    <img class=\"col-12\" src=\"{{i.photo_data}}\" style=\"max-width:100%;height:auto;\"/>   \r\n                </div>\r\n                <div class=\"row col-12 displaycards\">\r\n                    <div class=\"row col-12 \">\r\n                        <div class=\"col-8\">\r\n                            <p  class=\"comment_P\"> {{i.story}}</p>\r\n                        </div>     \r\n                        <div class=\"col-4 text-right\">\r\n                            <i class=\"far fa-heart \" style=\"color:deeppink;\" (click)=\"setlike(i.photo_id)\"> <span>  {{i.num_likes}}</span></i>\r\n                            <i class=\"far fa-comment msgicon_top_story\" style=\"color:orange;\"  (click) = \"viewcomment(i.photo_id)\"  > <span> {{i.num_comments}}</span></i>                               \r\n                                \r\n                        </div>     \r\n                    \r\n                    </div>\r\n                    <div class=\"row col-12 \">\r\n                        <br>\r\n                        <p class=\"small comment_P\"> {{i.time | date: 'medium'}} </p>\r\n                    </div>\r\n                    <div class=\"col-12 comment_div\" *ngIf=\"comment_div == true && divid == i.photo_id\">\r\n                            <ul *ngFor=\"let comment of i.comments \" class=\"col-12\">\r\n                                <li class=\"col-12\">  <strong> {{comment.username}}</strong> just commented  </li>\r\n                                <li class=\"col-12\">  <p class=\"small\" style=\"margin-left:0%\">  {{comment.time | date}}</p>  </li>\r\n                                <li class=\"col-12\">  <p style=\"padding-bottom: 1%;\"> {{comment.body}}</p></li>\r\n                                <i class=\"fa fa-thumbs-up\" aria-hidden=\"true\" (click) = \" add_counts_of_like(comment.comment_id)\"></i>\r\n                                {{comment.num_likes}}\r\n\r\n                                <li class=\"col-12\">  <hr> </li> \r\n                            </ul>\r\n                            <br>\r\n                            <form class=\"row col-12\" (ngSubmit)=\"add_comment(com)\" #com=\"ngForm\">                        \r\n                                <div class=\"col-8 form-group\">\r\n                                    <input type=\"hidden\"  name = \"photo_id\" id=\"photo_id\" [(ngModel)]=\"i.photo_id\" />\r\n                                    <input type=\"text\" class=\"form-control comment_input\" name = \"body\" id=\"Inputname\" [(ngModel)]=\"body\" />\r\n                                </div>\r\n                                <div class=\"col-4 form-group comment_button\">\r\n                                    \r\n                                    <button type=\"submit\" class=\"btn btn-primary small\">comment</button>\r\n                                </div>    \r\n                            </form> \r\n                        </div>                     \r\n                </div>\r\n                    \r\n        </div>\r\n    <!-- </div>     -->\r\n    <div class=\"clearfix\"></div>"

/***/ }),

/***/ "./src/app/allpost/allpost.component.ts":
/*!**********************************************!*\
  !*** ./src/app/allpost/allpost.component.ts ***!
  \**********************************************/
/*! exports provided: AllpostComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllpostComponent", function() { return AllpostComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AllpostComponent = /** @class */ (function () {
    function AllpostComponent(http, router) {
        this.http = http;
        this.router = router;
        this.comment = { num_likes: '' };
        this.id = { photo_id: '' };
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
    }
    AllpostComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.comment_div = false;
        this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        this.http.post('https://api.vowfest.com/story/list', { 'wedding_id': this.wid }, { headers: header }).subscribe(function (data) {
            console.log('DATA RECIEVED: ', data);
            _this.image_data = data;
            _this.image_data2 = _this.image_data.stories_list;
            console.log(_this.image_data2);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    AllpostComponent.prototype.setlike = function (x) {
        var _this = this;
        this.id.photo_id = x;
        this.http.post("https://api.vowfest.com/photo/like/add", this.id, this.options).subscribe(function (data) {
            _this.getstory_list();
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    AllpostComponent.prototype.add_comment = function (c) {
        var _this = this;
        console.log(c.value);
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/add", c.value, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.getstory_list();
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    AllpostComponent.prototype.viewcomment = function (x) {
        var _this = this;
        if (this.comment_div == false) {
            this.comment_div = true;
        }
        else {
            this.comment_div = false;
        }
        this.divid = x;
        this.http.post('https://api.vowfest.com/photo/comment/list', { photo_id: x }).subscribe(function (data) {
            _this.viewwcomments = data;
            console.log(_this.viewwcomments);
            _this.com = _this.viewwcomments.comments;
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    AllpostComponent.prototype.getstory_list = function () {
        var _this = this;
        this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        this.http.post('https://api.vowfest.com/story/list', { 'wedding_id': this.wid }, { headers: header }).subscribe(function (data) {
            console.log('DATA RECIEVED: ', data);
            _this.image_data = data;
            if (_this.image_data.status == true) {
                _this.image_data2 = _this.image_data.stories_list;
                console.log(_this.image_data2);
                if (_this.image_data2) {
                    for (var _i = 0, _a = _this.image_data2; _i < _a.length; _i++) {
                        var comment_array = _a[_i];
                        console.log(comment_array.comments.length);
                        if (comment_array.comments.length == 0) {
                            _this.comment_div = false;
                        }
                        else {
                            _this.comment_div = true;
                        }
                    }
                }
            }
        }, function (error) {
            console.log(error);
            //  this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    AllpostComponent.prototype.add_counts_of_like = function (x) {
        var _this = this;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/like/add", { comment_id: x }, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.getstory_list();
            //this.view_counts_of_like(x);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    AllpostComponent.prototype.view_counts_of_like = function (x) {
        var _this = this;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/like/list", { comment_id: x }, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.comment_like = data;
            _this.comment.num_likes = _this.comment_like.num_likes;
            console.log(_this.comment.num_likes);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    AllpostComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-allpost',
            template: __webpack_require__(/*! ./allpost.component.html */ "./src/app/allpost/allpost.component.html"),
            styles: [__webpack_require__(/*! ../stories/stories.component.css */ "./src/app/stories/stories.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AllpostComponent);
    return AllpostComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<router-outlet></router-outlet>\r\n\r\n\r\n\r\n\r\n<!-- <div class=\"snapshot\" >\r\n\t<img [src]=\"webcamImage.imageAsDataUrl\" />\r\n</div> -->"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css"), __webpack_require__(/*! ../../node_modules/bootstrap/dist/css/bootstrap.min.css */ "./node_modules/bootstrap/dist/css/bootstrap.min.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: getAuthServiceConfigs, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAuthServiceConfigs", function() { return getAuthServiceConfigs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_font_awesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-font-awesome */ "./node_modules/angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_sharebuttons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-sharebuttons */ "./node_modules/ngx-sharebuttons/ngx-sharebuttons.es5.js");
/* harmony import */ var angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-6-social-login-v2 */ "./node_modules/angular-6-social-login-v2/angular-6-social-login-v2.umd.js");
/* harmony import */ var angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./calendar/calendar.component */ "./src/app/calendar/calendar.component.ts");
/* harmony import */ var _gift_gift_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./gift/gift.component */ "./src/app/gift/gift.component.ts");
/* harmony import */ var _guestlist_guestlist_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./guestlist/guestlist.component */ "./src/app/guestlist/guestlist.component.ts");
/* harmony import */ var _invitation_invitation_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./invitation/invitation.component */ "./src/app/invitation/invitation.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _photo_photo_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./photo/photo.component */ "./src/app/photo/photo.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _engagement_engagement_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./engagement/engagement.component */ "./src/app/engagement/engagement.component.ts");
/* harmony import */ var _grid_gallery_grid_gallery_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./grid-gallery/grid-gallery.component */ "./src/app/grid-gallery/grid-gallery.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var _gift_item_gift_item_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./gift-item/gift-item.component */ "./src/app/gift-item/gift-item.component.ts");
/* harmony import */ var _gift_wishlist_gift_wishlist_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./gift-wishlist/gift-wishlist.component */ "./src/app/gift-wishlist/gift-wishlist.component.ts");
/* harmony import */ var _gift_edit_gift_edit_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./gift-edit/gift-edit.component */ "./src/app/gift-edit/gift-edit.component.ts");
/* harmony import */ var _add_event_add_event_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./add-event/add-event.component */ "./src/app/add-event/add-event.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _dashbord_dashbord_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./dashbord/dashbord.component */ "./src/app/dashbord/dashbord.component.ts");
/* harmony import */ var _stories_stories_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./stories/stories.component */ "./src/app/stories/stories.component.ts");
/* harmony import */ var _event_list_event_list_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./event-list/event-list.component */ "./src/app/event-list/event-list.component.ts");
/* harmony import */ var _edit_event_edit_event_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./edit-event/edit-event.component */ "./src/app/edit-event/edit-event.component.ts");
/* harmony import */ var _phonenumber_phonenumber_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./phonenumber/phonenumber.component */ "./src/app/phonenumber/phonenumber.component.ts");
/* harmony import */ var _account_account_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./account/account.component */ "./src/app/account/account.component.ts");
/* harmony import */ var _add_stories_add_stories_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./add-stories/add-stories.component */ "./src/app/add-stories/add-stories.component.ts");
/* harmony import */ var _camera_camera_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./camera/camera.component */ "./src/app/camera/camera.component.ts");
/* harmony import */ var ngx_webcam__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ngx-webcam */ "./node_modules/ngx-webcam/fesm5/ngx-webcam.js");
/* harmony import */ var _snapshot_snapshot_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./snapshot/snapshot.component */ "./src/app/snapshot/snapshot.component.ts");
/* harmony import */ var _images_images_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./images/images.component */ "./src/app/images/images.component.ts");
/* harmony import */ var _comment_comment_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./comment/comment.component */ "./src/app/comment/comment.component.ts");
/* harmony import */ var _allpost_allpost_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./allpost/allpost.component */ "./src/app/allpost/allpost.component.ts");
/* harmony import */ var _couple_profile_couple_profile_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./couple-profile/couple-profile.component */ "./src/app/couple-profile/couple-profile.component.ts");
/* harmony import */ var _contact_list_contact_list_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./contact-list/contact-list.component */ "./src/app/contact-list/contact-list.component.ts");
/* harmony import */ var _contact_setting_contact_setting_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./contact-setting/contact-setting.component */ "./src/app/contact-setting/contact-setting.component.ts");
/* harmony import */ var _guest_profile_update_guest_profile_update_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./guest-profile-update/guest-profile-update.component */ "./src/app/guest-profile-update/guest-profile-update.component.ts");
/* harmony import */ var _error_error_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./error/error.component */ "./src/app/error/error.component.ts");
/* harmony import */ var _guest_dashboard_guest_dashboard_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./guest-dashboard/guest-dashboard.component */ "./src/app/guest-dashboard/guest-dashboard.component.ts");
/* harmony import */ var _guest_signup_guest_signup_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./guest-signup/guest-signup.component */ "./src/app/guest-signup/guest-signup.component.ts");
/* harmony import */ var _send_invitation_send_invitation_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./send-invitation/send-invitation.component */ "./src/app/send-invitation/send-invitation.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







//import { MDBBootstrapModule } from 'angular-bootstrap-md';
//import { ToastrModule } from 'ngx-toastr';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
// import { NativeScriptModule } from "nativescript-angular/nativescript.module";
// // import {WebcamModule} from 'ngx-webcam';
// import { AppRoutingModule } from "./app.routing";



























// import { WebCamModule } from 'ack-angular-webcam';













var routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"] },
    { path: 'phonenumber', component: _phonenumber_phonenumber_component__WEBPACK_IMPORTED_MODULE_30__["PhonenumberComponent"] },
    { path: 'phonenumber/:data', component: _phonenumber_phonenumber_component__WEBPACK_IMPORTED_MODULE_30__["PhonenumberComponent"] },
    { path: 'stories', component: _stories_stories_component__WEBPACK_IMPORTED_MODULE_27__["StoriesComponent"] },
    { path: 'stories/:data', component: _stories_stories_component__WEBPACK_IMPORTED_MODULE_27__["StoriesComponent"] },
    { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_17__["ProfileComponent"] },
    { path: 'profile/:data', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_17__["ProfileComponent"] },
    { path: 'invitation_cards', component: _couple_profile_couple_profile_component__WEBPACK_IMPORTED_MODULE_39__["CoupleProfileComponent"] },
    { path: 'allpost', component: _allpost_allpost_component__WEBPACK_IMPORTED_MODULE_38__["AllpostComponent"] },
    { path: 'photo', component: _photo_photo_component__WEBPACK_IMPORTED_MODULE_16__["PhotoComponent"] },
    { path: 'engagement', component: _engagement_engagement_component__WEBPACK_IMPORTED_MODULE_18__["EngagementComponent"] },
    { path: 'grid', component: _grid_gallery_grid_gallery_component__WEBPACK_IMPORTED_MODULE_19__["GridGalleryComponent"] },
    { path: 'calendar', component: _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_11__["CalendarComponent"] },
    { path: 'gift', component: _gift_gift_component__WEBPACK_IMPORTED_MODULE_12__["GiftComponent"] },
    { path: 'gift-item', component: _gift_item_gift_item_component__WEBPACK_IMPORTED_MODULE_21__["GiftItemComponent"] },
    { path: 'gift-lists', component: _gift_wishlist_gift_wishlist_component__WEBPACK_IMPORTED_MODULE_22__["GiftWishlistComponent"] },
    { path: 'gift-edit/:data', component: _gift_edit_gift_edit_component__WEBPACK_IMPORTED_MODULE_23__["GiftEditComponent"] },
    { path: 'add-event', component: _add_event_add_event_component__WEBPACK_IMPORTED_MODULE_24__["AddEventComponent"] },
    { path: 'register', component: _register_register_component__WEBPACK_IMPORTED_MODULE_25__["RegisterComponent"] },
    { path: 'dashboard', component: _dashbord_dashbord_component__WEBPACK_IMPORTED_MODULE_26__["DashbordComponent"] },
    { path: 'dashboard/:data', component: _dashbord_dashbord_component__WEBPACK_IMPORTED_MODULE_26__["DashbordComponent"] },
    { path: 'edit-event', component: _edit_event_edit_event_component__WEBPACK_IMPORTED_MODULE_29__["EditEventComponent"] },
    { path: 'gift-lists/:data', component: _gift_wishlist_gift_wishlist_component__WEBPACK_IMPORTED_MODULE_22__["GiftWishlistComponent"] },
    { path: 'gift-edit/:data', component: _gift_edit_gift_edit_component__WEBPACK_IMPORTED_MODULE_23__["GiftEditComponent"] },
    { path: 'event-list', component: _event_list_event_list_component__WEBPACK_IMPORTED_MODULE_28__["EventListComponent"] },
    { path: 'event-list/:data', component: _event_list_event_list_component__WEBPACK_IMPORTED_MODULE_28__["EventListComponent"] },
    { path: 'edit-event/:data', component: _edit_event_edit_event_component__WEBPACK_IMPORTED_MODULE_29__["EditEventComponent"] },
    { path: 'invitation', component: _invitation_invitation_component__WEBPACK_IMPORTED_MODULE_14__["InvitationComponent"] },
    { path: 'guest-list', component: _guestlist_guestlist_component__WEBPACK_IMPORTED_MODULE_13__["GuestlistComponent"] },
    { path: 'account', component: _account_account_component__WEBPACK_IMPORTED_MODULE_31__["AccountComponent"] },
    { path: 'add-stories', component: _add_stories_add_stories_component__WEBPACK_IMPORTED_MODULE_32__["AddStoriesComponent"] },
    { path: 'add-stories/:data', component: _add_stories_add_stories_component__WEBPACK_IMPORTED_MODULE_32__["AddStoriesComponent"] },
    { path: 'camera', component: _camera_camera_component__WEBPACK_IMPORTED_MODULE_33__["CameraComponent"] },
    { path: 'snapshot/:data', component: _snapshot_snapshot_component__WEBPACK_IMPORTED_MODULE_35__["SnapshotComponent"] },
    { path: 'snapshot', component: _snapshot_snapshot_component__WEBPACK_IMPORTED_MODULE_35__["SnapshotComponent"] },
    { path: 'images', component: _images_images_component__WEBPACK_IMPORTED_MODULE_36__["ImagesComponent"] },
    { path: 'images/:data', component: _images_images_component__WEBPACK_IMPORTED_MODULE_36__["ImagesComponent"] },
    { path: 'comment', component: _comment_comment_component__WEBPACK_IMPORTED_MODULE_37__["CommentComponent"] },
    { path: 'Contact-list', component: _contact_list_contact_list_component__WEBPACK_IMPORTED_MODULE_40__["ContactListComponent"] },
    { path: 'Contact-Setting', component: _contact_setting_contact_setting_component__WEBPACK_IMPORTED_MODULE_41__["ContactSettingComponent"] },
    { path: 'invite/:data', component: _guest_signup_guest_signup_component__WEBPACK_IMPORTED_MODULE_45__["GuestSignupComponent"] },
    { path: 'Guest_Dashboard', component: _guest_dashboard_guest_dashboard_component__WEBPACK_IMPORTED_MODULE_44__["GuestDashboardComponent"] },
    { path: 'Guest_Profile_Update', component: _guest_profile_update_guest_profile_update_component__WEBPACK_IMPORTED_MODULE_42__["GuestProfileUpdateComponent"] },
    { path: 'error/:data', component: _error_error_component__WEBPACK_IMPORTED_MODULE_43__["ErrorComponent"] },
    { path: 'SendInvitation', component: _send_invitation_send_invitation_component__WEBPACK_IMPORTED_MODULE_46__["SendInvitationComponent"] }
];
function getAuthServiceConfigs() {
    var config = new angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_8__["AuthServiceConfig"]([
        {
            id: angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_8__["GoogleLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_8__["GoogleLoginProvider"]("939569453062-jkpvc37viho01u1c9hmkppt5pe15lpfd.apps.googleusercontent.com")
        }
    ]);
    return config;
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _account_account_component__WEBPACK_IMPORTED_MODULE_31__["AccountComponent"],
                _allpost_allpost_component__WEBPACK_IMPORTED_MODULE_38__["AllpostComponent"],
                _couple_profile_couple_profile_component__WEBPACK_IMPORTED_MODULE_39__["CoupleProfileComponent"],
                _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_11__["CalendarComponent"],
                _gift_gift_component__WEBPACK_IMPORTED_MODULE_12__["GiftComponent"],
                _guestlist_guestlist_component__WEBPACK_IMPORTED_MODULE_13__["GuestlistComponent"],
                _invitation_invitation_component__WEBPACK_IMPORTED_MODULE_14__["InvitationComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"],
                _photo_photo_component__WEBPACK_IMPORTED_MODULE_16__["PhotoComponent"],
                _profile_profile_component__WEBPACK_IMPORTED_MODULE_17__["ProfileComponent"],
                _engagement_engagement_component__WEBPACK_IMPORTED_MODULE_18__["EngagementComponent"],
                _grid_gallery_grid_gallery_component__WEBPACK_IMPORTED_MODULE_19__["GridGalleryComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_20__["NavbarComponent"],
                _gift_item_gift_item_component__WEBPACK_IMPORTED_MODULE_21__["GiftItemComponent"],
                _gift_wishlist_gift_wishlist_component__WEBPACK_IMPORTED_MODULE_22__["GiftWishlistComponent"],
                _gift_edit_gift_edit_component__WEBPACK_IMPORTED_MODULE_23__["GiftEditComponent"],
                _add_event_add_event_component__WEBPACK_IMPORTED_MODULE_24__["AddEventComponent"],
                _register_register_component__WEBPACK_IMPORTED_MODULE_25__["RegisterComponent"],
                _dashbord_dashbord_component__WEBPACK_IMPORTED_MODULE_26__["DashbordComponent"],
                _stories_stories_component__WEBPACK_IMPORTED_MODULE_27__["StoriesComponent"],
                _event_list_event_list_component__WEBPACK_IMPORTED_MODULE_28__["EventListComponent"],
                _edit_event_edit_event_component__WEBPACK_IMPORTED_MODULE_29__["EditEventComponent"],
                _phonenumber_phonenumber_component__WEBPACK_IMPORTED_MODULE_30__["PhonenumberComponent"],
                _add_stories_add_stories_component__WEBPACK_IMPORTED_MODULE_32__["AddStoriesComponent"],
                _camera_camera_component__WEBPACK_IMPORTED_MODULE_33__["CameraComponent"],
                _snapshot_snapshot_component__WEBPACK_IMPORTED_MODULE_35__["SnapshotComponent"],
                _images_images_component__WEBPACK_IMPORTED_MODULE_36__["ImagesComponent"],
                _comment_comment_component__WEBPACK_IMPORTED_MODULE_37__["CommentComponent"],
                _allpost_allpost_component__WEBPACK_IMPORTED_MODULE_38__["AllpostComponent"],
                _couple_profile_couple_profile_component__WEBPACK_IMPORTED_MODULE_39__["CoupleProfileComponent"],
                _contact_list_contact_list_component__WEBPACK_IMPORTED_MODULE_40__["ContactListComponent"],
                _contact_setting_contact_setting_component__WEBPACK_IMPORTED_MODULE_41__["ContactSettingComponent"],
                _guest_profile_update_guest_profile_update_component__WEBPACK_IMPORTED_MODULE_42__["GuestProfileUpdateComponent"],
                _error_error_component__WEBPACK_IMPORTED_MODULE_43__["ErrorComponent"],
                _guest_dashboard_guest_dashboard_component__WEBPACK_IMPORTED_MODULE_44__["GuestDashboardComponent"],
                _guest_signup_guest_signup_component__WEBPACK_IMPORTED_MODULE_45__["GuestSignupComponent"],
                _send_invitation_send_invitation_component__WEBPACK_IMPORTED_MODULE_46__["SendInvitationComponent"],
            ],
            imports: [
                angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_8__["SocialLoginModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                angular_font_awesome__WEBPACK_IMPORTED_MODULE_2__["AngularFontAwesomeModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_5__["HttpModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"].forRoot(routes),
                // WebCamModule,
                ngx_webcam__WEBPACK_IMPORTED_MODULE_34__["WebcamModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                ngx_sharebuttons__WEBPACK_IMPORTED_MODULE_7__["ShareButtonsModule"].forRoot()
            ],
            providers: [
                _angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"],
                {
                    provide: angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_8__["AuthServiceConfig"],
                    useFactory: getAuthServiceConfigs
                }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]],
            entryComponents: [],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], AppModule);
    return AppModule;
}());

// host user 
// name : jeetraj@gmail.com
// password : jeetraj123 
// guest user 
// name parv@gmail.com
// password : parv123 


/***/ }),

/***/ "./src/app/calendar/calendar.component.css":
/*!*************************************************!*\
  !*** ./src/app/calendar/calendar.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".jumbotron{\r\n    background-color: transparent;\r\n    padding-bottom: 0%;\r\n}\r\nhr{\r\n    width:20%;\r\n    background-color: brown;\r\n}\r\n.jumbo{\r\n    margin-top: 60%;\r\n}\r\n.fa-plus-circle{\r\n    color: orange;\r\n    font-size: 20px;\r\n}\r\n.arrow a{\r\n    color: orange;\r\n    font-size: 20px;\r\n    cursor: pointer !important;\r\n    padding-top: 30px;\r\n}"

/***/ }),

/***/ "./src/app/calendar/calendar.component.html":
/*!**************************************************!*\
  !*** ./src/app/calendar/calendar.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <app-navbar></app-navbar>\r\n  </div>\r\n  <!-- <div class=\"row col-12 arrow\">\r\n    <a routerLink=\"/event-list\" class=\"fa fa-arrow-left\" > Check current event </a>\r\n  </div> -->\r\n  <div class=\"row\">\r\n    <div class=\"col-12\">\r\n        <div class=\"jumbotron\">\r\n            <!-- class=\"col-12 arrow text-center\" -->\r\n            <div class=\"jumbotron text-center\">\r\n                <h1 class=\"display-6\">Calendar</h1>\r\n                <hr class=\"mx-auto\">\r\n                \r\n                <div class=\"jumbo\">\r\n                  <p>Add the iternanrry of your wedding festival</p>\r\n                  <a *ngIf = \"!validate\" routerLink=\"/add-event\" class=\"fas fa-plus-circle \">ADD AN EVENT</a>\r\n               \r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/calendar/calendar.component.ts":
/*!************************************************!*\
  !*** ./src/app/calendar/calendar.component.ts ***!
  \************************************************/
/*! exports provided: CalendarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarComponent", function() { return CalendarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CalendarComponent = /** @class */ (function () {
    function CalendarComponent(http, router) {
        this.http = http;
        this.router = router;
        this.eventdata = [];
        this.edata = [];
    }
    CalendarComponent.prototype.ngOnInit = function () {
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        ;
        if (!authToken) {
            this.router.navigate(['../login']);
        }
        this.wedding_id = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        this.http.post('https://api.vowfest.com/event/list', { "wedding_id": this.wedding_id }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; })).subscribe(function (data) {
            if (data == null) {
                console.log("data is empty");
            }
            console.log(data);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
        ///for guest user 
        var user_login_type;
        user_login_type = JSON.parse(localStorage.getItem('userinfo'));
        user_login_type.login_type = localStorage.getItem('login_type');
        console.log(user_login_type.login_type);
        if (user_login_type.login_type == 'guest') {
            this.validate = true;
            console.log(this.validate);
        }
        else {
            console.log(this.validate);
            this.validate = false;
        }
    };
    CalendarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-calendar',
            template: __webpack_require__(/*! ./calendar.component.html */ "./src/app/calendar/calendar.component.html"),
            styles: [__webpack_require__(/*! ./calendar.component.css */ "./src/app/calendar/calendar.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], CalendarComponent);
    return CalendarComponent;
}());



/***/ }),

/***/ "./src/app/camera.service.ts":
/*!***********************************!*\
  !*** ./src/app/camera.service.ts ***!
  \***********************************/
/*! exports provided: CameraService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CameraService", function() { return CameraService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CameraService = /** @class */ (function () {
    function CameraService() {
        this.image = { base64: '', files: '' };
        this.fileToUpload = null;
    }
    CameraService.prototype.setimage = function (x) {
        this.image = x;
        console.log("%%%%%", this.image);
    };
    CameraService.prototype.setd = function (w) {
        console.log("%%%%%&&&&&&&", w);
        this.image.files = w;
    };
    CameraService.prototype.getimage = function () {
        return this.image;
    };
    CameraService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], CameraService);
    return CameraService;
}());



/***/ }),

/***/ "./src/app/camera/camera.component.css":
/*!*********************************************!*\
  !*** ./src/app/camera/camera.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ack-webcam{\n    width: 100% !important;\n    height: 736px !important;\n    position: absolute;\n    top:0;\n    -o-object-fit: fill;\n       object-fit: fill;\n    display: block;\n    z-index: -1;\n}\nnav{\n    background-color: black;\n    opacity: 0.3;\n   \n    z-index: 1;\n  \n}\n.navbar a{\n    font-size: 24px;\n    color: white;\n    text-align: center;\n}\n.circle button{\n    background-color: transparent;\n    outline: none;\n    font-size: 70px;\n    border: none;\n    color: white;\n}\n.switch button{\n    background-color: transparent;\n    outline: none;\n    font-size: 30px;\n    border: none;\n    color: white;\n    margin: auto;\n}\n"

/***/ }),

/***/ "./src/app/camera/camera.component.html":
/*!**********************************************!*\
  !*** ./src/app/camera/camera.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"col-12\">\n  <!-- camera window  -->\n    <div style=\"text-align:center\">\n        <webcam [height]=\"500\" [width]=\"500\" [trigger]=\"triggerObservable\" \n        (imageCapture)=\"handleImage($event)\" *ngIf=\"showWebcam\"\n          [allowCameraSwitch]=\"allowCameraSwitch\" \n          [switchCamera]=\"nextWebcamObservable\"\n          [videoOptions]=\"videoOptions\"\n          (cameraSwitched)=\"cameraWasSwitched($event)\"\n          (initError)=\"handleInitError($event)\">\n        </webcam>\n    </div>\n  <!-- camera window end  -->\n  <!-- buttom buttons   -->\n    <div class=\"row\">\n      <nav class=\"col-12 navbar fixed-bottom navbar-expand-md navbar-light\">\n        <!-- 1) upload button with gallery  -->\n        <div class=\"col-4 switch\">\n          <button type=\"submit\"  *ngIf=\"click_to_open_file\" class=\"nav-item nav-link\" (click) = \"addstory() \">\n            <i class=\"far fa-image\"></i>\n          </button>\n          <input type=\"file\" *ngIf=\"chosefile\"   #Image accept=\"image/*\" (change)=\"handleFileInput($event.target.files)\" multiple >\n        </div>\n        <!-- 2) upload button with front camera  -->\n        <div class=\"col-4 circle\">\n          <button (click)=\"triggerSnapshot()\"  class=\"nav-item nav-link\">\n            <i class=\"far fa-circle\"></i>\n          </button>\n        </div>\n        <!-- 3) upload button with back camera -->\n        <div class=\"col-4 switch\">\n          <button (click)=\"changeFacingMode()\" class=\"nav-item nav-link\">\n            <i class=\"fas fa-sync-alt\"></i>\n          </button>\n        </div>\n      </nav>\n    </div>\n  <!-- end of button button    -->\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/camera/camera.component.ts":
/*!********************************************!*\
  !*** ./src/app/camera/camera.component.ts ***!
  \********************************************/
/*! exports provided: CameraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CameraComponent", function() { return CameraComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var ngx_webcam__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-webcam */ "./node_modules/ngx-webcam/fesm5/ngx-webcam.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _camera_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../camera.service */ "./src/app/camera.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { WebCamComponent } from 'ack-angular-webcam';



var CameraComponent = /** @class */ (function () {
    function CameraComponent(http, camera, router) {
        this.http = http;
        this.camera = camera;
        this.router = router;
        this.click_to_open_file = true;
        this.chosefile = false;
        this.fileToUpload = [];
        this.formData = new FormData();
        this.imagedata = { base64: "", file: Array };
        this.imagedata2 = { base64: "", file: Array };
        this.story = { photo_data: '', wedding_id: '', story: '' };
        this.webcamImage = null;
        this.pictureTaken = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // toggle webcam on/off
        this.showWebcam = true;
        this.allowCameraSwitch = true;
        this.multipleWebcamsAvailable = false;
        this.videoOptions = {};
        this.errors = [];
        // webcam snapshot trigger
        this.trigger = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
        this.nextWebcam = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    CameraComponent.prototype.handleImage2 = function (webcamImage) {
        this.webcamImage = webcamImage;
    };
    CameraComponent.prototype.ngOnInit = function () {
        var _this = this;
        ngx_webcam__WEBPACK_IMPORTED_MODULE_3__["WebcamUtil"].getAvailableVideoInputs()
            .then(function (mediaDevices) {
            _this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
        });
    };
    CameraComponent.prototype.triggerSnapshot = function () {
        this.trigger.next();
    };
    CameraComponent.prototype.toggleWebcam = function () {
        this.showWebcam = !this.showWebcam;
    };
    CameraComponent.prototype.handleInitError = function (error) {
        this.errors.push(error);
    };
    CameraComponent.prototype.showNextWebcam = function (directionOrDeviceId) {
        // true => move forward through devices
        // false => move backwards through devices
        // string => move to device with given deviceId
        this.nextWebcam.next(directionOrDeviceId);
    };
    CameraComponent.prototype.handleImage = function (webcamImage) {
        // console.info('received webcam image $$$', webcamImage.imageAsDataUrl);
        //console.log(webcamImage);
        var date = new Date().valueOf();
        // Replace extension according to your media type
        //console.log(date)
        var imageName = date + '.jpeg';
        // const imageFile = new File([webcamImage.imageAsBase64], imageName)
        // this.formData.append('photos_data',imageFile, imageName)
        // this.formData.forEach(element => {
        //   console.log(element )
        //   this.imagedata2 = {base64: webcamImage.imageAsDataUrl, file: element}
        //   this.camera.setimage( this.imagedata2);
        //   console.log(this.imagedata2 )
        //  this.router.navigate(["../snapshot"])
        // });
        //this.pictureTaken.emit(webcamImage);
        var encodedBase64Picture;
        var file = new File([webcamImage.imageAsBase64], imageName);
        var reader = new FileReader();
        reader.onloadend = function (e) {
            console.log(e);
            encodedBase64Picture = reader.result;
        };
        console.log(file);
        this.imagedata2 = { base64: webcamImage.imageAsDataUrl, file: file, type: 'image/jpeg' };
        this.camera.setimage(this.imagedata2);
        console.log(this.imagedata2);
        this.router.navigate(["../snapshot"]);
        reader.readAsDataURL(file);
        // console.log(reader.readAsDataURL( file ));
        return encodedBase64Picture;
    };
    CameraComponent.prototype.cameraWasSwitched = function (deviceId) {
        console.log('active device: ' + deviceId);
        this.deviceId = deviceId;
    };
    Object.defineProperty(CameraComponent.prototype, "triggerObservable", {
        get: function () {
            return this.trigger.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CameraComponent.prototype, "nextWebcamObservable", {
        get: function () {
            return this.nextWebcam.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    CameraComponent.prototype.handleFileInput = function (file) {
        var _this = this;
        var _loop_1 = function (i) {
            var element = file[i];
            this_1.fileToUpload.push(element);
            var files = this_1.fileToUpload;
            var myReader = new FileReader();
            myReader.onloadend = function (e) {
                _this.img = myReader.result;
                _this.formData.append('photos_data', files[i], files[i].name);
            };
            myReader.readAsDataURL(element);
        };
        var this_1 = this;
        for (var i = 0; i < file.length; i++) {
            _loop_1(i);
        }
        this.chosefile = false;
        this.click_to_open_file = true;
    };
    CameraComponent.prototype.addstory = function (f) {
        var _this = this;
        this.chosefile = true;
        this.click_to_open_file = false;
        this.formData.forEach(function (element) {
            _this.imagedata = { base64: _this.img, file: element };
            _this.camera.setimage(_this.imagedata);
            console.log(_this.imagedata);
            _this.router.navigate(["../snapshot"]);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", ngx_webcam__WEBPACK_IMPORTED_MODULE_3__["WebcamImage"])
    ], CameraComponent.prototype, "webcamImage", void 0);
    CameraComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-camera',
            template: __webpack_require__(/*! ./camera.component.html */ "./src/app/camera/camera.component.html"),
            styles: [__webpack_require__(/*! ./camera.component.css */ "./src/app/camera/camera.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"],
            _camera_service__WEBPACK_IMPORTED_MODULE_5__["CameraService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], CameraComponent);
    return CameraComponent;
}());



/***/ }),

/***/ "./src/app/comment/comment.component.css":
/*!***********************************************!*\
  !*** ./src/app/comment/comment.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/comment/comment.component.html":
/*!************************************************!*\
  !*** ./src/app/comment/comment.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-xs-6 col-xs-offset-3\">  \n  <!-- <comment ></comment>   -->\n</div>"

/***/ }),

/***/ "./src/app/comment/comment.component.ts":
/*!**********************************************!*\
  !*** ./src/app/comment/comment.component.ts ***!
  \**********************************************/
/*! exports provided: CommentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentComponent", function() { return CommentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CommentComponent = /** @class */ (function () {
    function CommentComponent() {
    }
    CommentComponent.prototype.ngOnInit = function () {
    };
    CommentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-comment',
            template: __webpack_require__(/*! ./comment.component.html */ "./src/app/comment/comment.component.html"),
            styles: [__webpack_require__(/*! ./comment.component.css */ "./src/app/comment/comment.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CommentComponent);
    return CommentComponent;
}());



/***/ }),

/***/ "./src/app/contact-list/contact-list.component.css":
/*!*********************************************************!*\
  !*** ./src/app/contact-list/contact-list.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.topnav {\r\noverflow: hidden;\r\nmargin-left: 5%;\r\ntext-align: center;\r\n}\r\n\r\n.topnav a {\r\nfloat: left;\r\ndisplay: block;\r\ncolor: black;\r\ntext-align: center;\r\npadding: 14px 16px;\r\ntext-decoration: none;\r\nfont-size: 17px;\r\nborder-bottom: 3px solid transparent;\r\n}\r\n\r\n.topnav a:hover {\r\nborder-bottom: 3px solid red;\r\n}\r\n\r\n.topnav a.active {\r\nborder-bottom: 3px solid red;\r\n}\r\n\r\nul{\r\n    list-style-type: none;\r\n}\r\n\r\n.send_in .col-8,.send_in .col-4{\r\n    float: left;\r\n}\r\n\r\n.col-12.send_in {\r\n    border-bottom: 1px solid #e5e5e5;\r\n    padding: 15px 0;\r\n    margin-top: 30px;\r\n}\r\n\r\n.send_text{\r\n    color: orange;\r\n}\r\n\r\n.send_list{\r\n    margin-top: 30px;\r\n}\r\n\r\n.send_list ul{\r\n    padding: 0 !important;\r\n}\r\n\r\n.send_list ul li span{\r\n    float: right !important;\r\n}\r\n\r\nshare-buttons button{\r\n    margin: 0 5px !important;\r\n    margin-top: 10px !important;\r\n}\r\n\r\n.sb-facebook{\r\n    background: red !important;\r\n}\r\n\r\nbutton, select {\r\n    text-transform: none;\r\n    margin: 0 6px;\r\n    margin-top: 10px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/contact-list/contact-list.component.html":
/*!**********************************************************!*\
  !*** ./src/app/contact-list/contact-list.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12\">\r\n        <app-navbar></app-navbar>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"topnav\">\r\n        <a routerLink=\"/invitation\"  [ngClass]=\"{  'inactive': !shouldShow  }\">Invitation</a>\r\n        <a routerLink=\"/guest-list\"  [ngClass]=\"{ 'active': shouldShow, 'inactive': !shouldShow  }\">Guest list</a>\r\n        <a routerLink=\"/account\"  [ngClass]=\"{  'inactive': !shouldShow  }\">Account</a>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-12 send_in\">\r\n        <div class=\"col-8\">\r\n          <a href=\"#\" class=\"send_text\"> <i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i>  Send invitation </a>\r\n        </div>\r\n        <div class=\"col-4\">\r\n          <button  (click) = \"send_tocreate_guest()\">\r\n            <i class=\"fa fa-cog\" aria-hidden=\"true\" ></i>\r\n          </button>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-12 share_icon\">\r\n          <share-button  [title]=\"'We are exited to invite you for our wedding .'\"\r\n          button=\"facebook\" url=\"https://www.vowfest.com/invite/{{wid}}\" [showText]=\"false\"></share-button>\r\n\r\n          <a href=\"{{msg}}\"> \r\n            <i class=\"fa fa-whatsapp whatsapp_share\" aria-hidden=\"true\"></i>\r\n          </a>\r\n          <a target=\"_blank\" href=\"{{emailmsg}}\">                \r\n            <img style=\"height: 37px;padding-left: 16px\" src = \"https://cloud.githubusercontent.com/assets/7534680/4515518/6739a508-4bc1-11e4-80bc-670bcc216762.png\">\r\n          </a>\r\n        </div>\r\n\r\n\r\n       <div class=\"col-12 send_list\">\r\n          <ul>\r\n              <li (click) = \"send_tocreate_guest()\">aditi <span > <i class=\"fa fa-window-close\" aria-hidden=\"true\" style=\"color:orange;\"></i></span> </li>\r\n              <li (click) = \"add()\">mahi <span > <i class=\"fa fa-window-close\" aria-hidden=\"true\" style=\"color:orange;\"></i></span> </li>\r\n          </ul>\r\n       </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/contact-list/contact-list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/contact-list/contact-list.component.ts ***!
  \********************************************************/
/*! exports provided: ContactListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactListComponent", function() { return ContactListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContactListComponent = /** @class */ (function () {
    function ContactListComponent(router, http) {
        var _this = this;
        this.router = router;
        this.http = http;
        this.shouldShow = true;
        //gggggggg
        // ngAfterViewInit(): void {
        //   setTimeout(() => this.signIn(), 1000);
        // }
        // signIn() {
        //   alert("dvd")
        //   this.googleContacts()
        // }
        // googleContacts() {
        //   gapi.client.setApiKey('AIzaSyARvwirFktEIi_BTaKcCi9Ja-m3IEJYIRk');
        //   gapi.auth2.authorize(this.authConfig, this.handleAuthorization);
        // }
        this.handleAuthorization = function (authorizationResult) {
            if (authorizationResult && !authorizationResult.error) {
                var url = "https://www.google.com/m8/feeds/contacts/default/thin?" +
                    "alt=json&max-results=500&v=3.0&access_token=" +
                    authorizationResult.access_token;
                console.log("Authorization success, URL: ", url);
                _this.http.get(url)
                    .subscribe(function (response) {
                    if (response.feed && response.feed.entry) {
                        console.log(response.feed.entry);
                    }
                });
            }
        };
    }
    ContactListComponent.prototype.ngOnInit = function () {
        this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        this.msg = 'https://wa.me/?text=Hi ,We are excited to invite you for our wedding . Please sign up here "https://www.vowfest.com/invite/' + this.wid + '"  to be a part of the wedding fun . We are waiting,love  varushka , anushka';
        this.emailmsg = 'mailto:?subject=&body=Hi We are excited to invite you for our wedding . Please sign up here "https://www.vowfest.com/invite/' + this.wid + '"  to be a part of the wedding fun . We are waiting,love  varushka , anushka';
        // this.signIn()
        this.authConfig = {
            client_id: 'AIzaSyARvwirFktEIi_BTaKcCi9Ja-m3IEJYIRk',
            scope: 'https://www.googleapis.com/auth/contacts.readonly'
        };
    };
    ContactListComponent.prototype.contact_setting = function () {
        this.router.navigate(['../Contact-Setting']);
    };
    ContactListComponent.prototype.send_tocreate_guest = function () {
        var c = decodeURIComponent('?weddingid=' + this.wid);
        console.log(c);
        this.router.navigate(['../Contact-Setting']);
    };
    ContactListComponent.prototype.add = function () {
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        ;
        var headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', authToken);
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Authorization': authToken });
        this.http.post('https://api.vowfest.com/invitees/add', {
            "contacts_list": [
                {
                    "invited": true,
                    "name": "mahi",
                    "phone": "8839380364",
                    "rsvp": false
                },
                {
                    "invited": true,
                    "name": "lalit",
                    "phone": "9284858141",
                    "rsvp": false
                },
                {
                    "invited": true,
                    "name": "manasi",
                    "phone": "8600141828",
                    "rsvp": false
                }
            ]
        }, { headers: this.headers }).subscribe(function (data) {
            console.log('response: ', data);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    ContactListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contact-list',
            template: __webpack_require__(/*! ./contact-list.component.html */ "./src/app/contact-list/contact-list.component.html"),
            styles: [__webpack_require__(/*! ./contact-list.component.css */ "./src/app/contact-list/contact-list.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ContactListComponent);
    return ContactListComponent;
}());



/***/ }),

/***/ "./src/app/contact-setting/contact-setting.component.css":
/*!***************************************************************!*\
  !*** ./src/app/contact-setting/contact-setting.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n.switch {\r\n    position: relative;\r\n    display: inline-block;\r\n    width: 60px;\r\n    height: 25px;\r\n  }\r\n  \r\n  .switch input { \r\n    opacity: 0;\r\n    width: 0;\r\n    height: 0;\r\n  }\r\n  \r\n  .slider {\r\n    position: absolute;\r\n    cursor: pointer;\r\n    top: 0;\r\n    left: 0;\r\n    right: 0;\r\n    bottom: 0;\r\n    background-color: #ccc;\r\n    transition: .4s;\r\n  }\r\n  \r\n  .slider:before {\r\n    position: absolute;\r\n    content: \"\";\r\n    height: 18px;\r\n    width: 18px;\r\n    left: 10px;\r\n    bottom: 4px;\r\n    background-color: white;\r\n    transition: .4s;\r\n  }\r\n  \r\n  input:checked + .slider {\r\n    background-color: #2196F3;\r\n  }\r\n  \r\n  input:focus + .slider {\r\n    box-shadow: 0 0 1px #2196F3;\r\n  }\r\n  \r\n  input:checked + .slider:before {\r\n    -webkit-transform: translateX(26px);\r\n    transform: translateX(26px);\r\n  }\r\n  \r\n  /* Rounded sliders */\r\n  \r\n  .slider.round {\r\n    border-radius: 34px;\r\n  }\r\n  \r\n  .slider.round:before {\r\n    border-radius: 50%;\r\n  }\r\n  \r\n  .topnav {\r\n    overflow: hidden;\r\n    margin-left: 5%;\r\n    text-align: center;\r\n    }\r\n  \r\n  .topnav a {\r\n    float: left;\r\n    display: block;\r\n    color: black;\r\n    text-align: center;\r\n    padding: 14px 16px;\r\n    text-decoration: none;\r\n    font-size: 17px;\r\n    border-bottom: 3px solid transparent;\r\n    }\r\n  \r\n  .topnav a:hover {\r\n    border-bottom: 3px solid red;\r\n    }\r\n  \r\n  .topnav a.active {\r\n    border-bottom: 3px solid red;\r\n    }\r\n  \r\n  ul{\r\n        list-style-type: none;\r\n    }\r\n  \r\n  .swr_list li ,.swr_list1 li{\r\n      display: inline-block;\r\n      margin: 0 5px;\r\n    }\r\n  \r\n  .swr_list li a{\r\n    color: orange;\r\n    border: 1px solid orange;\r\n    border-radius: 100px;\r\n    padding: 3px 6px;\r\n    cursor: pointer !important;\r\n    font-size: 12px;\r\n    }\r\n  \r\n  .swr_list1 li a{\r\n        color: orange;\r\n        border-radius: 100px;\r\n        padding: 2px 5px;\r\n        cursor: pointer !important;\r\n        }"

/***/ }),

/***/ "./src/app/contact-setting/contact-setting.component.html":
/*!****************************************************************!*\
  !*** ./src/app/contact-setting/contact-setting.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12\">\r\n        <app-navbar></app-navbar>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"topnav\">\r\n       \r\n        <a routerLink=\"/invitation\"  [ngClass]=\"{  'inactive': !shouldShow  }\">Invitation</a>\r\n        <a routerLink=\"/guest-list\"  [ngClass]=\"{ 'active': shouldShow, 'inactive': !shouldShow  }\">Guest list</a>\r\n        <a routerLink=\"/account\"  [ngClass]=\"{  'inactive': !shouldShow  }\">Account</a>\r\n\r\n      </div>\r\n    </div>\r\n    <div class=\"row\" style=\"margin-top: 30px;border-bottom: 1px solid #ccc;\">\r\n    <div class=\"col-4\" style=\"color: orange;\">\r\n        <i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i> Add\r\n    </div>\r\n    <div class=\"col-3 text-right nopadd\">\r\n        <label class=\"switch\">\r\n            <input type=\"checkbox\" checked>\r\n            <span class=\"slider round\"></span>\r\n          </label>\r\n    </div>\r\n    <div class=\"col-5 text-right nopadd\">\r\n      <ul class=\"swr_list\">\r\n        <li><a href=\"\"> S</a> </li>\r\n        <li><a href=\"\"> W</a> </li>\r\n        <li><a href=\"\"> R</a> </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\" style=\"margin-top: 30px;\">\r\n      <div class=\"col-6 \">\r\n          <p style=\"color:#b1aeae;\">Natasha</p>\r\n        </div>\r\n    <div class=\"col-6 text-right nopadd\">\r\n      <ul class=\"swr_list1\">\r\n        <li><a href=\"\"> S</a> </li>\r\n        <li><a href=\"\"> W</a> </li>\r\n        <li><a href=\"\"> R</a> </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n  </div>\r\n"

/***/ }),

/***/ "./src/app/contact-setting/contact-setting.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/contact-setting/contact-setting.component.ts ***!
  \**************************************************************/
/*! exports provided: ContactSettingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactSettingComponent", function() { return ContactSettingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ContactSettingComponent = /** @class */ (function () {
    function ContactSettingComponent() {
        this.shouldShow = true;
    }
    ContactSettingComponent.prototype.ngOnInit = function () {
    };
    ContactSettingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contact-setting',
            template: __webpack_require__(/*! ./contact-setting.component.html */ "./src/app/contact-setting/contact-setting.component.html"),
            styles: [__webpack_require__(/*! ./contact-setting.component.css */ "./src/app/contact-setting/contact-setting.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ContactSettingComponent);
    return ContactSettingComponent;
}());



/***/ }),

/***/ "./src/app/couple-profile/couple-profile.component.css":
/*!*************************************************************!*\
  !*** ./src/app/couple-profile/couple-profile.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.jumbotron{\r\n    background-color: transparent;\r\n    padding-bottom: 0%;\r\n}\r\nhr{\r\n    width:10%;\r\n    background-color: brown;\r\n}\r\n.fa-plus-circle{\r\n    color: orange;\r\n    font-size: 20px;\r\n}\r\n.topnav {\r\noverflow: hidden;\r\nmargin-left: 5%;\r\ntext-align: center;\r\n}\r\n.topnav a {\r\nfloat: left;\r\ndisplay: block;\r\ncolor: black;\r\ntext-align: center;\r\npadding: 14px 16px;\r\ntext-decoration: none;\r\nfont-size: 17px;\r\nborder-bottom: 3px solid transparent;\r\n}\r\n.topnav a:hover {\r\nborder-bottom: 3px solid red;\r\n}\r\n.topnav a.active {\r\nborder-bottom: 3px solid red;\r\n}\r\n.card_box{\r\n    margin-top: 50px;\r\n    padding: 0px;\r\n}\r\n.card_box .col-4{\r\n    float: left\r\n}\r\n.card_box .col-4 img{\r\n    width: 100% !important;\r\n    border-radius: 5px;\r\n}"

/***/ }),

/***/ "./src/app/couple-profile/couple-profile.component.html":
/*!**************************************************************!*\
  !*** ./src/app/couple-profile/couple-profile.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-12\">\r\n      <app-navbar></app-navbar>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"topnav\">\r\n      <a routerLink=\"/invitation\" class=\"active\">Invitation</a>\r\n      <a routerLink=\"/guest-list\">Guest list</a>\r\n      <a routerLink=\"/account\">Account</a>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-12 card_box\">\r\n          <div class=\"col-4\">\r\n            <img class=\"img-responsive\" src = \"../../assets/invite_card.jpg\">\r\n          </div>\r\n          <div  class=\"col-4\">\r\n            <img class=\"img-responsive\" src = \"../../assets/invite_card.jpg\">\r\n          </div>\r\n          <div  class=\"col-4\">\r\n            <img class=\"img-responsive\" src = \"../../assets/invite_card.jpg\">\r\n          </div>\r\n      \r\n      \r\n    </div>\r\n    <div class=\"jumbotron text-center\">\r\n        <p>Add pictures of your invitation card to send to your guests</p>\r\n        <i class=\"fas fa-plus-circle\" type=\"button\" (click)= \"addpic()\" >ADD A PHOTO</i>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/couple-profile/couple-profile.component.ts":
/*!************************************************************!*\
  !*** ./src/app/couple-profile/couple-profile.component.ts ***!
  \************************************************************/
/*! exports provided: CoupleProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoupleProfileComponent", function() { return CoupleProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CoupleProfileComponent = /** @class */ (function () {
    function CoupleProfileComponent(http, router) {
        this.http = http;
        this.router = router;
    }
    CoupleProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        this.http.post('https://api.vowfest.com/story/list', { 'wedding_id': this.wid }, { headers: header }).subscribe(function (data) {
            console.log('DATA RECIEVED: ', data);
            _this.image_data = data;
            _this.image_data2 = _this.image_data.stories_list;
            console.log(_this.image_data2);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    CoupleProfileComponent.prototype.addpic = function () { };
    CoupleProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-couple-profile',
            template: __webpack_require__(/*! ./couple-profile.component.html */ "./src/app/couple-profile/couple-profile.component.html"),
            styles: [__webpack_require__(/*! ./couple-profile.component.css */ "./src/app/couple-profile/couple-profile.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], CoupleProfileComponent);
    return CoupleProfileComponent;
}());



/***/ }),

/***/ "./src/app/dashbord/dashbord.component.css":
/*!*************************************************!*\
  !*** ./src/app/dashbord/dashbord.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".story{\r\n    margin-top: 50px;\r\n}\r\n#pro{\r\n    border-radius: 50%;\r\n    height:50px;\r\n    margin:5%;\r\n}\r\n.col-8{\r\n    display: inline;\r\n    padding-top: 3%;\r\n}\r\n#ring{\r\n    float: right;\r\n    height: 20px;\r\n    width: 20px;\r\n\r\n    position: absolute;\r\n    right: 15%;\r\n    top: 15%;\r\n}\r\n.content{\r\n    height: -webkit-fit-content;\r\n    height: -moz-fit-content;\r\n    height: fit-content;\r\n    text-align: center;\r\n    margin: 30px;\r\n}\r\n#pro{\r\n    border-radius: 50%;\r\n    height: 40px;\r\n    width: 40px;\r\n    margin-top: 20%;\r\n}\r\np{\r\n    display: inline;\r\n}\r\n.icon{\r\n    padding-top: 10px;\r\n    text-align: center;\r\n}\r\n.share{\r\n    float: left;\r\n    margin-left: -25%;\r\n    margin-right: 50%;\r\n}\r\n/* .select{\r\n    float: right;\r\n} */\r\n.dropdown {\r\n    position: absolute;\r\n    display: inline-block;\r\n    /* right: 0.4em; */\r\n}\r\n.dropdown-content {\r\n    display: none;\r\n    position: relative;\r\n    margin-top: 50px;\r\n    background-color: wheat;\r\n    min-width: 160px;\r\n    overflow: auto;\r\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\r\n    z-index: 1;\r\n}\r\n.dropdown-content a {\r\n    color: black;\r\n    padding: 12px 16px;\r\n    text-decoration: none;\r\n    display: block;\r\n}\r\n.dropdown a:hover {background-color: black}\r\n.show {display:block;\r\nwidth: 30%;\r\nfloat: right;}\r\n.icon{\r\n    padding: 0 !important;\r\n    line-height: 40px;\r\n}\r\n.icon ul{\r\n    padding: 0 !important;\r\n    margin: 0;\r\n}\r\n.icon ul li{\r\n    list-style-type: none;\r\n    display: inline-block;\r\n}\r\n*{\r\n    margin: 0%;\r\n    padding: 0%;\r\n}\r\n.clearfix {\r\n    display: block;\r\n    content: \"\";\r\n    clear: both;\r\n}\r\n.signoutbutton{\r\n    margin-left: 10%;\r\n          background-color: #ef0873;\r\n}\r\nul {\r\n    list-style-type: none;\r\n    margin-left: 15px;\r\n    padding-top: 20px;\r\n  }\r\nul li{\r\n    margin: 10px 0px;\r\n}\r\n.viweallbutton{\r\n    position: fixed;\r\n    bottom: 9%;\r\n    left: 0;\r\n    right: 0;\r\n    margin: 0 auto;\r\n    width: inherit;\r\n    padding: 5px 0;\r\n}\r\n.story{\r\n    margin-top: 20px;\r\n}\r\n.padd_15{\r\n    padding: 0 15px !important;\r\n}\r\n.row{\r\n    padding: 0 15px !important;\r\n}\r\n.padd_zero{\r\n    padding: 0px !important;\r\n}\r\n.col-8{\r\n    display: inline;\r\n    padding-top: 3%;\r\n}\r\n#ring{\r\n    float: right;\r\n    height: 30px;\r\n    width: 30px;\r\n\r\n    position: absolute;\r\n    right: 15%;\r\n    top: 15%;\r\n}\r\n.content{\r\n    height: -webkit-fit-content;\r\n    height: -moz-fit-content;\r\n    height: fit-content;\r\n    text-align: center;\r\n    margin: 30px;\r\n}\r\n#pro{\r\n    height:50px !important;\r\n    margin:5%;\r\n    border-radius: 50%;\r\n    width: 50px;\r\n    margin-top: 20%;\r\n}\r\np{\r\n    display: inline;\r\n}\r\n.updimg{\r\n    text-align: center;\r\n}\r\n.icon{\r\n    float: right;\r\n    padding-top: 10px;\r\n    /* margin-left: -15px; */\r\n    text-align: center;\r\n}\r\n/* .content{\r\n    margin-bottom: 20px;\r\n    margin-top: 10px;\r\n} */\r\n.para{\r\n    /* word-wrap: break-word; */\r\n    padding-top: 18px\r\n}\r\n.share{\r\n    float: left;\r\n    /* margin-left: -10%; */\r\n}\r\n.select{\r\n    margin-right: -20%;\r\n}\r\n.dropdown {\r\n    position: absolute;\r\n    display: inline-block;\r\n    /* right: 0.4em; */\r\n}\r\n.gap i{\r\n    font-size: 15px;\r\n   \r\n}\r\n.sp i{\r\n    font-size: 20px;\r\n    margin-left:10px;\r\n    margin-right: 10px;\r\n    margin-top:10px;\r\n}\r\n.dropdown-content {\r\n    display: none;\r\n    /* position: relative; */\r\n    margin-top: 50px;\r\n    background-color: wheat;\r\n    min-width: 160px;\r\n    overflow: auto;\r\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\r\n    z-index: 1;\r\n}\r\n.dropdown-content a {\r\n    color: black;\r\n    padding: 12px 16px;\r\n    text-decoration: none;\r\n    display: block;\r\n}\r\n.dropdown a:hover {background-color: black}\r\n.show {display:block;\r\nwidth: 30%;\r\nfloat: right;}\r\n.strip{\r\nheight: 90px;\r\n\r\n}\r\n* {\r\n    box-sizing: border-box;\r\n  }\r\n/* .column {\r\n    float: left;\r\n    width: 33.33%;\r\n    padding: 5px;\r\n  } */\r\n.gap{\r\n      padding-left: 2%;\r\n      padding-right: 2%;\r\n  \r\n      padding-top: 26px;\r\n      }\r\n/* Clearfix (clear floats) */\r\n.row::after {\r\n    content: \"\";\r\n    clear: both;\r\n    display: table;\r\n  }\r\nh6{\r\n    margin-top:4%;\r\n    margin-bottom: 4%;\r\n}\r\nh1 {\r\n                text-align: center;\r\n                text-transform: uppercase;\r\n                color: #4CAF50;\r\n              }\r\np {\r\n                /* text-indent: 50px; */\r\n                /* text-align: justify; */\r\n                letter-spacing: 0px;\r\n              }\r\na {\r\n                text-decoration: none;\r\n                color: #008CBA;\r\n              }\r\n/* .container {\r\n    background-color: rgb(184, 185, 185);\r\n    } */\r\n.event1{\r\n        color: white;\r\n        font-size: 15px;\r\n        text-align: center;\r\n        position: absolute;\r\n        bottom: 0;\r\n        left: 0;\r\n    }\r\n.event2{\r\n        color: white;\r\n        font-size: 15px;\r\n        text-align: center;\r\n        position: absolute;\r\n        bottom: 0;\r\n        right: 0;\r\n    }\r\n.mainevent{\r\n        color: white;\r\n        font-size: 20px;\r\n        text-align: center;\r\n        position: absolute;\r\n        bottom: 40%;\r\n        right: 35%;\r\n    }\r\nlabel{color: #ffd6a5}\r\n.displaycards{\r\n    background-color: rgba(128, 128, 128, 0.14);\r\n    height: 100%;\r\n    padding: 0px 0 15px !important;\r\n}\r\n.lates_update{\r\n    margin-top: 30px;\r\n}\r\n.propic{\r\n    margin-bottom: 20px;\r\n    padding: 0px 15px !important;\r\n}\r\n.w3-dropdown-content{\r\n        right: 0;\r\n        top: 30px;\r\n    }\r\n/* .w3-bar-block{\r\n        width: 10px !important;\r\n    } */\r\n.top{\r\n        margin-top: 18px;\r\n        margin-bottom: 0px;\r\n    }\r\n.text{\r\n    word-break: break-all;\r\n    max-width: 100%;\r\n  }\r\n.far{\r\n      font-size: 20px;\r\n      margin-top: 10px;\r\n  }\r\n.eventheading{\r\n    text-align: center;\r\n   \r\n  }\r\n/* new css */\r\n.top_story{\r\n      height: 10px;\r\n  }\r\n.color{\r\n    background: linear-gradient(to right,#ff8d00,#ff1556);\r\n    height: 100px;\r\n    position: relative;\r\n    margin-top: 35%;\r\n}\r\n.msgicon_top_story{\r\n    padding-left: 10%;\r\n}\r\n.top_story img{\r\n    max-height: 140px;\r\n}\r\n.date_color{\r\n    color: #ffd6a5 !important;\r\n}\r\n.comment_button{\r\n    text-align: right\r\n}\r\n.comment_button button{\r\n    margin-top: 10px;\r\n    padding: 4px 20px;\r\n}\r\n.comment_input{\r\n    text-indent: 10px;\r\n}\r\n.comment_P{\r\n    color:gray;\r\n    font-size: 16px;\r\n}\r\nhr{\r\n    color: black;\r\n}\r\n.nostory{\r\n\r\n    margin-left: 13%;\r\n    margin-right: 13%;\r\n    margin-top: 19%;\r\n    margin-bottom: 16%;\r\n    color: black;\r\n}\r\n.margin_bottom{\r\n    margin-bottom: 25px;\r\n}\r\n.vow{\r\n    font-size: 14px;\r\n    line-height: 40px;\r\n}\r\n/*spinner */\r\n#spinner {\r\n\t-webkit-animation: frames 1s infinite linear;\r\n\tanimation: frames 1s infinite linear;\r\n\tbackground: transparent;\r\n\tborder: 1.75vw solid #FFF;\r\n\tborder-radius: 100%;\r\n\tborder-top-color: #DF691A;\r\n\twidth: 20vw;\r\n\theight: 20vw;\r\n\topacity: .6;\r\n\tpadding: 0;\r\n\tposition: absolute;\r\n\tz-index: 999;\r\n}\r\n@-webkit-keyframes frames {\r\n  0% {\r\n\t-webkit-transform: rotate(0deg);\r\n\ttransform: rotate(0deg);\r\n  }\r\n  100% {\r\n\t-webkit-transform: rotate(359deg);\r\n\ttransform: rotate(359deg);\r\n  }\r\n}\r\n@keyframes frames {\r\n  0% {\r\n\t-webkit-transform: rotate(0deg);\r\n\ttransform: rotate(0deg);\r\n  }\r\n  100% {\r\n\t-webkit-transform: rotate(359deg);\r\n\ttransform: rotate(359deg);\r\n  }\r\n}\r\n#pause {\r\n\tdisplay: block;\r\n\tbackground:\r\n\t\trgba(0, 0, 0, 0.66)\r\n\t\tno-repeat\r\n\t\t0 0;\r\n\twidth: 100%;\r\n\theight: 100%;\r\n\tposition: fixed;\r\n\tbottom: 0;\r\n\tleft: 0;\r\n\tz-index: 1000;\r\n}\r\n"

/***/ }),

/***/ "./src/app/dashbord/dashbord.component.html":
/*!**************************************************!*\
  !*** ./src/app/dashbord/dashbord.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"container-fluid\">\r\n  <div class=\"row story\" style=\"padding:0 !important\">\r\n      <div class=\"col-2\">\r\n          <!-- <img src=\"../../assets/0.jpeg\" alt=\"\" srcset=\"\" id=\"pro\"> -->\r\n          <img src=\"{{propic}}\" alt=\"\"  id=\"pro\">\r\n      </div>\r\n      <div class=\"col-8 vow\">\r\n          <p>{{name}} and {{better_half}} vow's</p>\r\n      </div>\r\n      <div class=\"col-2\">\r\n            <p>\r\n             <!-- <a routerLink=\"/invitation\"> -->\r\n                <a routerLink=\"/engagement\">\r\n                        <img src=\"../../assets/rings.png\" alt=\"\" id=\"ring\">                    </a>\r\n                    </p>\r\n      </div>\r\n  </div>\r\n  <div class=\"row content\">\r\n      <div class=\"col-12\">\r\n          <h3>Congrats User!</h3>\r\n          <h6>Start adding photos and stories for your guests to stay updated with your VowFest</h6>\r\n      </div>\r\n  </div>\r\n\r\n  <div class=\"row\">\r\n      <div class=\"col-12 updimg\">\r\n        <!-- | slice:0:3 -->\r\n        <div class=\"margin_bottom\" *ngFor=\"let i of image_data2 ;let i=index\">\r\n            <div class=\"row\"  style=\"padding:0 !important;margin-bottom: 15px\">\r\n                    <div class=\"col-2\">\r\n                       \r\n                        <img src=\"{{i.userphoto}}\" alt=\"\" srcset=\"\" id=\"pro\">\r\n                        \r\n                    </div>\r\n                    <div class=\"col-8 para\">\r\n                        <strong> {{i.username}}'s </strong> Wedding story has begun <br>\r\n                        <small class=\"small comment_P\"> {{i.time | date: 'medium'}} </small>\r\n                      \r\n                    </div>\r\n                    <div class=\"col-2 icon\">\r\n                        <ul>\r\n                            <li style=\"margin-right: 15px;\"><a class=\"\"><img src=\"../../assets/share.png\" alt=\"\"></a></li>\r\n                            <li class=\" w3-dropdown-hover select\"><a><img src=\"../../assets/select.png\"></a>\r\n                                <div class=\"w3-dropdown-content w3-bar-block w3-border w3-card-4\">\r\n                                    <a class=\"w3-bar-item w3-button\">Edit</a>\r\n                                    <a  class=\"w3-bar-item w3-button\">Delete</a>\r\n                                </div></li>\r\n                           \r\n                        </ul>\r\n                        \r\n                    </div>\r\n                </div>\r\n                <div  class=\" col-12\">\r\n                    <img class=\"col-12\" src=\"{{i.photo_data}}\" style=\"max-width:100%;height:auto;\"/>   \r\n                </div>\r\n                <div class=\"row col-12 displaycards\">\r\n                    <div class=\"row col-12 \">\r\n                        <div class=\"col-8 text-left\">\r\n                            <p  class=\"comment_P\"> {{i.story}}</p>\r\n                        </div>     \r\n                        <div class=\"col-4 text-right\">\r\n                            <i class=\"far fa-heart \" style=\"color:deeppink;\" (click)=\"setlike(i.photo_id)\">\r\n                                 <span *ngIf= \" i.num_likes > 0\">  {{i.num_likes}}</span>\r\n                            </i>\r\n                            <i class=\"far fa-comment msgicon_top_story\" style=\"color:orange;\"  (click) = \"viewcomment(i.photo_id)\"  > \r\n                                <span *ngIf= \" i.num_comments > 0\"> {{i.num_comments}}</span>\r\n                            </i>                               \r\n                                \r\n                        </div>     \r\n                    \r\n                    </div>\r\n                    \r\n                    <div class=\"col-12 comment_div\" *ngIf=\"comment_div == true && divid == i.photo_id\">\r\n                            <ul *ngFor=\"let comment of i.comments \" class=\"col-12 text-left\">\r\n                                <li class=\"col-12\">  <strong> {{comment.username}}</strong> just commented  </li>\r\n                                <li class=\"col-12\">  <p class=\"small\" style=\"margin-left:0%\">  {{comment.time | date}}</p>  </li>\r\n                                <li class=\"col-12\">  <p style=\"padding-bottom: 1%;\"> {{comment.body}}</p></li>\r\n                                <i class=\"fa fa-thumbs-up\" aria-hidden=\"true\" (click) = \" add_counts_of_like(comment.comment_id)\"></i>\r\n                                {{comment.num_likes}}\r\n\r\n                                <li class=\"col-12\">  <hr> </li> \r\n                            </ul>\r\n                            <br>\r\n                            <form class=\"row col-12\" (ngSubmit)=\"add_comment(com)\" #com=\"ngForm\">                        \r\n                                <div class=\"col-8 form-group\">\r\n                                    <input type=\"hidden\"  name = \"photo_id\" id=\"photo_id\" [(ngModel)]=\"i.photo_id\" />\r\n                                    <input type=\"text\" class=\"form-control comment_input\" name = \"body\" id=\"Inputname\" [(ngModel)]=\"body\" />\r\n                                </div>\r\n                                <div class=\"col-4 form-group comment_button\">\r\n                                    \r\n                                    <button type=\"submit\" class=\"btn btn-primary small\">comment</button>\r\n                                </div>    \r\n                            </form> \r\n                        </div>                     \r\n                </div>\r\n                    \r\n        </div>\r\n      </div>\r\n  </div>\r\n  <br><br><br>\r\n  <div class=\"clearfix\"></div>\r\n    <div class=\"row\">\r\n        <app-navbar></app-navbar>\r\n    </div>\r\n    <div id=\"pause\" class=\"d-flex align-items-center justify-content-center\" *ngIf = \"spinner == true\">\r\n        <div id=\"spinner\"></div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dashbord/dashbord.component.ts":
/*!************************************************!*\
  !*** ./src/app/dashbord/dashbord.component.ts ***!
  \************************************************/
/*! exports provided: DashbordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashbordComponent", function() { return DashbordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var rxjs_add_observable_of__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/observable/of */ "./node_modules/rxjs-compat/_esm5/add/observable/of.js");
/* harmony import */ var rxjs_add_operator_delay__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/delay */ "./node_modules/rxjs-compat/_esm5/add/operator/delay.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DashbordComponent = /** @class */ (function () {
    function DashbordComponent(http, router, next) {
        this.http = http;
        this.router = router;
        this.next = next;
        this.spinner = true;
        this.image_data = { status: '' };
        this.comment = { num_likes: '' };
        this.id = { photo_id: '' };
        this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
    }
    DashbordComponent.prototype.ngOnInit = function () {
        this.name = JSON.parse(localStorage.getItem('userinfo')).name || localStorage.getItem('guest_name');
        this.better_half = localStorage.getItem('userinfo') ? JSON.parse(localStorage.getItem('userinfo')).better_half : "no Betterhalf yet";
        this.propic = localStorage.getItem('userinfo') ? JSON.parse(localStorage.getItem('userinfo')).profile_photo : localStorage.getItem('guest_profile_photo');
        this.comment_div = false;
        this.getstory_list();
    };
    DashbordComponent.prototype.setlike = function (x) {
        var _this = this;
        this.id.photo_id = x;
        this.http.post("https://api.vowfest.com/photo/like/add", this.id, this.options).subscribe(function (data) {
            _this.getstory_list();
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    DashbordComponent.prototype.add_comment = function (c) {
        var _this = this;
        console.log(c.value);
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/add", c.value, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.getstory_list();
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    DashbordComponent.prototype.viewcomment = function (x) {
        var _this = this;
        if (this.comment_div == false) {
            this.comment_div = true;
        }
        else {
            this.comment_div = false;
        }
        this.divid = x;
        this.http.post('https://api.vowfest.com/photo/comment/list', { photo_id: x }).subscribe(function (data) {
            _this.viewwcomments = data;
            console.log(_this.viewwcomments);
            _this.com = _this.viewwcomments.comments;
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    DashbordComponent.prototype.getstory_list = function () {
        var _this = this;
        this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        this.http.post('https://api.vowfest.com/story/list', { 'wedding_id': this.wid }, { headers: header }).subscribe(function (data) {
            console.log('DATA RECIEVED: ', data);
            console.log(_this.image_data);
            _this.image_data = data;
            _this.spinner = false;
            console.log(_this.image_data);
            var delayedObservable = rxjs_Observable__WEBPACK_IMPORTED_MODULE_4__["Observable"].of(_this.image_data).delay(5000);
            delayedObservable.subscribe(function (data) { return console.log(data); });
            if (_this.image_data.status == true) {
                _this.image_data2 = _this.image_data.stories_list;
                // for(let i= 0 ; i< this.image_data.stories_list.length ; i++ ){
                //   this.image_data2[i] = this.image_data.stories_list[i];
                //   console.log( this.image_data2[i])
                // }
                console.log(_this.image_data2);
                if (_this.image_data2) {
                    for (var _i = 0, _a = _this.image_data2; _i < _a.length; _i++) {
                        var comment_array = _a[_i];
                        console.log(comment_array.comments.length);
                        if (comment_array.comments.length == 0) {
                            _this.comment_div = false;
                        }
                        else {
                            _this.comment_div = true;
                        }
                    }
                }
            }
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    DashbordComponent.prototype.add_counts_of_like = function (x) {
        var _this = this;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/like/add", { comment_id: x }, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.getstory_list();
            //this.view_counts_of_like(x);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    DashbordComponent.prototype.view_counts_of_like = function (x) {
        var _this = this;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/like/list", { comment_id: x }, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.comment_like = data;
            _this.comment.num_likes = _this.comment_like.num_likes;
            console.log(_this.comment.num_likes);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    DashbordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashbord',
            template: __webpack_require__(/*! ./dashbord.component.html */ "./src/app/dashbord/dashbord.component.html"),
            styles: [__webpack_require__(/*! ./dashbord.component.css */ "./src/app/dashbord/dashbord.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHandler"]])
    ], DashbordComponent);
    return DashbordComponent;
}());



/***/ }),

/***/ "./src/app/edit-event/edit-event.component.css":
/*!*****************************************************!*\
  !*** ./src/app/edit-event/edit-event.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".arrow{\n    float: left;\n    font-size: 30px;\n    line-height: 80px;\n}\n.button{\n    float: right;\n    height: 40px;\n    margin-top: 2vh;\n    width: 100px;\n    background-color: orange;\n    color: white;\n    text-align: center;\n    line-height: 40px;\n    border: none;\n}\n.button:active,.fa-arrow-left:active{\n    text-decoration: none;\n    color: white;\n}\n.jumbotron{\n    background-color: transparent;\n}\n.fa-arrow-left{\n    color: orange;\n}\n.form-group a{\n    color: red;\n}"

/***/ }),

/***/ "./src/app/edit-event/edit-event.component.html":
/*!******************************************************!*\
  !*** ./src/app/edit-event/edit-event.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <form (ngSubmit)=\"edit(eventedit)\" #eventedit=\"ngForm\">\r\n        <div class=\"row\" style=\"height:80px;\">\r\n            <div class=\"col-3 arrow\">\r\n                <a routerLink=\"/event-list\" class=\"fa fa-arrow-left\"></a>\r\n            </div>\r\n            <div class=\"col-6\"></div>\r\n            <div class=\"col-3\">\r\n                <button type=\"submit\"  class=\"button\">Done</button>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\" style=\"height:auto;\">\r\n            <div class=\"jumbotron ml-auto mr-auto\">\r\n                <div>\r\n                    <div class=\"form-group\">\r\n                        <input type=\"hidden\" class=\"form-control\"  name = \"event_id\" id=\"event_id\" [(ngModel)]=\"eventdata.event_id\"  >\r\n                          \r\n                        <input type=\"text\" class=\"form-control\"  name = \"event_name\" id=\"Inputname\" [(ngModel)]=\"eventdata.name\" placeholder=\"Enter event name\" >\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <input type=\"text\" class=\"form-control\" name = \"event_date\" id=\"InputLink\" [(ngModel)]=\"eventdata.date\" >\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <input type=\"text\" class=\"form-control\" name = \"event_venue\" id=\"InputAddress\" [(ngModel)]=\"eventdata.location\" >\r\n                    </div>\r\n                    \r\n                    <div class=\"form-group col-12 row\" *ngFor=\"let data of eventdata.programe; let i =index\">\r\n\r\n                        <div class=\" col-4\">\r\n                            <input type=\"text\" \r\n                                   class=\"form-control\" \r\n                                   name = \"{{data.name}}\" \r\n                                   id=\"{{i}}\" \r\n                                   [(ngModel)]=\"data.name\" >\r\n                        </div>\r\n                        <div class=\" col-4\">  \r\n                            <input type=\"time\" \r\n                                   class=\"form-control\" \r\n                                   name = \"{{i}}\" \r\n                                   id=\"{{i}}\" \r\n                                   [(ngModel)]=\"data.time\">\r\n                        </div>\r\n                        <div class=\" col-4\">  \r\n                            <a  class=\"gray5 fa fa-times-circle\"></a>\r\n                        </div>\r\n                    \r\n                    </div>     \r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>   \r\n    <a (click)=delete(eventdata.event_id) style=\"color:red\">DELETE EVENT</a>      \r\n</div>"

/***/ }),

/***/ "./src/app/edit-event/edit-event.component.ts":
/*!****************************************************!*\
  !*** ./src/app/edit-event/edit-event.component.ts ***!
  \****************************************************/
/*! exports provided: EditEventComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditEventComponent", function() { return EditEventComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditEventComponent = /** @class */ (function () {
    function EditEventComponent(http, router, activatedroute) {
        this.http = http;
        this.router = router;
        this.activatedroute = activatedroute;
        this.eve = { programe: {} };
        this.pr = [];
        this.eid = { event_id: '' };
        this.programs_list = [{ program_name: "", program_time: "" }];
    }
    EditEventComponent.prototype.ngOnInit = function () {
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        var headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', authToken);
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Authorization': authToken });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
        this.eventdata = JSON.parse(this.activatedroute.snapshot.params['data']);
        console.log(this.eventdata);
        console.log(this.eventdata.name);
    };
    EditEventComponent.prototype.edit = function (eventedit) {
        var _this = this;
        console.log(eventedit.value);
        var eventupdate = {
            "event_id": eventedit.value.event_id,
            "event_name": eventedit.value.event_name,
            "event_date": eventedit.value.event_date,
            "event_venue": eventedit.value.event_venue,
            "programs_list": [
                {
                    "program_name": eventedit.value.program_name,
                    "program_time": eventedit.value.program_time
                }
            ]
        };
        var headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        headers.append('Authorization', authToken);
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Authorization': authToken });
        this.eve.programe = JSON.stringify(this.eve.programe);
        this.http.post('https://api.vowfest.com/event/modify', eventupdate, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.router.navigate(['../event-list/']);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    EditEventComponent.prototype.delete = function (x) {
        var _this = this;
        this.http.post('https://api.vowfest.com/event/delete', { event_id: x }, { headers: this.headers }).subscribe(function (data) {
            console.log("event is deleted #######", data);
            _this.router.navigate(['../event-list/']);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    EditEventComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-event',
            template: __webpack_require__(/*! ./edit-event.component.html */ "./src/app/edit-event/edit-event.component.html"),
            styles: [__webpack_require__(/*! ./edit-event.component.css */ "./src/app/edit-event/edit-event.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], EditEventComponent);
    return EditEventComponent;
}());



/***/ }),

/***/ "./src/app/engagement/engagement.component.css":
/*!*****************************************************!*\
  !*** ./src/app/engagement/engagement.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".round_image{\r\n                height: 59%;\r\n                margin: 5%;\r\n                border-radius: 50%;\r\n                width: 42%;\r\n                margin-top: 39%;\r\n                margin-left: 28%\r\n}\r\n\r\n.paragraph{\r\n    padding-top: 16%;\r\n  \r\n}\r\n\r\n.modal_content{\r\n    padding: 15px;\r\n}\r\n\r\n.modal_content label{\r\n    margin-right: 15px;\r\n}\r\n\r\n.modal_content .btn-primary{\r\n    background-color: orange !important;\r\n    border: none !important;\r\n}"

/***/ }),

/***/ "./src/app/engagement/engagement.component.html":
/*!******************************************************!*\
  !*** ./src/app/engagement/engagement.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-2\">\r\n            <a routerLink=\"/stories\" class=\"fas fa-arrow-left\"></a>\r\n        </div>\r\n    </div>\r\n           \r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <img src=\"{{about_us_data.profile_photo}}\"  alt=\"\" srcset=\"\" id=\"pro\" class=\"round_image\">\r\n        </div>\r\n        <div class=\"col-12 paragraph text-center\">\r\n            <button class=\"fas fa-pen\" data-target=\"#Vediogallarypopup\" data-toggle=\"modal\"></button>\r\n            <p>\r\n              {{about_us_data.about}}\r\n            </p>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n <div class=\"modal fade\" id=\"Vediogallarypopup\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">\r\n          <div class=\"modal-dialog\" role=\"document\">\r\n            <div class=\"modal-content\">\r\n              <div class=\"col-sm-12 modal_content\">\r\n                    <form (ngSubmit)=\"update_aboutus(f)\" #f=\"ngForm\" style=\"border:1px solid #ccc;padding: 15px\">\r\n                        <div class=\"container\">\r\n                          <h2 class=\"text-center\">Edit About us</h2>\r\n                          <hr>\r\n                          <label for=\"name\"><b>About Us</b></label>\r\n                          <input type=\"text\" placeholder=\"About Us\" name=\"about\" [(ngModel)]= \"about\" required>   \r\n                          <input type=\"hidden\"  name=\"phone_num\" [(ngModel)]= \"about_us_data.phone_num\" >   \r\n                          <input type=\"hidden\"  name=\"profile_photo\" [(ngModel)]= \"about_us_data.profile_photo\" >\r\n                          <input type=\"hidden\"  name=\"better_half\" [(ngModel)]= \"about_us_data.better_half\">   \r\n                        </div>\r\n                        <button type=\"submit\" class=\"btn btn-primary\" >submit</button>\r\n                        <button style=\"float:right\" type=\"button \" class=\"btn btn-primary\" data-dismiss=\"modal\">Cancel</button>\r\n                    </form>\r\n                \r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>"

/***/ }),

/***/ "./src/app/engagement/engagement.component.ts":
/*!****************************************************!*\
  !*** ./src/app/engagement/engagement.component.ts ***!
  \****************************************************/
/*! exports provided: EngagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EngagementComponent", function() { return EngagementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EngagementComponent = /** @class */ (function () {
    function EngagementComponent(http, router, httpclient) {
        this.http = http;
        this.router = router;
        this.httpclient = httpclient;
        this.about_us_data = { profile_photo: '', about: '', phone_num: '', better_half: '' };
        this.formData = new FormData();
    }
    EngagementComponent.prototype.ngOnInit = function () {
        // wedding/details
        this.user_about_details();
    };
    EngagementComponent.prototype.user_about_details = function () {
        var _this = this;
        var weddingID = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        this.http.post('https://api.vowfest.com/wedding/details', { "wedding_id": weddingID }, { headers: this.headers }).subscribe(function (data) {
            console.log(data.json());
            _this.about_us_data = data.json();
        }, function (error) {
            console.log(error);
            //  this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    EngagementComponent.prototype.update_aboutus = function (a) {
        var _this = this;
        this.formData.append('about', a.value.about);
        this.formData.append('better_half', a.value.better_half);
        this.formData.append('phone_num', a.value.phone_num);
        // this.formData.append('profile_photo', a.value.profile_photo);
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({ 'Authorization': authToken });
        this.httpclient.post('https://api.vowfest.com/updateProfile', this.formData, { headers: header }).subscribe(function (data) {
            console.log(data);
            _this.user_about_details();
        }, function (error) {
            console.log(error);
            //  this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    EngagementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-engagement',
            template: __webpack_require__(/*! ./engagement.component.html */ "./src/app/engagement/engagement.component.html"),
            styles: [__webpack_require__(/*! ./engagement.component.css */ "./src/app/engagement/engagement.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], EngagementComponent);
    return EngagementComponent;
}());



/***/ }),

/***/ "./src/app/error/error.component.css":
/*!*******************************************!*\
  !*** ./src/app/error/error.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error_page{\r\n    margin-top: 100%;\r\n}\r\n.erros_status{\r\n    text-align: center;\r\n    font-weight: 800;\r\n}"

/***/ }),

/***/ "./src/app/error/error.component.html":
/*!********************************************!*\
  !*** ./src/app/error/error.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<section class=\"error_page\">\r\n    <p class=\"erros_status\">{{error.status}} </p>\r\n    <p class=\"text-center\">{{error.error.msg}} URL NOT FOUND  {{error.message}}</p>\r\n</section>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/error/error.component.ts":
/*!******************************************!*\
  !*** ./src/app/error/error.component.ts ***!
  \******************************************/
/*! exports provided: ErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorComponent", function() { return ErrorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ErrorComponent = /** @class */ (function () {
    function ErrorComponent(activatedroute) {
        this.activatedroute = activatedroute;
    }
    ErrorComponent.prototype.ngOnInit = function () {
        this.error = JSON.parse(this.activatedroute.snapshot.params['data']);
        console.log(this.error);
    };
    ErrorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-error',
            template: __webpack_require__(/*! ./error.component.html */ "./src/app/error/error.component.html"),
            styles: [__webpack_require__(/*! ./error.component.css */ "./src/app/error/error.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], ErrorComponent);
    return ErrorComponent;
}());



/***/ }),

/***/ "./src/app/event-list/event-list.component.css":
/*!*****************************************************!*\
  !*** ./src/app/event-list/event-list.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".jumbotron{\n    background-color: transparent;\n    padding-bottom: 0%;\n}\nhr{\n    width:20%;\n    background-color: brown;\n}\n.fa-plus-circle{\n    color: orange;\n    font-size: 20px;\n}\n#date{\n    float: left;\n}\n.fa-pen{\n    color: orangered;\n}\nbutton{\n    background-color: transparent;\n    border: none;\n    float: right;\n}\n.tablespace{\n    margin-bottom: 40px;\n}\nspan{\n    color: gray\n}\n/* ///// elobrated bar  */\n.container {\n    width: 600px;\n    margin: 28px auto; \n}\n.progressbar {\n    counter-reset: step;\n}\n.progressbar li {\n    list-style-type: none;\n    width: 20%;\n    float: left;\n    font-size: 12px;\n    position: relative;\n    text-align: center;\n    text-transform: uppercase;\n    color: #7d7d7d;\n}\n.progressbar li:before {\n    width: 30px;\n    height: 30px;\n    content: counter(step);\n    counter-increment: step;\n    line-height: 30px;\n    border: 2px solid #7d7d7d;\n    display: block;\n    text-align: center;\n    margin: 0 auto 10px auto;\n    border-radius: 50%;\n    background-color: white;\n}\n.progressbar li:after {\n    width: 100%;\n    height: 2px;\n    content: '';\n    position: absolute;\n    background-color: #7d7d7d;\n    top: 15px;\n    left: -50%;\n    z-index: -1;\n}\n.progressbar li:first-child:after {\n    content: none;\n}\n.progressbar li.active {\n    color: green;\n}\n.progressbar li.active:before {\n    border-color: #55b776;\n}\n.progressbar li.active + li:after {\n    background-color: #55b776;\n}"

/***/ }),

/***/ "./src/app/event-list/event-list.component.html":
/*!******************************************************!*\
  !*** ./src/app/event-list/event-list.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n      <app-navbar></app-navbar>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-12 jumbotron\">\r\n        <h1 class=\"display-6 text-center\">Calendar</h1>\r\n        <hr class=\"mx-auto\">\r\n        <div class=\"row tablespace\" *ngFor=\"let i of eventdata?.event_list \">\r\n          <div class=\"col-6\" (click) = \"show_progress_bar() \">\r\n            <span id=\"date\">\r\n              <strong> {{ i?.date | date }}</strong> \r\n            </span>\r\n          </div>\r\n          <div class=\"col-2 text-center\" (click) = \"show_progress_bar() \">  \r\n            <span id=\"name\">\r\n              <strong>{{ i?.name }}</strong>\r\n            </span>\r\n          </div>\r\n          <div class=\"col-4 \" *ngIf = \"!validate\"><button (click)=editevent(i) class=\"fas fa-pen\"></button></div>\r\n          <div class=\"container\" *ngIf = \"progress_bar_div\">\r\n            <ul class=\"progressbar\"  (click) = \"progress_bar_div = false\" >\r\n                <li class=\"active\">Start </li>\r\n                <li  *ngFor=\"let j of i.programe\">{{j.name}} <br>{{j.time}}</li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n      <div class=\"col-12\" style=\"width:100%\">\r\n          <a *ngIf = \"!validate\" routerLink=\"/add-event\"><i class=\"fas fa-plus-circle\">ADD EVENT</i></a>\r\n      </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/event-list/event-list.component.ts":
/*!****************************************************!*\
  !*** ./src/app/event-list/event-list.component.ts ***!
  \****************************************************/
/*! exports provided: EventListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventListComponent", function() { return EventListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EventListComponent = /** @class */ (function () {
    function EventListComponent(http, router) {
        this.http = http;
        this.router = router;
        this.edata = [];
        this.wedding_ids = { wedding_id: '' };
        this.progress_bar_div = false;
    }
    EventListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.wedding_ids.wedding_id = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        this.http.post('https://api.vowfest.com/event/list', this.wedding_ids).subscribe(function (data) {
            _this.eventdata = data;
            console.log(_this.eventdata);
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
        var user_login_type;
        user_login_type = JSON.parse(localStorage.getItem('userinfo'));
        console.log(user_login_type.login_type);
        if (user_login_type.login_type == 'guest') {
            this.validate = true;
            console.log(this.validate);
        }
        else {
            console.log(this.validate);
            this.validate = false;
        }
    };
    EventListComponent.prototype.editevent = function (x) {
        console.log("****************", this.edata);
        console.log("$$$$$$$$$%%%%%%", x);
        this.router.navigate(['../edit-event/', JSON.stringify(x)]);
    };
    EventListComponent.prototype.show_progress_bar = function () {
        this.progress_bar_div = true;
    };
    EventListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-event-list',
            template: __webpack_require__(/*! ./event-list.component.html */ "./src/app/event-list/event-list.component.html"),
            styles: [__webpack_require__(/*! ./event-list.component.css */ "./src/app/event-list/event-list.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EventListComponent);
    return EventListComponent;
}());



/***/ }),

/***/ "./src/app/gift-edit/gift-edit.component.css":
/*!***************************************************!*\
  !*** ./src/app/gift-edit/gift-edit.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".arrow{\n    float: left;\n    font-size: 30px;\n    line-height: 80px;\n}\n.button{\n    float: right;\n    height: 40px;\n    margin-top: 2vh;\n    width: 100px;\n    background-color: orange;\n    color: white;\n    text-align: center;\n    line-height: 40px;\n    border: none;\n}\n.button:active,.fa-arrow-left:active{\n    text-decoration: none;\n    color: white;\n}\n.jumbotron{\n    background-color: transparent;\n}\n.fa-arrow-left{\n    color: orange;\n}\n.form-group a{\n    color: red;\n}\nlabel{\n    color: orange;\n}\ninput[type=text] {\n    border: none;\n    border-bottom: 4px solid orange;  \n}"

/***/ }),

/***/ "./src/app/gift-edit/gift-edit.component.html":
/*!****************************************************!*\
  !*** ./src/app/gift-edit/gift-edit.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\" style=\"height:80px;\">\r\n      <div class=\"col-3 arrow\">\r\n          <a routerLink=\"/gift-lists\" class=\"fa fa-arrow-left\"></a>\r\n      </div>\r\n      <div class=\"col-6\"></div>\r\n      <div class=\"col-3\">\r\n          <a routerLink=\"/gift-lists\" class=\"button\">Delete Gift</a>\r\n      </div>\r\n  </div>\r\n  <div class=\"row\" style=\"height:auto;\">\r\n      <div class=\"jumbotron ml-auto mr-auto\">\r\n          <form (ngSubmit)=\"Edit_gift(giftedit)\" #giftedit=\"ngForm\">\r\n              <div class=\"form-group\">\r\n                  <label for=\"Inputname\">Gift item name</label>\r\n                  <input type=\"text\" class=\"form-control\" name = \"gift_name\" id=\"gift_name\" [(ngModel)]=\"gift_data.gift_name\" >\r\n              </div>\r\n              <div class=\"form-group\">\r\n                  <label for=\"InputLink\">Purchase link</label>\r\n                  <input type=\"text\" class=\"form-control\" name = \"gift_link\" id=\"gift_link\" [(ngModel)]=\"gift_data.gift_link\" >\r\n                  <input type=\"hidden\"  name = \"gift_id\" id=\"gift_id\" [(ngModel)]=\"gift_data.gift_id\" >\r\n              </div>\r\n              <div class=\"form-group\">\r\n                  <button type=\"submit\" class=\"button\">Done</button>\r\n              </div>\r\n          </form>\r\n          \r\n      </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/gift-edit/gift-edit.component.ts":
/*!**************************************************!*\
  !*** ./src/app/gift-edit/gift-edit.component.ts ***!
  \**************************************************/
/*! exports provided: GiftEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftEditComponent", function() { return GiftEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GiftEditComponent = /** @class */ (function () {
    function GiftEditComponent(http, router, activatedroute) {
        this.http = http;
        this.router = router;
        this.activatedroute = activatedroute;
    }
    GiftEditComponent.prototype.ngOnInit = function () {
        console.log(JSON.parse(this.activatedroute.snapshot.params['data']));
        this.gift_data = JSON.parse(this.activatedroute.snapshot.params['data']);
    };
    GiftEditComponent.prototype.Edit_gift = function (x) {
        var _this = this;
        var headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        headers.append('Authorization', authToken);
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken });
        this.http.post('https://api.vowfest.com/gift/modify', x.value, { headers: this.headers }).subscribe(function (result) {
            console.log(result);
            _this.router.navigate(['../gift-lists']);
        }, function (error) {
            console.log('There was an error: ', error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GiftEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gift-edit',
            template: __webpack_require__(/*! ./gift-edit.component.html */ "./src/app/gift-edit/gift-edit.component.html"),
            styles: [__webpack_require__(/*! ./gift-edit.component.css */ "./src/app/gift-edit/gift-edit.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], GiftEditComponent);
    return GiftEditComponent;
}());



/***/ }),

/***/ "./src/app/gift-item/gift-item.component.css":
/*!***************************************************!*\
  !*** ./src/app/gift-item/gift-item.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".arrow{\n    float: left;\n    font-size: 30px;\n    line-height: 80px;\n}\n\n.button{\n    float: right;\n    height: 40px;\n    margin-top: 2vh;\n    width: 100px;\n    background-color: orange;\n    text-align: center;\n    text-decoration: none;\n    color: white;\n    line-height: 40px;\n    border: none;\n}\n\n.jumbotron{\n    background-color: transparent;\n}\n\n.fa-arrow-left{\n    color: orange;\n    text-decoration: none;\n}\n\nlabel{\n    color: orange;\n}\n\ninput[type=text] {\n    border: none;\n    border-bottom: 4px solid orange;  \n}"

/***/ }),

/***/ "./src/app/gift-item/gift-item.component.html":
/*!****************************************************!*\
  !*** ./src/app/gift-item/gift-item.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <form (ngSubmit)=\"gift_item(f)\" #f=\"ngForm\">\r\n        <div class=\"row\" style=\"height:80px;\">\r\n            <div class=\"col-3 arrow\">\r\n                <a routerLink=\"/stories\" class=\"fa fa-arrow-left\"></a>\r\n            </div>\r\n            <div class=\"col-6\"></div>\r\n            <div class=\"col-3\">\r\n                <button  type=\"submit\" class=\"button\">Done</button>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\" style=\"height:auto;\">\r\n            <div class=\"jumbotron ml-auto mr-auto\">\r\n            \r\n                    <div class=\"form-group\">\r\n                        <label for=\"Inputname\">Gift item name</label>\r\n                        <input type=\"text\" class=\"form-control\" name=\"gift_name\" [(ngModel)]=\"gift_name\" >\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"InputLink\">Purchase link</label>\r\n                        <input type=\"text\" class=\"form-control\" name=\"gift_link\" [(ngModel)]=\"gift_link\" >\r\n                    </div>\r\n                   \r\n                    \r\n            </div>\r\n        </div>\r\n    </form>    \r\n  <!-- <app-gift-wishlist [gift]=\"item\"></app-gift-wishlist> -->\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/gift-item/gift-item.component.ts":
/*!**************************************************!*\
  !*** ./src/app/gift-item/gift-item.component.ts ***!
  \**************************************************/
/*! exports provided: GiftItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftItemComponent", function() { return GiftItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GiftItemComponent = /** @class */ (function () {
    function GiftItemComponent(router, http) {
        this.router = router;
        this.http = http;
        this.item = { gift_name: '', gift_link: '' };
    }
    GiftItemComponent.prototype.ngOnInit = function () {
    };
    GiftItemComponent.prototype.gift_item = function (a) {
        var _this = this;
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Authorization': authToken });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
        this.http.post('https://api.vowfest.com/gift/add', a.value, { headers: this.headers })
            .subscribe(function (data) {
            console.log(data);
            _this.router.navigate(['../gift-lists']);
        }, function (error) {
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GiftItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gift-item',
            template: __webpack_require__(/*! ./gift-item.component.html */ "./src/app/gift-item/gift-item.component.html"),
            styles: [__webpack_require__(/*! ./gift-item.component.css */ "./src/app/gift-item/gift-item.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], GiftItemComponent);
    return GiftItemComponent;
}());



/***/ }),

/***/ "./src/app/gift-wishlist/gift-wishlist.component.css":
/*!***********************************************************!*\
  !*** ./src/app/gift-wishlist/gift-wishlist.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".jumbotron{\r\n    background-color: transparent;\r\n    padding-bottom: 0%;\r\n}\r\nhr{\r\n    width:10%;\r\n    background-color: brown;\r\n}\r\n.fa-pen{\r\n    float: right;\r\n    color: orangered;\r\n    font-size: 20px;\r\n}\r\n.fa-pen:active{\r\n    color: orangered;\r\n    text-decoration: none;\r\n}\r\n.fa-trash{\r\n    float: right;\r\n    color: orangered;\r\n    font-size: 20px;\r\n}\r\n.fa-trash:active{\r\n    color: orangered;\r\n    text-decoration: none;\r\n}\r\n.list-group{\r\n    border: none;\r\n}\r\n.list-group li{\r\n    border: 0 none;\r\n}\r\n.fa-plus-circle{\r\n    color: orange;\r\n    margin-left: 10%;\r\n}\r\nbutton{\r\n    background-color: transparent;\r\n    border: none;\r\n    float: right;\r\n}\r\n.list{\r\n       padding-top: 10%\r\n     }\r\n"

/***/ }),

/***/ "./src/app/gift-wishlist/gift-wishlist.component.html":
/*!************************************************************!*\
  !*** ./src/app/gift-wishlist/gift-wishlist.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <app-navbar></app-navbar>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-12\">\r\n        <div class=\"jumbotron\">\r\n            <h1 class=\"display-6 text-center\">Gift-Wishlist</h1>\r\n            <hr class=\"mx-auto\">\r\n          <div class=\"row list \" *ngFor=\"let item of giftname;let i = index\">\r\n              <div class=\"col-4\">  \r\n                <span id=\"name\">              \r\n                  <a>{{ item.gift_name }}</a>\r\n                </span>\r\n              </div>\r\n              <div class=\"col-4 \"><button (click)=\"deletegift(i,item.gift_id)\" class=\"fas fa-trash\"></button></div>\r\n              <div class=\"col-4 \"><button (click)=\"editgift(item)\"  class=\"fas fa-pen\"></button></div>\r\n          </div>\r\n        </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-12\">\r\n        <a routerLink=\"/gift-item\"><i class=\"fas fa-plus-circle\">ADD GIFT ITEM</i></a>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/gift-wishlist/gift-wishlist.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/gift-wishlist/gift-wishlist.component.ts ***!
  \**********************************************************/
/*! exports provided: GiftWishlistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftWishlistComponent", function() { return GiftWishlistComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GiftWishlistComponent = /** @class */ (function () {
    function GiftWishlistComponent(activatedroute, http, router) {
        this.activatedroute = activatedroute;
        this.http = http;
        this.router = router;
        this.data = [];
        this.showdata = { name: '', link: '' };
        this.giftname = [];
        this.gifts = [];
    }
    GiftWishlistComponent.prototype.ngOnInit = function () {
        var _this = this;
        var wedding_id = { 'wedding_id': localStorage.getItem('weddingID') };
        console.log(wedding_id);
        this.http.post('https://api.vowfest.com/gift/list', wedding_id).subscribe(function (data) {
            _this.giftname = data.json().gift_list;
            console.log(data.json());
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GiftWishlistComponent.prototype.editgift = function (x) {
        this.router.navigate(['../gift-edit', JSON.stringify(x)]);
    };
    GiftWishlistComponent.prototype.deletegift = function (a, x) {
        var _this = this;
        console.log(a);
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Authorization': authToken });
        this.http.post('https://api.vowfest.com/gift/delete', {
            "gift_id": x
        }, { headers: headers }).subscribe(function (data) {
            console.log(data.json());
            _this.giftname.splice(a, 1);
        }, function (error) {
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GiftWishlistComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gift-wishlist',
            template: __webpack_require__(/*! ./gift-wishlist.component.html */ "./src/app/gift-wishlist/gift-wishlist.component.html"),
            styles: [__webpack_require__(/*! ./gift-wishlist.component.css */ "./src/app/gift-wishlist/gift-wishlist.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], GiftWishlistComponent);
    return GiftWishlistComponent;
}());



/***/ }),

/***/ "./src/app/gift/gift.component.css":
/*!*****************************************!*\
  !*** ./src/app/gift/gift.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".jumbotron{\n    background-color: transparent;\n    padding-bottom: 0%;\n}\nhr{\n    width:20%;\n    background-color: brown;\n}\n.jumbo{\n    margin-top: 35%;\n}\n.fa-plus-circle{\n    color: orange;\n    font-size: 20px;\n}\n.add_gift{\n    padding-top:80%;\n}\n.icon{\n    padding-left: 26%;\n}"

/***/ }),

/***/ "./src/app/gift/gift.component.html":
/*!******************************************!*\
  !*** ./src/app/gift/gift.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n    <div class=\"row\">\n      <app-navbar></app-navbar>  \n    </div>\n<div class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"jumbotron\">\n            <div class=\"jumbotron text-center\">\n                <h1 class=\"display-6\">Gift-Wishlist</h1>\n                <hr class=\"mx-auto\">\n                \n                <div  *ngIf = \"validate\">\n                    <p>This is your Host gift list </p>\n                </div>\n                <div class=\"col-4 \" *ngIf = \"validate\"> \n                    <label> Gift name </label> \n                    <div class=\"row\" *ngFor=\"let i of GIftArray.gift_list\">\n                        <div class=\"col-4\"> \n                          \n                            <span id=\"name\">\n                             \n                               <a>{{ i.gift_name }}</a>\n                            </span>\n                        </div>\n                    </div>\n                </div>\n              \n            </div>\n            <div *ngIf = \"!validate\"  class=\" add_gift\">\n                <p>Help your guests decide what to gift you for starting a new life</p>\n                <a  routerLink=\"/gift-item\" class=\"icon row col-12\">\n                    <i class=\"fas fa-plus-circle \">\n                        ADD A GIFT \n                    </i>\n                </a>\n            </div>\n        </div>\n    </div>\n</div>\n<!-- <div class=\"row\">\n    <div class=\"col-12 text-center\">\n    </div>\n</div> -->\n</div>"

/***/ }),

/***/ "./src/app/gift/gift.component.ts":
/*!****************************************!*\
  !*** ./src/app/gift/gift.component.ts ***!
  \****************************************/
/*! exports provided: GiftComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftComponent", function() { return GiftComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GiftComponent = /** @class */ (function () {
    function GiftComponent(http, router) {
        this.http = http;
        this.router = router;
        this.GIftArray = { gift_list: [] };
    }
    GiftComponent.prototype.ngOnInit = function () {
        var _this = this;
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        if (!authToken) {
            this.router.navigate(['../login']);
        }
        var user_login_type;
        user_login_type = JSON.parse(localStorage.getItem('userinfo'));
        user_login_type.login_type = localStorage.getItem('login_type');
        console.log(user_login_type.login_type);
        if (user_login_type.login_type == 'guest') {
            this.validate = true;
            console.log(this.validate);
        }
        else {
            console.log(this.validate);
            this.validate = false;
        }
        var wedding_id = { 'wedding_id': localStorage.getItem('weddingID') };
        console.log(wedding_id);
        this.http.post('https://api.vowfest.com/gift/list', wedding_id).subscribe(function (data) {
            _this.GIftArray = data;
            console.log(_this.GIftArray);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GiftComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gift',
            template: __webpack_require__(/*! ./gift.component.html */ "./src/app/gift/gift.component.html"),
            styles: [__webpack_require__(/*! ./gift.component.css */ "./src/app/gift/gift.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], GiftComponent);
    return GiftComponent;
}());



/***/ }),

/***/ "./src/app/grid-gallery/grid-gallery.component.css":
/*!*********************************************************!*\
  !*** ./src/app/grid-gallery/grid-gallery.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".color {\n    font-size: 20px;\n    color: deeppink;\n    margin-top: 20px;\n}\n\n.dropdown {\n    position: absolute;\n    display: inline-block;\n    /* right: 0.4em; */\n}\n\n.dropdown-content {\n    display: none;\n    position: relative;\n    /* margin-top: 10px; */\n    margin-right: -5px;\n    background-color: wheat;\n    min-width: 160px;\n    overflow: auto;\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    z-index: 1;\n}\n\n.dropdown-content a {\n    color: black;\n    padding: 12px 16px;\n    text-decoration: none;\n    display: block;\n}\n\n.dropdown a:hover {\n    background-color: black;\n}\n\n.show {\n    display:block;\n    width: 30%;\n    float: right;\n}\n\n.fa-image{\n    font-size: 28px !important;\n}\n\n.row { /* IE10 */\n    display: flex; /* IE10 */\n    flex-wrap: wrap;\n    padding: 0 4px;\n    }\n\n/* Create four equal columns that sits next to each other */\n\n.column { /* IE10 */\n    flex: 25%;\n    padding: 0 4px;\n    }\n\n.column img {\n    vertical-align: middle;\n        width: 100%;\n        height: 100px;\n    }\n\n.fa-image,.fa-image:active,.fa-arrow-left,.fa-arrow-left:active{\n    color: deeppink;\n    text-decoration: none !important;\n}\n\n/* The Modal (background) */\n\n.modal {\ndisplay: none; /* Hidden by default */\nposition: fixed; /* Stay in place */\nz-index: 1; /* Sit on top */\npadding-top: 200px; /* Location of the box */\nleft: 0;\ntop: 0;\nwidth: 100%; /* Full width */\nheight: 100%; /* Full height */\noverflow: auto; /* Enable scroll if needed */\nbackground-color: rgb(0,0,0); /* Fallback color */\nbackground-color: rgba(0,0,0,0.4); /* Black w/ opacity */\n}\n\n/* Modal Content */\n\n.modal-content {\nposition: relative;\nbackground-color: #fefefe;\nmargin: auto;\npadding: 0;\nwidth: 80%;\nbox-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);\n-webkit-animation-name: animatetop;\n-webkit-animation-duration: 0.4s;\nanimation-name: animatetop;\nanimation-duration: 0.4s\n}\n\n/* Add Animation */\n\n@-webkit-keyframes animatetop {\nfrom {top:-300px; opacity:0} \nto {top:0; opacity:1}\n}\n\n@keyframes animatetop {\nfrom {top:-300px; opacity:0}\nto {top:0; opacity:1}\n}\n\n/* The Close Button */\n\n.close {\ncolor: grey;\nfloat: right;\nfont-size: 20px;\nfont-weight: bold;\nmargin: 2%;\n}\n\n.close:hover,\n.close:focus {\ncolor: #000;\ntext-decoration: none;\ncursor: pointer;\n}\n\n.modal-header {\npadding: 16px 16px;\nbackground-color: white;\ncolor: black;\n}\n\n.modal-body {padding: 16px 16px;\ncolor: deeppink;}\n\n.modal-footer {\npadding: 2px 16px;\ncolor: black;\n}\n\ninput[type=email]{\n    width: 100%;\n}\n\ninput[type=email]:focus{\n    border-bottom-color: deeppink;\n}\n\n#send{\n    background-color: transparent;\n    color: deeppink;\n    border: none;\n    font-size: 20px;\n    font-weight: bold;\n    margin: 2%;\n}"

/***/ }),

/***/ "./src/app/grid-gallery/grid-gallery.component.html":
/*!**********************************************************!*\
  !*** ./src/app/grid-gallery/grid-gallery.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row color\">\n      <div class=\"col-2\">\n          <a routerLink=\"/engagement\" class=\"fas fa-arrow-left\"></a> \n      </div>\n      <div class=\"col-8\" style=\"color:black;padding-top: 0px;\">\n          Engagement\n      </div>\n      <div class=\"col-1\">\n          <a routerLink=\"/engagement\" class=\"fas fa-image\"></a> \n      </div>\n      <div class=\"col-1\">\n          <i class=\" dropbtn icons fas fa-ellipsis-v\" onclick=\"showDropdown()\" style=\"margin-top:5px;\">\n          </i>\n          <!-- menu -->\n          <div id=\"myDropdown\" class=\"dropdown-content\">\n              <a  id=\"myBtn\">Send for print</a>\n              <a>Delete posts</a>\n          </div>\n      </div>\n  </div>\n  <div id=\"myModal\" class=\"modal\">\n      <!-- Modal content -->\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n          <h4>Send album for print</h4>\n          </div>\n          <div class=\"modal-body\">\n          <p>Email id to send the album</p>\n          <input type=\"email\" name=\"\" id=\"email\">\n          </div>\n          <div class=\"modal-footer\">\n              <button class=\"close\">CANCEL</button>\n              <button id=\"send\">SEND</button>\n          </div>\n      </div>\n  </div>\n  <div class=\"row\" > \n      <div class=\"col-4 column\" *ngFor=\"let item of img\">\n          <img src={{item}}>\n      </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/grid-gallery/grid-gallery.component.ts":
/*!********************************************************!*\
  !*** ./src/app/grid-gallery/grid-gallery.component.ts ***!
  \********************************************************/
/*! exports provided: GridGalleryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GridGalleryComponent", function() { return GridGalleryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GridGalleryComponent = /** @class */ (function () {
    function GridGalleryComponent(http, router) {
        this.http = http;
        this.router = router;
        this.geturl = 'https://api.vowfest.com/images/'; //only get and display
        this.img = [];
    }
    GridGalleryComponent.prototype.ngOnInit = function () {
        var _this = this;
        // var authToken = localStorage.getItem('userToken');
        // if(!authToken) 
        // {  this.router.navigate(['../login']);
        //  }        
        this.http.get(this.geturl).subscribe(function (data) {
            _this.GridArray = data;
            console.log(data);
        }, function (error) {
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
        this.http.get("http://13.233.29.234:8080/images").subscribe(function (data) {
            console.log("$$$$$$$$$$$$$$$$", JSON.parse(data._body));
            _this.pics = JSON.parse(data._body);
            _this.pics.forEach(function (element) {
                console.log("***************", element.image);
                _this.img.push(element.image);
            });
        });
    };
    GridGalleryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-grid-gallery',
            template: __webpack_require__(/*! ./grid-gallery.component.html */ "./src/app/grid-gallery/grid-gallery.component.html"),
            styles: [__webpack_require__(/*! ./grid-gallery.component.css */ "./src/app/grid-gallery/grid-gallery.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], GridGalleryComponent);
    return GridGalleryComponent;
}());



/***/ }),

/***/ "./src/app/guest-dashboard/guest-dashboard.component.css":
/*!***************************************************************!*\
  !*** ./src/app/guest-dashboard/guest-dashboard.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".story{\r\n    margin-top: 50px;\r\n}\r\n#pro{\r\n    border-radius: 50%;\r\n    height:50px;\r\n    margin:5%;\r\n}\r\n.col-8{\r\n    display: inline;\r\n    padding-top: 3%;\r\n}\r\n#ring{\r\n    float: right;\r\n    height: 20px;\r\n    width: 20px;\r\n\r\n    position: absolute;\r\n    right: 15%;\r\n    top: 15%;\r\n}\r\n.content{\r\n    height: -webkit-fit-content;\r\n    height: -moz-fit-content;\r\n    height: fit-content;\r\n    text-align: center;\r\n    margin: 30px;\r\n}\r\n#pro{\r\n    border-radius: 50%;\r\n    height: 40px;\r\n    width: 40px;\r\n    margin-top: 20%;\r\n}\r\np{\r\n    display: inline;\r\n}\r\n.icon{\r\n    padding-top: 10px;\r\n    text-align: center;\r\n}\r\n.share{\r\n    float: left;\r\n    margin-left: -25%;\r\n    margin-right: 50%;\r\n}\r\n/* .select{\r\n    float: right;\r\n} */\r\n.dropdown {\r\n    position: absolute;\r\n    display: inline-block;\r\n    /* right: 0.4em; */\r\n}\r\n.dropdown-content {\r\n    display: none;\r\n    position: relative;\r\n    margin-top: 50px;\r\n    background-color: wheat;\r\n    min-width: 160px;\r\n    overflow: auto;\r\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\r\n    z-index: 1;\r\n}\r\n.dropdown-content a {\r\n    color: black;\r\n    padding: 12px 16px;\r\n    text-decoration: none;\r\n    display: block;\r\n}\r\n.dropdown a:hover {background-color: black}\r\n.show {display:block;\r\nwidth: 30%;\r\nfloat: right;}\r\n.icon{\r\n    padding: 0 !important;\r\n    line-height: 40px;\r\n}\r\n.icon ul{\r\n    padding: 0 !important;\r\n    margin: 0;\r\n}\r\n.icon ul li{\r\n    list-style-type: none;\r\n    display: inline-block;\r\n}\r\n*{\r\n    margin: 0%;\r\n    padding: 0%;\r\n}\r\n.clearfix {\r\n    display: block;\r\n    content: \"\";\r\n    clear: both;\r\n}\r\n.signoutbutton{\r\n    margin-left: 10%;\r\n          background-color: #ef0873;\r\n}\r\nul {\r\n    list-style-type: none;\r\n    margin-left: 15px;\r\n    padding-top: 20px;\r\n  }\r\nul li{\r\n    margin: 10px 0px;\r\n}\r\n.viweallbutton{\r\n    position: fixed;\r\n    bottom: 9%;\r\n    left: 0;\r\n    right: 0;\r\n    margin: 0 auto;\r\n    width: inherit;\r\n    padding: 5px 0;\r\n}\r\n.story{\r\n    margin-top: 20px;\r\n}\r\n.padd_15{\r\n    padding: 0 15px !important;\r\n}\r\n.row{\r\n    padding: 0 15px !important;\r\n}\r\n.padd_zero{\r\n    padding: 0px !important;\r\n}\r\n.col-8{\r\n    display: inline;\r\n    padding-top: 3%;\r\n}\r\n#ring{\r\n    float: right;\r\n    height: 30px;\r\n    width: 30px;\r\n\r\n    position: absolute;\r\n    right: 15%;\r\n    top: 15%;\r\n}\r\n.content{\r\n    height: -webkit-fit-content;\r\n    height: -moz-fit-content;\r\n    height: fit-content;\r\n    text-align: center;\r\n    margin: 30px;\r\n}\r\n#pro{\r\n    height:50px !important;\r\n    margin:5%;\r\n    border-radius: 50%;\r\n    width: 50px;\r\n    margin-top: 20%;\r\n}\r\np{\r\n    display: inline;\r\n}\r\n.updimg{\r\n    text-align: center;\r\n}\r\n.icon{\r\n    float: right;\r\n    padding-top: 10px;\r\n    /* margin-left: -15px; */\r\n    text-align: center;\r\n}\r\n/* .content{\r\n    margin-bottom: 20px;\r\n    margin-top: 10px;\r\n} */\r\n.para{\r\n    /* word-wrap: break-word; */\r\n    padding-top: 18px\r\n}\r\n.share{\r\n    float: left;\r\n    /* margin-left: -10%; */\r\n}\r\n.select{\r\n    margin-right: -20%;\r\n}\r\n.dropdown {\r\n    position: absolute;\r\n    display: inline-block;\r\n    /* right: 0.4em; */\r\n}\r\n.gap i{\r\n    font-size: 15px;\r\n   \r\n}\r\n.sp i{\r\n    font-size: 20px;\r\n    margin-left:10px;\r\n    margin-right: 10px;\r\n    margin-top:10px;\r\n}\r\n.dropdown-content {\r\n    display: none;\r\n    /* position: relative; */\r\n    margin-top: 50px;\r\n    background-color: wheat;\r\n    min-width: 160px;\r\n    overflow: auto;\r\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\r\n    z-index: 1;\r\n}\r\n.dropdown-content a {\r\n    color: black;\r\n    padding: 12px 16px;\r\n    text-decoration: none;\r\n    display: block;\r\n}\r\n.dropdown a:hover {background-color: black}\r\n.show {display:block;\r\nwidth: 30%;\r\nfloat: right;}\r\n.strip{\r\nheight: 90px;\r\n\r\n}\r\n* {\r\n    box-sizing: border-box;\r\n  }\r\n/* .column {\r\n    float: left;\r\n    width: 33.33%;\r\n    padding: 5px;\r\n  } */\r\n.gap{\r\n      padding-left: 2%;\r\n      padding-right: 2%;\r\n  \r\n      padding-top: 26px;\r\n      }\r\n/* Clearfix (clear floats) */\r\n.row::after {\r\n    content: \"\";\r\n    clear: both;\r\n    display: table;\r\n  }\r\nh6{\r\n    margin-top:4%;\r\n    margin-bottom: 4%;\r\n}\r\nh1 {\r\n                text-align: center;\r\n                text-transform: uppercase;\r\n                color: #4CAF50;\r\n              }\r\np {\r\n                /* text-indent: 50px; */\r\n                /* text-align: justify; */\r\n                letter-spacing: 0px;\r\n              }\r\na {\r\n                text-decoration: none;\r\n                color: #008CBA;\r\n              }\r\n/* .container {\r\n    background-color: rgb(184, 185, 185);\r\n    } */\r\n.event1{\r\n        color: white;\r\n        font-size: 15px;\r\n        text-align: center;\r\n        position: absolute;\r\n        bottom: 0;\r\n        left: 0;\r\n    }\r\n.event2{\r\n        color: white;\r\n        font-size: 15px;\r\n        text-align: center;\r\n        position: absolute;\r\n        bottom: 0;\r\n        right: 0;\r\n    }\r\n.mainevent{\r\n        color: white;\r\n        font-size: 20px;\r\n        text-align: center;\r\n        position: absolute;\r\n        bottom: 40%;\r\n        right: 35%;\r\n    }\r\nlabel{color: #ffd6a5}\r\n.displaycards{\r\n    background-color: rgba(128, 128, 128, 0.14);\r\n    height: 100%;\r\n    padding: 0px 0 15px !important;\r\n}\r\n.lates_update{\r\n    margin-top: 30px;\r\n}\r\n.propic{\r\n    margin-bottom: 20px;\r\n    padding: 0px 15px !important;\r\n}\r\n.w3-dropdown-content{\r\n        right: 0;\r\n        top: 30px;\r\n    }\r\n/* .w3-bar-block{\r\n        width: 10px !important;\r\n    } */\r\n.top{\r\n        margin-top: 18px;\r\n        margin-bottom: 0px;\r\n    }\r\n.text{\r\n    word-break: break-all;\r\n    max-width: 100%;\r\n  }\r\n.far{\r\n      font-size: 20px;\r\n      margin-top: 10px;\r\n  }\r\n.eventheading{\r\n    text-align: center;\r\n   \r\n  }\r\n/* new css */\r\n.top_story{\r\n      height: 10px;\r\n  }\r\n.color{\r\n    background: linear-gradient(to right,#ff8d00,#ff1556);\r\n    height: 100px;\r\n    position: relative;\r\n    margin-top: 35%;\r\n}\r\n.msgicon_top_story{\r\n    padding-left: 10%;\r\n}\r\n.top_story img{\r\n    max-height: 140px;\r\n}\r\n.date_color{\r\n    color: #ffd6a5 !important;\r\n}\r\n.comment_button{\r\n    text-align: right\r\n}\r\n.comment_button button{\r\n    margin-top: 10px;\r\n    padding: 4px 20px;\r\n}\r\n.comment_input{\r\n    text-indent: 10px;\r\n}\r\n.comment_P{\r\n    color:gray;\r\n    font-size: 16px;\r\n}\r\nhr{\r\n    color: black;\r\n}\r\n.nostory{\r\n\r\n    margin-left: 13%;\r\n    margin-right: 13%;\r\n    margin-top: 19%;\r\n    margin-bottom: 16%;\r\n    color: black;\r\n}\r\n.margin_bottom{\r\n    margin-bottom: 25px;\r\n}\r\n.vow{\r\n    font-size: 14px;\r\n    line-height: 40px;\r\n    text-align: center;\r\n}"

/***/ }),

/***/ "./src/app/guest-dashboard/guest-dashboard.component.html":
/*!****************************************************************!*\
  !*** ./src/app/guest-dashboard/guest-dashboard.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row story\" style=\"padding:0 !important\">\r\n        <div class=\"col-2\">\r\n            <!-- <img src=\"../../assets/0.jpeg\" alt=\"\" srcset=\"\" id=\"pro\"> -->\r\n            <img src=\"{{guest_user_profile_pic}}\" alt=\"\"  id=\"pro\">\r\n        </div>\r\n        <div class=\"col-8 vow\">\r\n            <p>{{guest_user_name}}</p>\r\n        </div>\r\n        <div class=\"col-2\">\r\n              <p>\r\n               <a routerLink=\"/invitation\">\r\n                          <img src=\"../../assets/rings.png\" alt=\"\" id=\"ring\">                    </a>\r\n                      </p>\r\n        </div>\r\n    </div>\r\n    <div class=\"row content\">\r\n        <div class=\"col-12\">\r\n            <h3>Congrats User!</h3>\r\n            <h6>Start adding photos and stories for your guests to stay updated with your VowFest</h6>\r\n        </div>\r\n    </div>\r\n  \r\n    <div class=\"row\">\r\n        <div class=\"col-12 updimg\">\r\n          <div class=\"margin_bottom\" *ngFor=\"let i of image_data2  | slice:0:3;let i=index\">\r\n              <div class=\"row\"  style=\"padding:0 !important;margin-bottom: 15px\">\r\n                      <div class=\"col-2\">\r\n                          <img src=\"{{i.userphoto}}\" alt=\"\" srcset=\"\" id=\"pro\">\r\n                          \r\n                      </div>\r\n                      <div class=\"col-8 para\">\r\n                          <p> <b> {{i.username}}'s </b> added a story - \"{{i.story}}\"</p>\r\n                      </div>\r\n                      <div class=\"col-2 icon\">\r\n                          <ul>\r\n                              <li style=\"margin-right: 15px;\"><a class=\"\"><img src=\"../../assets/share.png\" alt=\"\"></a></li>\r\n                              <li class=\" w3-dropdown-hover select\"><a><img src=\"../../assets/select.png\"></a>\r\n                                  <div class=\"w3-dropdown-content w3-bar-block w3-border w3-card-4\">\r\n                                      <a class=\"w3-bar-item w3-button\">Edit</a>\r\n                                      <a  class=\"w3-bar-item w3-button\">Delete</a>\r\n                                  </div></li>\r\n                             \r\n                          </ul>\r\n                          \r\n                      </div>\r\n                  </div>\r\n                  <div  class=\" col-12\">\r\n                      <img class=\"col-12\" src=\"{{i.photo_data}}\" style=\"max-width:100%;height:auto;\"/>   \r\n                  </div>\r\n                  <div class=\"row col-12 displaycards\">\r\n                      <div class=\"row col-12 \">\r\n                          <div class=\"col-8 text-left\">\r\n                              <p  class=\"comment_P\"> {{i.story}}</p>\r\n                          </div>     \r\n                          <div class=\"col-4 text-right\">\r\n                              <i class=\"far fa-heart \" style=\"color:deeppink;\" (click)=\"setlike(i.photo_id)\"> <span>  {{i.num_likes}}</span></i>\r\n                              <i class=\"far fa-comment msgicon_top_story\" style=\"color:orange;\"  (click) = \"viewcomment(i.photo_id)\"  > <span> {{i.num_comments}}</span></i>                               \r\n                                  \r\n                          </div>     \r\n                      \r\n                      </div>\r\n                      <div class=\"row col-12 \">\r\n                          <br>\r\n                          <p class=\"small comment_P\"> {{i.time | date: 'medium'}} </p>\r\n                      </div>\r\n                      <div class=\"col-12 comment_div\" *ngIf=\"comment_div == true && divid == i.photo_id\">\r\n                              <ul *ngFor=\"let comment of i.comments \" class=\"col-12 text-left\">\r\n                                  <li class=\"col-12\">  <strong> {{comment.username}}</strong> just commented  </li>\r\n                                  <li class=\"col-12\">  <p class=\"small\" style=\"margin-left:0%\">  {{comment.time | date}}</p>  </li>\r\n                                  <li class=\"col-12\">  <p style=\"padding-bottom: 1%;\"> {{comment.body}}</p></li>\r\n                                  <i class=\"fa fa-thumbs-up\" aria-hidden=\"true\" (click) = \" add_counts_of_like(comment.comment_id)\"></i>\r\n                                  {{comment.num_likes}}\r\n  \r\n                                  <li class=\"col-12\">  <hr> </li> \r\n                              </ul>\r\n                              <br>\r\n                              <form class=\"row col-12\" (ngSubmit)=\"add_comment(com)\" #com=\"ngForm\">                        \r\n                                  <div class=\"col-8 form-group\">\r\n                                      <input type=\"hidden\"  name = \"photo_id\" id=\"photo_id\" [(ngModel)]=\"i.photo_id\" />\r\n                                      <input type=\"text\" class=\"form-control comment_input\" name = \"body\" id=\"Inputname\" [(ngModel)]=\"body\" />\r\n                                  </div>\r\n                                  <div class=\"col-4 form-group comment_button\">\r\n                                      \r\n                                      <button type=\"submit\" class=\"btn btn-primary small\">comment</button>\r\n                                  </div>    \r\n                              </form> \r\n                          </div>                     \r\n                  </div>\r\n                      \r\n          </div>\r\n        </div>\r\n    </div>\r\n    <br><br><br>\r\n    <div class=\"clearfix\"></div>\r\n    <div class=\"row\">\r\n          <app-navbar></app-navbar>\r\n      </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/guest-dashboard/guest-dashboard.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/guest-dashboard/guest-dashboard.component.ts ***!
  \**************************************************************/
/*! exports provided: GuestDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuestDashboardComponent", function() { return GuestDashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GuestDashboardComponent = /** @class */ (function () {
    function GuestDashboardComponent(http) {
        this.http = http;
        this.image_data = { status: '' };
        this.comment = { num_likes: '' };
        this.id = { photo_id: '' };
    }
    GuestDashboardComponent.prototype.ngOnInit = function () {
        this.guest_user_name = localStorage.getItem('guest_name');
        this.guest_user_profile_pic = localStorage.getItem('guest_profile_photo');
        console.log(this.guest_user_name);
        console.log(this.guest_user_profile_pic);
        this.comment_div = false;
        this.getstory_list();
    };
    GuestDashboardComponent.prototype.setlike = function (x) {
        var _this = this;
        this.id.photo_id = x;
        this.http.post("https://api.vowfest.com/photo/like/add", this.id, this.options).subscribe(function (data) {
            _this.getstory_list();
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GuestDashboardComponent.prototype.add_comment = function (c) {
        var _this = this;
        console.log(c.value);
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/add", c.value, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.getstory_list();
        }, function (error) {
            console.log(error);
            //  this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GuestDashboardComponent.prototype.viewcomment = function (x) {
        var _this = this;
        if (this.comment_div == false) {
            this.comment_div = true;
        }
        else {
            this.comment_div = false;
        }
        this.divid = x;
        this.http.post('https://api.vowfest.com/photo/comment/list', { photo_id: x }).subscribe(function (data) {
            _this.viewwcomments = data;
            console.log(_this.viewwcomments);
            _this.com = _this.viewwcomments.comments;
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GuestDashboardComponent.prototype.getstory_list = function () {
        var _this = this;
        this.wid = localStorage.getItem('guest_wedding_id');
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        this.http.post('https://api.vowfest.com/story/list', { 'wedding_id': this.wid }, { headers: header }).subscribe(function (data) {
            console.log('DATA RECIEVED: ', data);
            _this.image_data = data;
            if (_this.image_data.status == true) {
                _this.image_data2 = _this.image_data.stories_list;
                console.log(_this.image_data2);
                if (_this.image_data2) {
                    for (var _i = 0, _a = _this.image_data2; _i < _a.length; _i++) {
                        var comment_array = _a[_i];
                        console.log(comment_array.comments.length);
                        if (comment_array.comments.length == 0) {
                            _this.comment_div = false;
                        }
                        else {
                            _this.comment_div = true;
                        }
                    }
                }
            }
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GuestDashboardComponent.prototype.add_counts_of_like = function (x) {
        var _this = this;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/like/add", { comment_id: x }, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.getstory_list();
            //this.view_counts_of_like(x);
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GuestDashboardComponent.prototype.view_counts_of_like = function (x) {
        var _this = this;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/like/list", { comment_id: x }, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.comment_like = data;
            _this.comment.num_likes = _this.comment_like.num_likes;
            console.log(_this.comment.num_likes);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    GuestDashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-guest-dashboard',
            template: __webpack_require__(/*! ./guest-dashboard.component.html */ "./src/app/guest-dashboard/guest-dashboard.component.html"),
            styles: [__webpack_require__(/*! ./guest-dashboard.component.css */ "./src/app/guest-dashboard/guest-dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], GuestDashboardComponent);
    return GuestDashboardComponent;
}());



/***/ }),

/***/ "./src/app/guest-profile-update/guest-profile-update.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/guest-profile-update/guest-profile-update.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\nh1{\r\n    margin-top: 30px;\r\n    text-align: center;\r\n}\r\n\r\nh6,form{\r\n    margin-top: 30px;\r\n    text-align: center;\r\n}\r\n\r\nform{\r\n    height: 400px;\r\n}\r\n\r\nh5{\r\n    font-size: 20px;\r\n    letter-spacing: 2px;\r\n}\r\n\r\ntextarea{\r\n    width: 80%;\r\n    border: none;\r\n    overflow: auto;\r\n    outline: none;\r\n    border-color: transparent;\r\n}\r\n\r\ninput:focus{\r\n    outline: none;\r\n    border-bottom: 1px solid #ff1556;\r\n}\r\n\r\ninput[type=text]{\r\n    width: 80%;\r\n    border-bottom: 1px solid #ff1556;\r\n    /* margin-left: 30px; */\r\n    overflow: auto;\r\n    outline: none;\r\n    border-top:none;\r\n    border-left:none;\r\n    border-right:none;\r\n}\r\n\r\n.img span{\r\n    float: left;\r\n    margin-left: 30px;\r\n    margin-bottom: 2%;\r\n}\r\n\r\nbutton{\r\n    width: 90%;\r\n    margin-top: 60px;\r\n    background-color: #ff1556;\r\n    color: white;\r\n}\r\n\r\ntextarea{\r\n    border:solid 1px grey;\r\n    position: relative;\r\n}\r\n\r\n.img{\r\n    margin-top: 20px;\r\n    color: #ff1556;\r\n    font-weight: bold;\r\n}\r\n\r\n.arrow{\r\nfloat: left;\r\nfont-size: 30px;\r\nline-height: 80px;\r\n}\r\n\r\n.fa-arrow-left,.fa-arrow-left:active{\r\ncolor: orange;\r\ntext-decoration: none;\r\n}\r\n\r\n#done{\r\n    background-color: #ff1556;\r\n    color: white;\r\n    border: #ff1556;\r\n    width: 100%;\r\n    height: 50px;\r\n    font-size: 25px;\r\n    font-weight: bold;\r\n    position: absolute;\r\n    vertical-align: middle;\r\n    left: 0px;\r\n    position: fixed;\r\n    bottom: 0px;\r\n    text-decoration: none;\r\n}"

/***/ }),

/***/ "./src/app/guest-profile-update/guest-profile-update.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/guest-profile-update/guest-profile-update.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n      <div class=\"col-3 arrow\">\n          <a routerLink=\"/login\" class=\"fa fa-arrow-left\"></a>\n      </div>\n  </div>\n  <div class=\"row\">\n      <div class=\"col-12\">\n          <h1>Welcome {{name}} !</h1>\n      </div>\n  </div>\n  <div class=\"row\">\n      <div class=\"col-12\">\n          <h6>Let’s get you started!</h6>\n      </div>\n  </div>\n  <div class=\"row\">\n      <div class=\"col-12\">\n          <form  #imageForm=ngForm (ngSubmit)=\"OnSubmit(imageForm)\">\n              <input type=\"text\" name=\"phone_num\"  [(ngModel)]=\"phone_num\" placeholder=\"phone Number\" >\n              <h6></h6>\n             \n              <textarea cols=\"38\" rows=\"4\" placeholder=\"Relationship\" [(ngModel)]=\"pro.relationship\"  name=\"relationship\"></textarea>\n              \n              <div class=\"img\">\n                  <span>ADD A PHOTO</span> \n                  <input type=\"file\"  #Image accept=\"image/*\" (change)=\"handleFileInput($event.target.files)\">\n              </div>\n              <button id=\"done\" type=\"submit\">OK, PROCEED </button>\n          </form>   \n      </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/guest-profile-update/guest-profile-update.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/guest-profile-update/guest-profile-update.component.ts ***!
  \************************************************************************/
/*! exports provided: GuestProfileUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuestProfileUpdateComponent", function() { return GuestProfileUpdateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../profile.service */ "./src/app/profile.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GuestProfileUpdateComponent = /** @class */ (function () {
    function GuestProfileUpdateComponent(http, router, activatedroute, profile) {
        this.http = http;
        this.router = router;
        this.activatedroute = activatedroute;
        this.profile = profile;
        this.pro = { relationship: '', phone_num: '' };
        this.prof = { name: '', file: {} };
        this.data = [];
        this.fileToUpload = null;
        this.formData = new FormData();
    }
    GuestProfileUpdateComponent.prototype.ngOnInit = function () {
    };
    GuestProfileUpdateComponent.prototype.handleFileInput = function (file) {
        var _this = this;
        this.fileToUpload = file.item(0);
        console.log('File to upload: ', this.fileToUpload);
        this.prof.file = this.fileToUpload;
        this.profile.setData(this.fileToUpload);
        // Show image preview
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.imageUrl = event.target.result;
        };
        reader.readAsDataURL(this.fileToUpload);
    };
    GuestProfileUpdateComponent.prototype.OnSubmit = function (Image) {
        var _this = this;
        this.userdata = {
            relationship: Image.value.relationship,
            phone_num: Image.value.phone_num,
            profile_photo: this.imageUrl
        };
        console.log(this.userdata);
        this.formData.append('relationship', Image.value.relationship);
        this.formData.append('phone_num', Image.value.phone_num);
        this.formData.append('profile_photo', this.fileToUpload);
        localStorage.setItem('guest_profile_photo', this.imageUrl);
        console.log(this.formData);
        this.file = this.profile.getData();
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': localStorage.getItem('guest_invite_authtoken') });
        this.http.post('https://api.vowfest.com/guest/updateProfile', this.formData, { headers: header }).subscribe(function (data) {
            console.log(data);
            _this.wedding_data = data;
            localStorage.setItem('guest_wedding_id', _this.wedding_data.wedding_id);
            _this.router.navigate(['../../Guest_Dashboard/']);
        }, function (error) { return (console.log(error)); });
        // this.router.navigate(['../../dashboard/'])
    };
    GuestProfileUpdateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-guest-profile-update',
            template: __webpack_require__(/*! ./guest-profile-update.component.html */ "./src/app/guest-profile-update/guest-profile-update.component.html"),
            styles: [__webpack_require__(/*! ./guest-profile-update.component.css */ "./src/app/guest-profile-update/guest-profile-update.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"]])
    ], GuestProfileUpdateComponent);
    return GuestProfileUpdateComponent;
}());



/***/ }),

/***/ "./src/app/guest-signup/guest-signup.component.css":
/*!*********************************************************!*\
  !*** ./src/app/guest-signup/guest-signup.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {font-family: Arial, Helvetica, sans-serif;}\r\n* {box-sizing: border-box}\r\n/* Full-width input fields */\r\ninput[type=text], input[type=password] {\r\n  width: 100%;\r\n  padding: 15px;\r\n  margin: 5px 0 22px 0;\r\n  display: inline-block;\r\n  border: none;\r\n  background: #f1f1f1;\r\n}\r\ninput[type=text]:focus, input[type=password]:focus {\r\n  background-color: #ddd;\r\n  outline: none;\r\n}\r\nhr {\r\n  border: 1px solid #ff1556;\r\n  margin-bottom: 25px;\r\n}\r\n/* Set a style for all buttons */\r\nbutton {\r\n  background-color: #ff1556;\r\n  color: white;\r\n  padding: 14px 20px;\r\n  margin: 8px 0;\r\n  border: none;\r\n  cursor: pointer;\r\n  width: 100%;\r\n  opacity: 0.9;\r\n}\r\nbutton:hover {\r\n  opacity:1;\r\n}\r\n/* Extra styles for the cancel button */\r\n.cancelbtn {\r\n  padding: 14px 20px;\r\n  background-color: #ff1556;\r\n}\r\n/* Float cancel and signup buttons and add an equal width */\r\n.cancelbtn, .signupbtn {\r\n  float: left;\r\n  width: 50%;\r\n}\r\n/* Add padding to container elements */\r\n.container {\r\n  padding: 16px;\r\n}\r\n/* Clear floats */\r\n.clearfix::after {\r\n  content: \"\";\r\n  clear: both;\r\n  display: table;\r\n}\r\n/* Change styles for cancel button and signup button on extra small screens */\r\n@media screen and (max-width: 300px) {\r\n  .cancelbtn, .signupbtn {\r\n     width: 100%;\r\n  }\r\n}\r\n.email{\r\n  letter-spacing: .1rem;\r\n  font-weight: bold;\r\n  padding: 1rem;\r\n  margin-top: 10%;\r\n  /* width: 70%; */\r\n  text-align: center;\r\n  \r\n  }\r\n/* The Modal (background) */\r\n.modal {\r\n  display: none; /* Hidden by default */\r\n  position: fixed; /* Stay in place */\r\n  z-index: 1; /* Sit on top */\r\n  left: 0;\r\n  top: 0;\r\n  width: 100%; /* Full width */\r\n  height: 100%; /* Full height */\r\n  overflow: auto; /* Enable scroll if needed */\r\n  background-color: rgb(0,0,0); /* Fallback color */\r\n  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */\r\n  padding-top: 60px;\r\n}\r\n/* Modal Content/Box */\r\n.modal-content {\r\n  background-color: #fefefe;\r\n  margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */\r\n  border: 1px solid #888;\r\n  width: 80%; /* Could be more or less, depending on screen size */\r\n}\r\n/* The Close Button (x) */\r\n.close {\r\n  position: absolute;\r\n  right: 25px;\r\n  top: 0;\r\n  color: #000;\r\n  font-size: 35px;\r\n  font-weight: bold;\r\n}\r\n.close:hover,\r\n.close:focus {\r\n  color: red;\r\n  cursor: pointer;\r\n}\r\n/* Add Zoom Animation */\r\n.animate {\r\n  -webkit-animation: animatezoom 0.6s;\r\n  animation: animatezoom 0.6s\r\n}\r\n@-webkit-keyframes animatezoom {\r\n  from {-webkit-transform: scale(0)} \r\n  to {-webkit-transform: scale(1)}\r\n}\r\n@keyframes animatezoom {\r\n  from {-webkit-transform: scale(0);transform: scale(0)} \r\n  to {-webkit-transform: scale(1);transform: scale(1)}\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/guest-signup/guest-signup.component.html":
/*!**********************************************************!*\
  !*** ./src/app/guest-signup/guest-signup.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html>\n<html>\n<body >\n  <a routerLink=\"../login\">Back</a>&nbsp;\n  <a onclick=\"document.getElementById('id01').style.display='block'\" style=\"width:auto;color: blue\">login</a>\n\n<form (ngSubmit)=\"onSubmit(user)\" #f=\"ngForm\" style=\"border:1px solid #ccc\">\n  <div class=\"container\">\n    <h1 class=\"text-center\">Sign Up</h1>\n    <p class=\"text-center\">Please fill in this form to create an account.</p>\n  \n    <hr>\n    <label for=\"name\"><b>Name</b></label>\n    <!-- <input type=\"text\" placeholder=\"Enter Name\" name=\"name\" [(ngModel)]= \"namee\" required>\n    <label for=\"email\"><b>Email</b></label> -->\n    <input type=\"text\" placeholder=\"Enter name\" name=\"name\" [(ngModel)]= \"user.name\" required>\n    \n    <label for=\"email\"><b>Email</b></label>\n    <input type=\"text\" placeholder=\"Enter Email\" name=\"email\" [(ngModel)]= \"user.email\" required>\n    \n    <label for=\"password\"><b>Password</b></label>\n    <input type=\"password\" placeholder=\"Enter Password\" name=\"password\" [(ngModel)]= \"user.password\" required>\n    <div class=\"clearfix\" >\n      <button type=\"button\" class=\"cancelbtn\"><a routerLink=\"/login\">Cancel</a></button>\n      <button type=\"submit\" class=\"signupbtn\">Sign Up</button>\n    </div>\n  </div>\n</form>\n\n</body>\n</html>\n\n\n<div id=\"id01\" class=\"modal\">\n  <form class=\"modal-content animate\" (ngSubmit)=\"loginonSubmit(f)\" #f=\"ngForm\">\n <div class=\"imgcontainer\">\n   <span onclick=\"document.getElementById('id01').style.display='none'\" class=\"close\" title=\"Close Modal\">&times;</span>\n </div>\n\n <div class=\"container\">\n\n   <label for=\"email\"><b>Email</b></label>\n   <input type=\"text\" placeholder=\"Enter Email\" name=\"email\"[(ngModel)]= \"email\" required>\n   <font color=\"red\">{{password_error}}</font>\n\n   <label for=\"password\"><b>Password</b></label>\n   <input type=\"password\" placeholder=\"Enter Password\" name=\"password\"[(ngModel)]= \"password\"  required>\n   <font color=\"red\">{{non_field_errors}}</font>   \n   <button type=\"submit\">Login</button>\n </div>\n <!-- <label>\n   <input type=\"checkbox\" checked=\"checked\" name=\"remember\"> Remember me\n </label> -->\n <!-- <div class=\"container\" style=\"background-color:#f1f1f1\">\n   <button type=\"button\" onclick=\"document.getElementById('id01').style.display='none'\" class=\"cancelbtn\">Cancel</button>\n   <span class=\"psw\">Forgot <a href=\"#\">password?</a></span>\n </div> -->\n</form>\n</div>\n\n\n<script>\n  // Get the modal\n  var modal = document.getElementById('id01');\n  \n  // When the user clicks anywhere outside of the modal, close it\n  window.onclick = function(event) {\n      if (event.target == modal) {\n          modal.style.display = \"none\";\n      }\n  }\n</script>\n\n"

/***/ }),

/***/ "./src/app/guest-signup/guest-signup.component.ts":
/*!********************************************************!*\
  !*** ./src/app/guest-signup/guest-signup.component.ts ***!
  \********************************************************/
/*! exports provided: GuestSignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuestSignupComponent", function() { return GuestSignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GuestSignupComponent = /** @class */ (function () {
    function GuestSignupComponent(http, router, activatedroute) {
        this.http = http;
        this.router = router;
        this.activatedroute = activatedroute;
        this.user = { name: '', email: '', password: '' };
    }
    GuestSignupComponent.prototype.ngOnInit = function () {
        localStorage.setItem('login_type', "guest");
        console.log("dbd");
    };
    GuestSignupComponent.prototype.onSubmit = function (user) {
        var _this = this;
        console.log(user);
        console.log("dfds");
        localStorage.setItem('guest_name', user.name);
        debugger;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        this.http.post('https://api.vowfest.com/guest/createAccount', {
            email: user.email,
            password: user.password,
            name: user.name,
            wedding_id: this.activatedroute.snapshot.params['data']
        }, { headers: header }).subscribe(function (data) {
            console.log('response: ', data);
            _this.invite_authtoken = data;
            localStorage.setItem('guest_invite_authtoken', _this.invite_authtoken.token);
            _this.router.navigate(['../../Guest_Profile_Update']);
        }, function (error) {
            console.log(error);
            // this.router.(['../error', JSON.stringify(error)]);
        });
    };
    GuestSignupComponent.prototype.loginonSubmit = function (f) {
        var _this = this;
        console.log(f.value);
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        this.http.post('https://api.vowfest.com/login', f.value, { headers: header }).subscribe(function (data) {
            _this.a = data;
            console.log(_this.a);
            localStorage.setItem('userToken', _this.a.token);
            localStorage.setItem('userinfo', JSON.stringify(_this.a));
            localStorage.setItem('weddingID', _this.a.wedding_id);
            if (_this.a.wedding_id !== null) {
                // this.router.navigate(['../dashboard/']);
                _this.router.navigate(['../stories/']);
            }
            else {
                _this.router.navigate(['../profile/', JSON.stringify(data)]);
            }
            if (_this.a.wedding_id === undefined) {
                _this.router.navigate(['../profile/']);
            }
        }, function (error) {
            if (error.error.status == false) {
                _this.router.navigate(['../register']);
            }
            else {
                console.log(error);
                //this.router.navigate(['../error',JSON.stringify(error)]);
            }
            _this.msg = error.error.msg;
        });
    };
    GuestSignupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-guest-signup',
            template: __webpack_require__(/*! ./guest-signup.component.html */ "./src/app/guest-signup/guest-signup.component.html"),
            styles: [__webpack_require__(/*! ./guest-signup.component.css */ "./src/app/guest-signup/guest-signup.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], GuestSignupComponent);
    return GuestSignupComponent;
}());



/***/ }),

/***/ "./src/app/guestlist/guestlist.component.css":
/*!***************************************************!*\
  !*** ./src/app/guestlist/guestlist.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.jumbotron{\n    background-color: transparent;\n    padding-bottom: 0%;\n}\nhr{\n    width:10%;\n    background-color: brown;\n}\n.jumbo{\n    margin-top: 25%;\n}\n.fa-plus-circle{\n    color: orange;\n    font-size: 20px;\n}\n.topnav {\noverflow: hidden;\nmargin-left: 5%;\ntext-align: center;\n}\n.topnav a {\nfloat: left;\ndisplay: block;\ncolor: black;\ntext-align: center;\npadding: 14px 16px;\ntext-decoration: none;\nfont-size: 17px;\nborder-bottom: 3px solid transparent;\n}\n.topnav a:hover {\nborder-bottom: 3px solid red;\n}\n.topnav a.active {\nborder-bottom: 3px solid red;\n}"

/***/ }),

/***/ "./src/app/guestlist/guestlist.component.html":
/*!****************************************************!*\
  !*** ./src/app/guestlist/guestlist.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12\">\r\n        <app-navbar></app-navbar>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"topnav\">\r\n        <a routerLink=\"/invitation\"  [ngClass]=\"{'inactive': !shouldShow  }\">Invitation</a>\r\n        <a routerLink=\"/guest-list\"  [ngClass]=\"{ 'active': shouldShow, 'inactive': !shouldShow  }\">Guest list</a>\r\n        <a routerLink=\"/account\"  [ngClass]=\"{  'inactive': !shouldShow  }\">Account</a>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-12\">\r\n          <div class=\"jumbotron text-center jumbo\">\r\n              <p>Select contacts from your phone to invite to your Vowfest</p>\r\n              <i class=\"fas fa-plus-circle\" type=\"button\" (click)=\"addpic()\" >SELECT CONTACTS</i>\r\n          </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n "

/***/ }),

/***/ "./src/app/guestlist/guestlist.component.ts":
/*!**************************************************!*\
  !*** ./src/app/guestlist/guestlist.component.ts ***!
  \**************************************************/
/*! exports provided: GuestlistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuestlistComponent", function() { return GuestlistComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GuestlistComponent = /** @class */ (function () {
    function GuestlistComponent(router) {
        this.router = router;
        this.shouldShow = true;
    }
    GuestlistComponent.prototype.ngOnInit = function () {
    };
    GuestlistComponent.prototype.addpic = function () {
        this.router.navigate(['../Contact-list']);
    };
    GuestlistComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-guestlist',
            template: __webpack_require__(/*! ./guestlist.component.html */ "./src/app/guestlist/guestlist.component.html"),
            styles: [__webpack_require__(/*! ./guestlist.component.css */ "./src/app/guestlist/guestlist.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], GuestlistComponent);
    return GuestlistComponent;
}());



/***/ }),

/***/ "./src/app/images/images.component.css":
/*!*********************************************!*\
  !*** ./src/app/images/images.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {font-family: Arial, Helvetica, sans-serif;}\r\n\r\n/* Full-width input fields */\r\n\r\ninput[type=text], input[type=password] {\r\n  width: 100%;\r\n  padding: 12px 20px;\r\n  margin: 8px 0;\r\n  display: inline-block;\r\n  border: 1px solid #ccc;\r\n  box-sizing: border-box;\r\n}\r\n\r\n/* Set a style for all buttons */\r\n\r\nbutton {\r\n  background-color: white;\r\n  color: #ff1556;\r\n  padding: 14px 20px;\r\n  margin: 8px 0;\r\n  border: none;\r\n  cursor: pointer;\r\n  width: 100%;\r\n}\r\n\r\n.text{\r\n      padding-left: 20%;\r\n      padding-top: 27%;\r\n    }\r\n\r\nbutton:hover {\r\n  opacity: 0.8;\r\n}\r\n\r\n/* Extra styles for the cancel button */\r\n\r\n.cancelbtn {\r\n  width: auto;\r\n  padding: 10px 18px;\r\n  background-color: #f1f1f1;\r\n}\r\n\r\n/* Center the image and position the close button */\r\n\r\n.imgcontainer {\r\n  text-align: center;\r\n  margin: 24px 0 12px 0;\r\n  position: relative;\r\n}\r\n\r\n.img_class{\r\n   height:\"100%\" ;\r\n   width:\"100%\";\r\n   padding-top: 10%\r\n}\r\n\r\n.container {\r\n  padding: 16px;\r\n}\r\n\r\nspan.psw {\r\n  float: right;\r\n  padding-top: 16px;\r\n}\r\n\r\n/* The Modal (background) */\r\n\r\n.modal {\r\n  display: none; /* Hidden by default */\r\n  position: fixed; /* Stay in place */\r\n  z-index: 1; /* Sit on top */\r\n  left: 0;\r\n  top: 0;\r\n  width: 100%; /* Full width */\r\n  height: 100%; /* Full height */\r\n  overflow: auto; /* Enable scroll if needed */\r\n  background-color: rgb(0,0,0); /* Fallback color */\r\n  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */\r\n  padding-top: 60px;\r\n}\r\n\r\n/* Modal Content/Box */\r\n\r\n.modal-content {\r\n  background-color: #fefefe;\r\n  margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */\r\n  border: 1px solid #888;\r\n  width: 80%; /* Could be more or less, depending on screen size */\r\n}\r\n\r\n/* The Close Button (x) */\r\n\r\n.close {\r\n  position: absolute;\r\n  right: 25px;\r\n  top: 0;\r\n  color: #000;\r\n  font-size: 35px;\r\n  font-weight: bold;\r\n}\r\n\r\n#add{\r\n    position: absolute;\r\n    right:  20px;\r\n    width:150px;\r\n    bottom: 10px;\r\n    border: none;\r\n    /* top: 0; */\r\n    color: deeppink;\r\n    background-color: transparent;\r\n    /* font-size: 35px; */\r\n    font-weight: bold;\r\n}\r\n\r\n#add:hover,\r\n#add:focus{\r\n  border: 0px none;\r\n  outline: none;\r\n}\r\n\r\n.close:hover,\r\n.close:focus {\r\n  color: red;\r\n  cursor: pointer;\r\n  border: none;\r\n}\r\n\r\n/* Add Zoom Animation */\r\n\r\n.animate {\r\n  -webkit-animation: animatezoom 0.6s;\r\n  animation: animatezoom 0.6s\r\n}\r\n\r\n@-webkit-keyframes animatezoom {\r\n  from {-webkit-transform: scale(0)} \r\n  to {-webkit-transform: scale(1)}\r\n}\r\n\r\n@keyframes animatezoom {\r\n  from {-webkit-transform: scale(0);transform: scale(0)} \r\n  to {-webkit-transform: scale(1);transform: scale(1)}\r\n}\r\n\r\n.album_name{\r\n  text-align: center;\r\n}\r\n\r\n/* Change styles for span and cancel button on extra small screens */\r\n\r\n@media screen and (max-width: 300px) {\r\n  span.psw {\r\n     display: block;\r\n     float: none;\r\n  }\r\n  .cancelbtn {\r\n     width: 100%;\r\n     border:  1px solid blue;\r\n     background-color: #fefefe;\r\n  }\r\n}\r\n\r\n.jumbotron hr{\r\n    \r\n    width:20%;\r\n    background-color: brown;\r\n}\r\n\r\nnav{\r\n    background-color: lightpink;\r\n}\r\n\r\n.navbar a{\r\n    font-size: 30px;\r\n    color: deeppink;\r\n    text-align: center;\r\n}\r\n\r\n.jumbotron{\r\n    background-color: transparent;\r\n}\r\n\r\nhr{\r\n    width:10%;\r\n    background-color: brown;\r\n}\r\n\r\n.jumbo{\r\n    margin-top: 9%;\r\n    /* height: 84px; */\r\n}\r\n\r\n.border{\r\n    border: 2px solid black;\r\n    border-radius: 10%;\r\n}\r\n\r\n.fa-plus-circle{\r\n    color: orange;\r\n    font-size: 20px;\r\n    padding-top: 50%;\r\n    z-index: 1;\r\n}\r\n\r\n.curved{\r\n    border-radius: 10%;\r\n    margin-left: -7%;\r\n    margin-top: 3%;\r\n}\r\n\r\n.nopadd{\r\n  padding: 0px !important;\r\n}\r\n\r\n.color {\r\n  font-size: 20px;\r\n  color: deeppink;\r\n  margin-top: 20px;\r\n}\r\n\r\n.dropdown-content {\r\n  display: none;\r\n  position: relative;\r\n  /* margin-top: 10px; */\r\n  margin-right: -5px;\r\n  background-color: wheat;\r\n  min-width: 160px;\r\n  overflow: auto;\r\n  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\r\n  z-index: 1;\r\n}\r\n\r\n.dropdown-content a {\r\n  color: black;\r\n  padding: 12px 16px;\r\n  text-decoration: none;\r\n  display: block;\r\n}\r\n\r\n.fa-arrow-left,.fa-arrow-left:active{\r\n  color: deeppink;\r\n  text-decoration: none;\r\n}\r\n\r\n.image_box{\r\n  margin-top: 50px;\r\n}\r\n\r\n/* fdngvfdkngjkdfngn */\r\n\r\n"

/***/ }),

/***/ "./src/app/images/images.component.html":
/*!**********************************************!*\
  !*** ./src/app/images/images.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n  <div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n      <app-navbar></app-navbar>\r\n    </div>\r\n    <div class=\"col-12 row color\">\r\n      <div class=\"col-1\">\r\n        <a routerLink=\"/photo\" class=\"fas fa-arrow-left\"></a>\r\n      </div>\r\n      <div class=\"col-6 album_name\">\r\n        {{album_id.album_name }}\r\n      </div>\r\n      <div class=\"col-2\">\r\n        <a (click)=\"storigrid()\" *ngIf=\"stories_ui == true\" class=\"fas fa-th\"></a>\r\n        <a (click)=\"photogrid()\" *ngIf=\"photo_ui == true\" class=\"fas fa-th\"></a>\r\n      </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n      <div class=\"col-1\">\r\n        <i class=\" dropbtn icons fas fa-ellipsis-v\" onclick=\"showDropdown()\" style=\"margin-top:5px;\">\r\n        </i>\r\n        <!-- menu -->\r\n        <div id=\"myDropdown\" class=\"dropdown-content\">\r\n          <a>Send album for print</a>\r\n        </div>\r\n      </div>\r\n    </div>\r\n      <div class=\"col-12\" *ngIf=\"photo_ui == true\">\r\n        <div class=\"row\"  *ngIf = \"!validate\">\r\n          <div class=\"col-12 border jumbo\">\r\n            <button onclick=\"document.getElementById('id01').style.display='block'\" style=\"padding-top: 1%;\" class=\"fas fa-plus-circle\">Add Image </button>\r\n          </div>\r\n        </div>\r\n        <div class=\"row image_box\">\r\n          <div class=\"col-12 \" *ngFor=\"let item of image.photos_list\">\r\n            <a id=\"image\">\r\n              <img src=\"{{item.photo_data}}\" class= \"image_class\"   width=\"100%\" />\r\n            </a>\r\n            <br><br>\r\n            <p style=\"color:deeppink;margin-top: 1%;\"></p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-12\" *ngIf=\"stories_ui == true\">\r\n        <app-stories></app-stories>\r\n      </div>\r\n  </div>\r\n \r\n  <div id=\"id01\" class=\"modal\">\r\n    <form class=\"modal-content animate\" (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\r\n      <div class=\"imgcontainer\">\r\n        <span onclick=\"document.getElementById('id01').style.display='none'\" class=\"close\" title=\"Close Modal\">&times;</span>\r\n      </div>\r\n\r\n      <div class=\"container\">\r\n\r\n        <label for=\"photo\">\r\n          <b>Add Photo</b>\r\n        </label>\r\n        <input type=\"file\" #Image accept=\"image/*\" (change)=\"handleFileInput($event.target.files)\" multiple>\r\n\r\n        <button type=\"submit\" onclick=\"document.getElementById('id01').style.display='none'\" id=\"add\">Add Photos</button>\r\n      </div>\r\n\r\n      <div class=\"container\" style=\"background-color:#f1f1f1\">\r\n        <button type=\"button\" onclick=\"document.getElementById('id01').style.display='none'\" class=\"cancelbtn\">Cancel</button>\r\n      </div>\r\n    </form>\r\n  </div>\r\n\r\n    <div *ngIf = \"image.status == false \" class=\"col-12 row text\">\r\n     {{image.msg}}\r\n    <div>\r\n  <script>\r\n    // Get the modal\r\n    var modal = document.getElementById('id01');\r\n\r\n    // When the user clicks anywhere outside of the modal, close it\r\n    window.onclick = function (event) {\r\n      if (event.target == modal) {\r\n        modal.style.display = \"none\";\r\n      }\r\n    }\r\n  </script>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/images/images.component.ts":
/*!********************************************!*\
  !*** ./src/app/images/images.component.ts ***!
  \********************************************/
/*! exports provided: ImagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagesComponent", function() { return ImagesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ImagesComponent = /** @class */ (function () {
    function ImagesComponent(http, router, route) {
        this.http = http;
        this.router = router;
        this.route = route;
        this.data = { album_id: '', photos_data: '' };
        this.ProfileArray = [];
        this.album = [];
        this.image = { photos_list: '' };
        this.formData = new FormData();
        this.album_images = [];
        this.imageUrl = [];
        this.fileToUpload = [];
    }
    ImagesComponent.prototype.ngOnInit = function () {
        this.stories_ui = false;
        this.photo_ui = true;
        this.album_id = JSON.parse(this.route.snapshot.paramMap.get('data'));
        console.log(this.album_id.album_id);
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: this.headers });
        if (!authToken) {
            this.router.navigate(['../login']);
        }
        this.api();
        var user_login_type;
        user_login_type = JSON.parse(localStorage.getItem('userinfo'));
        user_login_type.login_type = localStorage.getItem('login_type');
        console.log(user_login_type);
        if (user_login_type.login_type == 'guest') {
            this.validate = true;
            console.log(this.validate);
        }
        else {
            this.validate = false;
        }
    };
    ImagesComponent.prototype.handleFileInput = function (file) {
        var _this = this;
        console.log('FILE: ', file);
        var _loop_1 = function (i) {
            var element = file[i];
            // console.log('%%%%%%%%%%', element);
            this_1.fileToUpload.push(element);
            var files = this_1.fileToUpload;
            console.log('@@@###', files[i]);
            var myReader = new FileReader();
            myReader.onloadend = function (e) {
                _this.img = myReader.result;
                _this.formData.append('photos_data', files[i], files[i].name);
                // tslint:disable-next-line:no-shadowed-variable
                _this.formData.forEach(function (element) {
                    console.log('@@@###@@@@', element);
                });
                // console.log( '@@@###@@@@', this.img);
            };
            myReader.readAsDataURL(element);
        };
        var this_1 = this;
        for (var i = 0; i < file.length; i++) {
            _loop_1(i);
        }
    };
    ImagesComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this.formData.append('album_id', this.album_id.album_id);
        this.formData.append('photos_data', this.fileToUpload);
        console.log("PHOTO DATA: ", this.formData.get("photos_data"));
        console.log(this.formData);
        this.http.post('https://api.vowfest.com/albums/photos/upload', this.formData, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) { return response; }))
            .subscribe(function (result) {
            console.log('######', result);
            _this.http.post('https://api.vowfest.com/albums/photos/list', { album_id: _this.album_id.album_id }, _this.options).subscribe(function (res) {
                console.log(res);
                _this.image = res;
                console.log(_this.image.photos_list);
            }, function (error) {
                console.log(error);
                //this.router.navigate(['../error', JSON.stringify(error)]);
            });
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
        //debugger;
    };
    ImagesComponent.prototype.showDropdown = function () {
        document.getElementById("myDropdown").classList.toggle("show");
    };
    ImagesComponent.prototype.storigrid = function () {
        this.stories_ui = false;
        this.photo_ui = true;
    };
    ImagesComponent.prototype.photolist = function () {
        var _this = this;
        this.http.post('https://api.vowfest.com/albums/photos/list', { "album_id": this.album_id.album_id }, this.options).subscribe(function (data) {
            _this.image = data;
            console.log(_this.image.photos_list);
        }, function (error) { console.log(error); });
    };
    ImagesComponent.prototype.photogrid = function () {
        this.stories_ui = true;
        this.photo_ui = false;
    };
    ImagesComponent.prototype.api = function () {
        var _this = this;
        this.http.post('https://api.vowfest.com/albums/photos/list', { album_id: this.album_id.album_id }, this.options).subscribe(function (res) {
            console.log(res);
            _this.image = res;
            console.log(_this.image.photos_list);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    ImagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-images',
            template: __webpack_require__(/*! ./images.component.html */ "./src/app/images/images.component.html"),
            styles: [__webpack_require__(/*! ./images.component.css */ "./src/app/images/images.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ImagesComponent);
    return ImagesComponent;
}());



/***/ }),

/***/ "./src/app/invitation/invitation.component.css":
/*!*****************************************************!*\
  !*** ./src/app/invitation/invitation.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.jumbotron{\n    background-color: transparent;\n    padding-bottom: 0%;\n}\nhr{\n    width:10%;\n    background-color: brown;\n}\n.jumbo{\n    margin-top: 25%;\n}\n.fa-plus-circle{\n    color: orange;\n    font-size: 20px;\n}\n.topnav {\noverflow: hidden;\nmargin-left: 5%;\ntext-align: center;\n}\n.topnav a {\nfloat: left;\ndisplay: block;\ncolor: black;\ntext-align: center;\npadding: 14px 16px;\ntext-decoration: none;\nfont-size: 17px;\nborder-bottom: 3px solid transparent;\n}\n.topnav a:hover {\nborder-bottom: 3px solid red;\n}\n.topnav a.active {\nborder-bottom: 3px solid red;\n}\n.send_text{\n    color: orange!important;\n}\n.image_grid{\n    width: 100%;\n    padding: 10%;\n\n}\n.select_grid{\n    padding-top: 15%;\n}\n.add_button{\n    padding-left: 25%;\n}\n.select{\n    margin-right: -20%;\n}"

/***/ }),

/***/ "./src/app/invitation/invitation.component.html":
/*!******************************************************!*\
  !*** ./src/app/invitation/invitation.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col-12\">\n      <app-navbar></app-navbar>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"topnav\">     \n      <a routerLink=\"/invitation\"  [ngClass]=\"{ 'active': shouldShow, 'inactive': !shouldShow  }\">Invitation</a>\n      <a routerLink=\"/guest-list\"  [ngClass]=\"{  'inactive': !shouldShow  }\">Guest list</a>\n      <a routerLink=\"/account\"  [ngClass]=\"{  'inactive': !shouldShow  }\">Account</a>\n    </div>\n    <div *ngIf=\" photo_select_grid == true \">\n      <div class=\"col-12 row  select_grid\">\n          <div class=\"col-10\">  <a  class=\"send_text\"> Select all </a> </div>\n          <!-- <div class=\"col-2\">  <a><img src=\"../../assets/select.png\"></a> </div> -->\n          <div class=\"col-1 w3-dropdown-hover select\">\n            \n            <a><img src=\"../../assets/select.png\"></a>\n            <div class=\"w3-dropdown-content w3-bar-block w3-border w3-card-4\">\n                <a class=\"w3-bar-item w3-button\">Edit</a>\n                <a  class=\"w3-bar-item w3-button\">Delete</a>\n            </div>\n        </div>\n      </div>\n      <div class=\"row\">\n          <div class=\"col-12 row \">\n              <div class=\"col-4\">  \n                <img class= \"image_grid\" src=\"https://cutt.ly/S1RKBX\">\n              </div>\n              <div class=\"col-4\">  \n                <img class= \"image_grid\" src=\"https://cutt.ly/S1RKBX\">\n              </div>\n              <div class=\"col-4\">\n                <img class= \"image_grid\" src=\"https://cutt.ly/S1RKBX\">\n              </div>\n          </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-12 row \">\n            <div class=\"col-4\">  \n              <img class= \"image_grid\" src=\"https://cutt.ly/S1RKBX\">\n            </div>\n            <div class=\"col-4\">  \n              <img class= \"image_grid\" src=\"https://cutt.ly/S1RKBX\">\n            </div>\n            <div class=\"col-4\">\n              <img class= \"image_grid\" src=\"https://cutt.ly/S1RKBX\">\n            </div>\n        </div>\n      </div>\n    </div>  \n\n    <div class=\"row\" *ngIf=\" photo_select_grid == false \">\n      <div class=\"col-12\">\n          <div class=\"jumbotron text-center jumbo\">\n              <p>Add pictures of your invitation card to send to your guests</p>\n          </div>\n      </div>\n    </div>\n    <div class=\"row add_button\">\n      <div class=\"col-12\">\n        <i class=\"fas fa-plus-circle\" type=\"button\" (click)=addpic()>ADD A PHOTO</i>\n      </div>\n    </div>\n  </div>\n</div>  "

/***/ }),

/***/ "./src/app/invitation/invitation.component.ts":
/*!****************************************************!*\
  !*** ./src/app/invitation/invitation.component.ts ***!
  \****************************************************/
/*! exports provided: InvitationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvitationComponent", function() { return InvitationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InvitationComponent = /** @class */ (function () {
    function InvitationComponent(http, router) {
        this.http = http;
        this.router = router;
        this.InviteArray = [];
        this.shouldShow = true;
        this.photo_select_grid = false;
    }
    InvitationComponent.prototype.ngOnInit = function () {
    };
    InvitationComponent.prototype.addpic = function () {
        if (this.photo_select_grid == false) {
            this.photo_select_grid = true;
        }
        else {
            this.photo_select_grid = false;
        }
        //this.router.navigate(['../SendInvitation']);
    };
    InvitationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invitation',
            template: __webpack_require__(/*! ./invitation.component.html */ "./src/app/invitation/invitation.component.html"),
            styles: [__webpack_require__(/*! ./invitation.component.css */ "./src/app/invitation/invitation.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], InvitationComponent);
    return InvitationComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".start{\n    height: 300px;\n    margin-top:-5%;\n    background-image: url('logoi.png');\n    background-repeat: no-repeat;\n    background-size: cover;\n    opacity: 0.9;\n}\n.logo{\n   text-align: center;\n   padding-top: 140px;\n   font-size: 22px;\n   color: #635e5e;\n   font-family: \"Avenir\";\n   letter-spacing: .1rem,\n}\n.image{\n    opacity: 0.5;\n    margin-top: 15%;\n}\n.btn-google {\ncolor:rgba(0, 0, 0, 0.54);\nbackground-color: white;\nmargin-top: 40px;\nborder: solid;\nfont-Family: \"Roboto\",\n}\n.submit{\n    text-align: center;\n}\n.btn {\nfont-size: 80%;\nletter-spacing: .1rem;\nfont-weight: bold;\npadding: 1rem;\ntransition: all 0.2s;\n}\n.email{\nletter-spacing: .1rem;\nfont-weight: bold;\npadding: 1rem;\nmargin-top: 10%;\n/* width: 70%; */\ntext-align: center;\n\n}\n.signup{\ntext-align: center;\n/* margin-top: ; */\nletter-spacing: .1rem;\nfont-weight: bold;\n/* width: 70%; */\npadding: 1rem;\ncolor: #263238;\nfont-family: \"Avenir\"\n}\nbody {font-family: Arial, Helvetica, sans-serif;}\n/* Full-width input fields */\ninput[type=text], input[type=password] {\n  width: 100%;\n  padding: 12px 20px;\n  margin: 8px 0;\n  display: inline-block;\n  border: 1px solid #ccc;\n  box-sizing: border-box;\n}\n.btn-info,.btn-danger{\ncolor: white !important;\n\n}\n/* Set a style for all buttons */\nbutton {\n  background-color: transparent;\n  color: #ff1556;\n  padding: 14px 20px;\n  margin: 8px 0;\n  border: none;\n  cursor: pointer;\n  width: 100%;\n}\nbutton:hover {\n  opacity: 0.8;\n}\n/* Extra styles for the cancel button */\n.cancelbtn {\n  width: auto;\n  padding: 10px 18px;\n  background-color: #f1f1f1;\n}\n/* Center the image and position the close button */\n.imgcontainer {\n  text-align: center;\n  margin: 24px 0 12px 0;\n  position: relative;\n}\n.container {\n  padding: 16px;\n}\nspan.psw {\n  float: right;\n  padding-top: 16px;\n}\n/* The Modal (background) */\n.modal {\n  display: none; /* Hidden by default */\n  position: fixed; /* Stay in place */\n  z-index: 1; /* Sit on top */\n  left: 0;\n  top: 0;\n  width: 100%; /* Full width */\n  height: 100%; /* Full height */\n  overflow: auto; /* Enable scroll if needed */\n  background-color: rgb(0,0,0); /* Fallback color */\n  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */\n  padding-top: 60px;\n}\n/* Modal Content/Box */\n.modal-content {\n  background-color: #fefefe;\n  margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */\n  border: 1px solid #888;\n  width: 80%; /* Could be more or less, depending on screen size */\n}\n/* The Close Button (x) */\n.close {\n  position: absolute;\n  right: 25px;\n  top: 0;\n  color: #000;\n  font-size: 35px;\n  font-weight: bold;\n}\n.close:hover,\n.close:focus {\n  color: red;\n  cursor: pointer;\n}\n/* Add Zoom Animation */\n.animate {\n  -webkit-animation: animatezoom 0.6s;\n  animation: animatezoom 0.6s\n}\n@-webkit-keyframes animatezoom {\n  from {-webkit-transform: scale(0)} \n  to {-webkit-transform: scale(1)}\n}\n@keyframes animatezoom {\n  from {-webkit-transform: scale(0);transform: scale(0)} \n  to {-webkit-transform: scale(1);transform: scale(1)}\n}\n/* Change styles for span and cancel button on extra small screens */\n/* @media screen and (max-width: 300px) {\n  span.psw {\n     display: block;\n     float: none;\n  }\n  .cancelbtn {\n     width: 100%;\n     background-color: #fefefe;\n  }\n} */"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" *ngIf=\"!msg; else error\">\n  <div class=\"row\">\n      <div class=\"col-12 start logo\">\n          <img src=\"../../assets/artboard_7.png\" alt=\"\" srcset=\"\" height=\"90\" width=\"60%\">\n          <p>For your best day</p>\n      </div>\n  </div>\n  <div class=\"row\">\n      <div class=\"col-offset-3 col-12 submit\">\n          <button (click)=\"socialSignIn('google')\" class=\"btn btn-lg btn-google btn-block\" type=\"submit\">\n            <i class=\"fab fa-google mr-2\"></i> \n              Sign up with Google\n          </button>\n          <!-- <fa name=\"cog\" animation=\"spin\"></fa> -->\n      </div>\n  </div>\n  <div class=\"row email\">\n      <div class=\"col-12\">\n          <button onclick=\"document.getElementById('id01').style.display='block'\" style=\"width:auto;\">Use email id to login</button>\n      </div>\n  </div>\n\n  <div class=\"row image\">\n    <div class=\"col-12\">\n        <img src=\"../../assets/line.jpeg\" alt=\"\" srcset=\"\" height=\"10px\" width=\"100%\">\n    </div>\n  </div>\n  <div class=\"row signup text-center\">\n      <div class=\"col-12\">\n          <span>Don't have an account? <a routerLink=\"/register\" style=\"color: #ff1556; font-family:Avenir;\">Sign up</a></span> \n      </div>\n  </div>\n</div>\n<ng-template #error>\n    {{msg}}\n  </ng-template>\n<!DOCTYPE html>\n<html>\n<head>\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n<style>\n\n</style>\n</head>\n<body>\n\n\n\n<div id=\"id01\" class=\"modal\">\n     <form class=\"modal-content animate\" (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\n    <div class=\"imgcontainer\">\n      <span onclick=\"document.getElementById('id01').style.display='none'\" class=\"close\" title=\"Close Modal\">&times;</span>\n    </div>\n\n    <div class=\"container\">\n\n      <label for=\"email\"><b>Email</b></label>\n      <input type=\"text\" placeholder=\"Enter Email\" name=\"email\"[(ngModel)]= \"email\" required>\n      <font color=\"red\">{{password_error}}</font>\n\n      <label for=\"password\"><b>Password</b></label>\n      <input type=\"password\" placeholder=\"Enter Password\" name=\"password\"[(ngModel)]= \"password\"  required>\n      <font color=\"red\">{{non_field_errors}}</font>   \n      <button type=\"submit\">Login</button>\n    </div>\n    <!-- <label>\n      <input type=\"checkbox\" checked=\"checked\" name=\"remember\"> Remember me\n    </label> -->\n    <div class=\"container\" style=\"background-color:#f1f1f1\">\n      <button type=\"button\" onclick=\"document.getElementById('id01').style.display='none'\" class=\"cancelbtn\">Cancel</button>\n      <span class=\"psw\">Forgot <a href=\"#\">password?</a></span>\n    </div>\n  </form>\n</div>\n \n\n<script>\n// Get the modal\nvar modal = document.getElementById('id01');\n\n// When the user clicks anywhere outside of the modal, close it\nwindow.onclick = function(event) {\n    if (event.target == modal) {\n        modal.style.display = \"none\";\n    }\n}\n</script>\n\n</body>\n</html>\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-6-social-login-v2 */ "./node_modules/angular-6-social-login-v2/angular-6-social-login-v2.umd.js");
/* harmony import */ var angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../profile.service */ "./src/app/profile.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(socialAuthService, http, router, pro) {
        this.socialAuthService = socialAuthService;
        this.http = http;
        this.router = router;
        this.pro = pro;
        this.userdata = { name: '', email: '', password: '' };
        this.userdata_google_signin = { email: '', password: '' };
    }
    LoginComponent.prototype.socialSignIn = function (socialPlatform) {
        var _this = this;
        var socialPlatformProvider;
        if (socialPlatform == "google") {
            socialPlatformProvider = angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_3__["GoogleLoginProvider"].PROVIDER_ID;
        }
        this.socialAuthService.signIn(socialPlatformProvider).then(function (userData) {
            console.log(socialPlatform + " sign in data : ", userData);
            // Now sign-in with userData
            // ...
            _this.userdata.name = userData.name;
            _this.userdata.email = userData.email;
            _this.userdata.password = userData.email;
            _this.userdata_google_signin.password = userData.email;
            _this.userdata_google_signin.email = userData.email;
            _this.signupwith_google();
        });
    };
    LoginComponent.prototype.ngOnInit = function () {
        localStorage.clear();
    };
    LoginComponent.prototype.onSubmit = function (f) {
        var _this = this;
        //  f.value.email = this.userdata.email;
        //  f.value.password = this.userdata.password;
        //  f.value.name = this.userdata.name; 
        console.log(f.value);
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        this.http.post('https://api.vowfest.com/login', f.value, { headers: header }).subscribe(function (data) {
            _this.a = data;
            console.log(_this.a);
            localStorage.setItem('userToken', _this.a.token);
            localStorage.setItem('userinfo', JSON.stringify(_this.a));
            localStorage.setItem('weddingID', _this.a.wedding_id);
            if (_this.a.wedding_id !== null) {
                if (_this.a.login_type == 'host') {
                    _this.router.navigate(['../stories/']);
                }
                else {
                    _this.router.navigate(['../dashboard/']);
                }
            }
            else {
                _this.router.navigate(['../profile/', JSON.stringify(data)]);
            }
            if (_this.a.wedding_id === undefined) {
                _this.router.navigate(['../profile/']);
            }
        }, function (error) {
            if (error.error.status == false) {
                _this.router.navigate(['../register']);
            }
            else {
                console.log(error);
                //this.router.navigate(['../error',JSON.stringify(error)]);
            }
            _this.msg = error.error.msg;
        });
    };
    LoginComponent.prototype.signupwith_google = function () {
        var _this = this;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        console.log(this.userdata);
        this.http.post('https://api.vowfest.com/createAccount', this.userdata, { headers: header }).subscribe(function (data) {
            console.log('response: ', data);
            _this.a = data;
            localStorage.setItem('userToken', _this.a.token);
            localStorage.setItem('weddingID', _this.a.wedding_id);
            _this.router.navigate(['../profile/', JSON.stringify(_this.userdata)]);
            alert('You are registered now');
        }, function (error) {
            console.log(error);
            console.log(_this.userdata_google_signin);
            var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
            header.append('Access-Control-Allow-Origin', '*');
            _this.http.post('https://api.vowfest.com/login', { email: _this.userdata_google_signin.email, password: _this.userdata_google_signin.email }, { headers: header }).subscribe(function (data) {
                _this.a = data;
                console.log(_this.a);
                localStorage.setItem('userToken', _this.a.token);
                localStorage.setItem('userinfo', JSON.stringify(_this.a));
                localStorage.setItem('weddingID', _this.a.wedding_id);
                if (_this.a.wedding_id !== null) {
                    // this.router.navigate(['../dashboard/']);
                    _this.router.navigate(['../stories/']);
                }
                else {
                    _this.router.navigate(['../profile/', JSON.stringify(data)]);
                }
                if (_this.a.wedding_id === undefined) {
                    _this.router.navigate(['../profile/']);
                }
            }, function (error) {
                if (error.error.status == false) {
                    _this.router.navigate(['../register']);
                }
                else {
                    console.log(error);
                    //this.router.navigate(['../error',JSON.stringify(error)]);
                }
                _this.msg = error.error.msg;
            });
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")],
            providers: [angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_3__["SocialLoginModule"], angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_3__["AuthService"]]
        }),
        __metadata("design:paramtypes", [angular_6_social_login_v2__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/navbar/navbar.component.css":
/*!*********************************************!*\
  !*** ./src/app/navbar/navbar.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nav{\n    background-color: #ff1556;\n    width: 100%;\n}\n.navbar a{\n    font-size: 24px;\n    color: white;\n    text-align: center;\n  \n}\n\n"

/***/ }),

/***/ "./src/app/navbar/navbar.component.html":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <nav class=\"col-12 navbar fixed-bottom navbar-expand-md navbar-light\">\r\n        <div class=\"col-2\">\r\n          <a class=\"nav-item nav-link\" (click)=\"rout()\"><i class=\"fas fa-scroll\"></i></a>\r\n      \r\n        </div>\r\n        <div class=\"col-2\">\r\n            <a class=\"nav-item nav-link\" routerLink=\"/photo\"><i class=\"fas fa-grip-horizontal\"></i></a>\r\n        </div>\r\n        <div class=\"col-2\">\r\n            <a class=\"nav-item nav-link\" routerLink=\"/camera\"><i class=\"fas fa-plus\"></i></a>\r\n        </div>\r\n        <div class=\"col-2\">\r\n            <!-- routerLink=\"/calendar\" -->\r\n            <a class=\"nav-item nav-link\" (click)= \"calendar()\" ><i class=\"fas fa-calendar-week\"></i></a>\r\n        </div>\r\n        <div class=\"col-2\">\r\n            <a class=\"nav-item nav-link\" routerLink=\"/gift\"><i class=\"fas fa-gift\"></i></a>\r\n        </div>\r\n      </nav>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/navbar/navbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(router, http) {
        this.router = router;
        this.http = http;
        this.edata = [];
        this.wedding_ids = { wedding_id: '' };
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.rout = function () {
        if (localStorage.getItem('guest_invites') == 'true') {
            this.router.navigate(['../stories']);
        }
        else {
            this.router.navigate(['../dashboard']);
        }
    };
    NavbarComponent.prototype.calendar = function () {
        var _this = this;
        this.wedding_ids.wedding_id = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        this.http.post('https://api.vowfest.com/event/list', this.wedding_ids).subscribe(function (data) {
            _this.eventdata = data;
            if (_this.eventdata.status == true) {
                _this.router.navigate(['../event-list']);
            }
            else {
                _this.router.navigate(['../calendar']);
            }
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/phonenumber/phonenumber.component.css":
/*!*******************************************************!*\
  !*** ./src/app/phonenumber/phonenumber.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\nh1{\r\n    margin-top: 30px;\r\n    text-align: center;\r\n}\r\n\r\nh6,form{\r\n    margin-top: 30px;\r\n    text-align: center;\r\n}\r\n\r\nform{\r\n    height: 400px;\r\n}\r\n\r\nh5{\r\n    font-size: 20px;\r\n    letter-spacing: 2px;\r\n}\r\n\r\ntextarea{\r\n    width: 80%;\r\n    border: none;\r\n    overflow: auto;\r\n    outline: none;\r\n    border-color: transparent;\r\n    background-color: transparent;\r\n}\r\n\r\ninput:focus{\r\n    outline: none;\r\n    border-bottom: 1px solid #ff1556;\r\n}\r\n\r\ninput[type=number]{\r\n    width: 80%;\r\n    border-bottom: 1px solid #ff1556;\r\n    /* margin-left: 30px; */\r\n    overflow: auto;\r\n    outline: none;\r\n    border-top:none;\r\n    border-left:none;\r\n    border-right:none;\r\n}\r\n\r\nbutton{\r\n    width: 90%;\r\n    margin-top: 60px;\r\n    background-color: #ff1556;\r\n    color: white;\r\n}\r\n\r\ntextarea{\r\n    border:solid 1px grey;\r\n    position: relative;\r\n}\r\n\r\n.img{\r\n    top: 10px;\r\n    color: #ff1556;\r\n    font-weight: bold;\r\n}\r\n\r\n.arrow{\r\nfloat: left;\r\nfont-size: 30px;\r\nline-height: 80px;\r\n}\r\n\r\n.fa-arrow-left,.fa-arrow-left:active{\r\ncolor: orange;\r\ntext-decoration: none;\r\n}\r\n\r\n#done{\r\n    background-color: #ff1556;\r\n    color: white;\r\n    border: #ff1556;\r\n    width: 100%;\r\n    height: 50px;\r\n    font-size: 25px;\r\n    font-weight: bold;\r\n    position: fixed;\r\n    bottom: 0%;\r\n    vertical-align: middle;\r\n    left: 0px;\r\n    text-decoration: none;\r\n}\r\n\r\n/*spinner */\r\n\r\n#spinner {\r\n\t-webkit-animation: frames 1s infinite linear;\r\n\tanimation: frames 1s infinite linear;\r\n\tbackground: transparent;\r\n\tborder: 1.75vw solid #FFF;\r\n\tborder-radius: 100%;\r\n\tborder-top-color: #DF691A;\r\n\twidth: 20vw;\r\n\theight: 20vw;\r\n\topacity: .6;\r\n\tpadding: 0;\r\n\tposition: absolute;\r\n\tz-index: 999;\r\n}\r\n\r\n@-webkit-keyframes frames {\r\n  0% {\r\n\t-webkit-transform: rotate(0deg);\r\n\ttransform: rotate(0deg);\r\n  }\r\n  100% {\r\n\t-webkit-transform: rotate(359deg);\r\n\ttransform: rotate(359deg);\r\n  }\r\n}\r\n\r\n@keyframes frames {\r\n  0% {\r\n\t-webkit-transform: rotate(0deg);\r\n\ttransform: rotate(0deg);\r\n  }\r\n  100% {\r\n\t-webkit-transform: rotate(359deg);\r\n\ttransform: rotate(359deg);\r\n  }\r\n}\r\n\r\n#pause {\r\n\tdisplay: block;\r\n\tbackground:\r\n\t\trgba(0, 0, 0, 0.66)\r\n\t\tno-repeat\r\n\t\t0 0;\r\n\twidth: 100%;\r\n\theight: 100%;\r\n\tposition: fixed;\r\n\tbottom: 0;\r\n\tleft: 0;\r\n\tz-index: 1000;\r\n}\r\n"

/***/ }),

/***/ "./src/app/phonenumber/phonenumber.component.html":
/*!********************************************************!*\
  !*** ./src/app/phonenumber/phonenumber.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n      <div class=\"col-3 arrow\">\r\n          <a routerLink=\"/profile\" class=\"fa fa-arrow-left\"></a>\r\n      </div>\r\n  </div>\r\n  <div class=\"row\">\r\n      <div class=\"col-12\">\r\n          <h1>Welcome {{value.name}} !</h1>\r\n      </div>\r\n  </div>\r\n  <div class=\"row\">\r\n      <div class=\"col-12\">\r\n          <h6>You’re almost done\r\n          </h6>\r\n      </div>\r\n  </div>\r\n  <div class=\"row\">\r\n      <div class=\"col-12\">\r\n          <form action=\"page2.2.html\">\r\n              <input type=\"number\" placeholder=\"Your Phone Number\" [(ngModel)]=\"data.phone_num\" [ngModelOptions]=\"{standalone: true}\" required>\r\n              <button id=\"done\" (click)=sub() type=\"button\" >Done</button>\r\n          </form>   \r\n      </div>\r\n  </div>\r\n</div>\r\n<div id=\"pause\" class=\"d-flex align-items-center justify-content-center\" *ngIf = \"spinner == true\">\r\n    <div id=\"spinner\"></div>\r\n</div>"

/***/ }),

/***/ "./src/app/phonenumber/phonenumber.component.ts":
/*!******************************************************!*\
  !*** ./src/app/phonenumber/phonenumber.component.ts ***!
  \******************************************************/
/*! exports provided: PhonenumberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhonenumberComponent", function() { return PhonenumberComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../profile.service */ "./src/app/profile.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import swal from 'sweetalert2';
var PhonenumberComponent = /** @class */ (function () {
    //public toastr: ToastrService,
    function PhonenumberComponent(http, router, activatedroute, profile) {
        this.http = http;
        this.router = router;
        this.activatedroute = activatedroute;
        this.profile = profile;
        this.name = [];
        this.xyz = [];
        this.formData = new FormData();
        this.fileToUpload = [];
        this.data = { profile_photo: '', name: '', about: '', better_half: '', phone_num: '' };
        this.value = { name: '', better_half: '' };
        this.spinner = false;
    }
    PhonenumberComponent.prototype.ngOnInit = function () {
        var _this = this;
        var authToken = localStorage.getItem('userToken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
        this.name.push(JSON.parse(this.activatedroute.snapshot.params['data']));
        console.log(this.name);
        this.name.forEach(function (element) {
            _this.value.name = element.name;
            _this.value.better_half = element.better_half;
            //this.formData.append('about', element.name);
            _this.formData.append('about', element.about);
            _this.formData.append('better_half', element.better_half);
        });
        this.file = this.profile.getData();
        console.log("!!!", this.file);
    };
    PhonenumberComponent.prototype.sub = function () {
        var _this = this;
        this.spinner = true;
        var guest_invite = localStorage.getItem('guest_invites');
        this.formData.append('phone_num', this.data.phone_num);
        this.formData.append('profile_photo', this.file);
        var authToken = localStorage.getItem('userToken');
        var headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', authToken);
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken });
        this.http.post('https://api.vowfest.com/updateProfile', this.formData, { headers: this.headers }).subscribe(function (data) {
            _this.wedding_id = data.wedding_id;
            localStorage.setItem('weddingID', _this.wedding_id);
            _this.spinner = false;
            _this.router.navigate(['/dashboard/']);
        }, function (error) {
            console.log(error);
        });
    };
    PhonenumberComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-phonenumber',
            template: __webpack_require__(/*! ./phonenumber.component.html */ "./src/app/phonenumber/phonenumber.component.html"),
            styles: [__webpack_require__(/*! ./phonenumber.component.css */ "./src/app/phonenumber/phonenumber.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"]])
    ], PhonenumberComponent);
    return PhonenumberComponent;
}());



/***/ }),

/***/ "./src/app/photo/photo.component.css":
/*!*******************************************!*\
  !*** ./src/app/photo/photo.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {font-family: Arial, Helvetica, sans-serif;}\r\n\r\n/* Full-width input fields */\r\n\r\ninput[type=text], input[type=password] {\r\n  width: 100%;\r\n  padding: 12px 20px;\r\n  margin: 8px 0;\r\n  display: inline-block;\r\n  border: 1px solid #ccc;\r\n  box-sizing: border-box;\r\n}\r\n\r\n/* Set a style for all buttons */\r\n\r\nbutton {\r\n  background-color: white;\r\n  color: #ff1556;\r\n  padding: 14px 20px;\r\n  margin: 8px 0;\r\n  border: none;\r\n  cursor: pointer;\r\n  width: 100%;\r\n}\r\n\r\nbutton:hover {\r\n  opacity: 0.8;\r\n}\r\n\r\n/* Extra styles for the cancel button */\r\n\r\n.cancelbtn {\r\n  width: auto;\r\n  padding: 10px 18px;\r\n  background-color: #f1f1f1;\r\n}\r\n\r\n/* Center the image and position the close button */\r\n\r\n.imgcontainer {\r\n  text-align: center;\r\n  margin: 24px 0 12px 0;\r\n  position: relative;\r\n}\r\n\r\n.container {\r\n  padding: 16px;\r\n}\r\n\r\nspan.psw {\r\n  float: right;\r\n  padding-top: 16px;\r\n}\r\n\r\n/* The Modal (background) */\r\n\r\n.modal {\r\n  display: none; /* Hidden by default */\r\n  position: fixed; /* Stay in place */\r\n  z-index: 1; /* Sit on top */\r\n  left: 0;\r\n  top: 0;\r\n  width: 100%; /* Full width */\r\n  height: 100%; /* Full height */\r\n  overflow: auto; /* Enable scroll if needed */\r\n  background-color: rgb(0,0,0); /* Fallback color */\r\n  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */\r\n  padding-top: 60px;\r\n}\r\n\r\n/* Modal Content/Box */\r\n\r\n.modal-content {\r\n  background-color: #fefefe;\r\n  margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */\r\n  border: 1px solid #888;\r\n  width: 80%; /* Could be more or less, depending on screen size */\r\n}\r\n\r\n/* The Close Button (x) */\r\n\r\n.close {\r\n  position: absolute;\r\n  right: 25px;\r\n  top: 0;\r\n  color: #000;\r\n  font-size: 35px;\r\n  font-weight: bold;\r\n}\r\n\r\n#add{\r\n    position: absolute;\r\n    right:  20px;\r\n    width:150px;\r\n    bottom: 10px;\r\n    border: none;\r\n    /* top: 0; */\r\n    color: deeppink;\r\n    background-color: transparent;\r\n    /* font-size: 35px; */\r\n    font-weight: bold;\r\n}\r\n\r\n#add:hover,\r\n#add:focus{\r\n  border: 0px none;\r\n  outline: none;\r\n}\r\n\r\n.close:hover,\r\n.close:focus {\r\n  color: red;\r\n  cursor: pointer;\r\n  border: none;\r\n}\r\n\r\n/* Add Zoom Animation */\r\n\r\n.animate {\r\n  -webkit-animation: animatezoom 0.6s;\r\n  animation: animatezoom 0.6s\r\n}\r\n\r\n@-webkit-keyframes animatezoom {\r\n  from {-webkit-transform: scale(0)} \r\n  to {-webkit-transform: scale(1)}\r\n}\r\n\r\n@keyframes animatezoom {\r\n  from {-webkit-transform: scale(0);transform: scale(0)} \r\n  to {-webkit-transform: scale(1);transform: scale(1)}\r\n}\r\n\r\n.album_heading{\r\n  width: 100%;\r\n  text-align: center;\r\n  margin: 40px 0;\r\n  position: relative;\r\n}\r\n\r\n/* Change styles for span and cancel button on extra small screens */\r\n\r\n@media screen and (max-width: 300px) {\r\n  span.psw {\r\n     display: block;\r\n     float: none;\r\n  }\r\n  .cancelbtn {\r\n     width: 100%;\r\n     border:  1px solid blue;\r\n     background-color: #fefefe;\r\n  }\r\n}\r\n\r\n.jumbotron hr{\r\n    width:20%;\r\n    background-color: brown;\r\n}\r\n\r\nnav{\r\n    background-color: lightpink;\r\n}\r\n\r\n.navbar a{\r\n    font-size: 30px;\r\n    color: deeppink;\r\n    text-align: center;\r\n}\r\n\r\n.jumbotron{\r\n    background-color: transparent;\r\n}\r\n\r\nhr{\r\n    width:10%;\r\n    background-color: #c20238;\r\n    margin-top: 10px;\r\n}\r\n\r\n.jumbo{\r\n    margin-top: 2%;\r\n    height: 71px;\r\n}\r\n\r\n.border{\r\n    border: 2px solid deeppink !important;\r\n    border-radius: 10%;\r\n    height: 140px;\r\n    line-height: 140px;\r\n    margin-top: 4px;\r\n}\r\n\r\n.fa-plus-circle{\r\n    color: #ff1556;\r\n    font-size: 20px;\r\n    padding-top: 50%;\r\n    z-index: 1;\r\n}\r\n\r\n.curved{\r\n    border-radius: 10%;\r\n    margin-top: 3%;\r\n}\r\n\r\n.small{\r\n  color:#b6c3c9;\r\n  font-size: 12px;\r\n}\r\n\r\n.album_name{\r\n  color:deeppink;\r\n  margin: 10px 0px 5px 0;\r\n}\r\n\r\n.album_heading::after {\r\n  content: \"\";\r\n  position: absolute;\r\n  bottom: 0;\r\n  left: 0;\r\n  top: 50px;\r\n  right: 0;\r\n  height: 2px;\r\n  width: 60px;\r\n  background: #ff1556;\r\n  margin: 0 auto;\r\n}\r\n"

/***/ }),

/***/ "./src/app/photo/photo.component.html":
/*!********************************************!*\
  !*** ./src/app/photo/photo.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n\r\n</head>\r\n\r\n<body>\r\n\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <app-navbar></app-navbar>\r\n        </div>\r\n        <h1 class=\"album_heading\">Albums</h1>\r\n        <p *ngIf = \"validate\">Your Host Album </p>\r\n        <!-- <div class=\"jumbotron col-6\">\r\n         \r\n            <div class=\"row\">\r\n                <div class=\"col-12 border \">\r\n                    <button onclick=\"document.getElementById('id01').style.display='block'\" style=\"padding:1%;\" class=\"fas fa-plus-circle\">Add Album</button>\r\n                </div>\r\n               \r\n            </div>\r\n           \r\n        </div>\r\n        <div class=\"row col-6\">\r\n            <div class=\" \" *ngFor=\"let item of album\">\r\n                <div (click)=\"rendertoImages(item)\">\r\n                  <a id=\"image\"><img src=\"{{item.cover_photo}}\" class=\"curved\"  height=\"140px\" width=\"100%\"></a> <br>\r\n                  <p style=\"color:deeppink;margin-top: 1%;\">{{item.album_name}}</p>\r\n                </div>\r\n              </div>\r\n        </div> -->\r\n\r\n        <div class=\" row\">\r\n            <div class=\"col-6 \" *ngIf = \"!validate\">\r\n                <div class=\"border \" >\r\n                    <button onclick=\"document.getElementById('id01').style.display='block'\" style=\"padding:1%;\" class=\"fas fa-plus-circle\"></button>\r\n                </div>\r\n                <p style=\"color:deeppink;margin-top: 1%;\">New Album</p>\r\n            </div>\r\n            <div class=\"col-6\" *ngFor=\"let item of album\">\r\n                <div (click)=\"rendertoImages(item)\">\r\n                    <a id=\"image\">\r\n                        <img src=\"{{item.cover_photo}}\" class=\"curved\" height=\"140px\" width=\"100%\">\r\n                    </a>\r\n                    <br>\r\n                    <p class=\"album_name\">{{item.album_name}}</p>\r\n                    <p class=\"small\">{{item.image_count || 0}} photos</p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <div id=\"id01\" class=\"modal\">\r\n\r\n        <form class=\"modal-content animate\" (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\r\n            <div class=\"imgcontainer\">\r\n                <span onclick=\"document.getElementById('id01').style.display='none'\" class=\"close\" title=\"Close Modal\">&times;</span>\r\n            </div>\r\n\r\n            <div class=\"container\">\r\n                <label for=\"name\">\r\n                    <b>Album</b>\r\n                </label>\r\n                <input type=\"text\" placeholder=\"Name\" name=\"album_name\" [(ngModel)]=\"album_name\" required>\r\n                <input type=\"hidden\" name=\"album_id\">\r\n\r\n                <label for=\"photo\">\r\n                    <b>Cover photo</b>\r\n                </label>\r\n                <input type=\"file\" #Image accept=\"image/*\" (change)=\"handleFileInput($event.target.files)\">\r\n\r\n                <button type=\"submit\" onclick=\"document.getElementById('id01').style.display='none'\" id=\"add\">Add Album</button>\r\n            </div>\r\n\r\n            <div class=\"container\" style=\"background-color:#f1f1f1\">\r\n                <button type=\"button\" onclick=\"document.getElementById('id01').style.display='none'\" class=\"cancelbtn\">Cancel</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n\r\n    <script>\r\n        // Get the modal\r\n        var modal = document.getElementById('id01');\r\n\r\n        // When the user clicks anywhere outside of the modal, close it\r\n        window.onclick = function (event) {\r\n            if (event.target == modal) {\r\n                modal.style.display = \"none\";\r\n            }\r\n        }\r\n    </script>\r\n\r\n</body>\r\n\r\n</html>"

/***/ }),

/***/ "./src/app/photo/photo.component.ts":
/*!******************************************!*\
  !*** ./src/app/photo/photo.component.ts ***!
  \******************************************/
/*! exports provided: PhotoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoComponent", function() { return PhotoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PhotoComponent = /** @class */ (function () {
    function PhotoComponent(http, router) {
        this.http = http;
        this.router = router;
        this.ProfileArray = [];
        this.album = [];
        this.image = [];
        this.id = { album_id: '' };
        this.fileToUpload = null;
    }
    PhotoComponent.prototype.ngOnInit = function () {
        var _this = this;
        ///for guest user 
        var user_login_type = { login_type: '' };
        console.log(localStorage.getItem('userinfo'));
        user_login_type = JSON.parse(localStorage.getItem('userinfo'));
        user_login_type.login_type = localStorage.getItem('login_type');
        if (user_login_type.login_type == 'guest') {
            this.validate = true;
            console.log(this.validate);
        }
        else {
            console.log(this.validate);
            this.validate = false;
        }
        ///end
        var wedding_id1 = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
        // if (!authToken) {  this.router.navigate(['../login']);
        //  }
        console.log('WEDDINGID: ', wedding_id1);
        this.http.post('https://api.vowfest.com/albums/list', { wedding_id: wedding_id1 }).subscribe(function (data) {
            console.log('@@@@@@@@@@@@@@@@@@@@@@', data);
            _this.image.push(data);
            _this.image.forEach(function (element) {
                console.log(element.albums_list);
                _this.album = element.albums_list;
            });
            console.log('####', _this.album);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    PhotoComponent.prototype.handleFileInput = function (file) {
        var _this = this;
        console.log("FILEEEEEEEEEEEEEEEEEEE:", (file.item(0)));
        this.fileToUpload = file.item(0);
        // Show image preview
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.imageUrl = event.target.result;
        };
        reader.readAsDataURL(this.fileToUpload);
    };
    PhotoComponent.prototype.onSubmit = function (f) {
        var _this = this;
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
        // f.value.item="--";
        f.value.cover_photo = this.imageUrl;
        console.log(f.value.album_name);
        console.log(f);
        console.log('@@@@@@@@@@@', this.imageUrl);
        var body = new FormData();
        body.append('album_name', f.value.album_name);
        body.append('cover_photo', this.fileToUpload);
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        this.http.post('https://api.vowfest.com/albums/create', body, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) { return response; }))
            .subscribe(function (result) {
            //this.id.weddingID=localStorage.getItem('wedding_id');
            _this.id.album_id = result.album_id;
            //  this.id.album_name=f.value.album_name;
            console.log("@@@@@@@@@@@@@@@@", result);
            console.log("@@@@@@@@@@@@@@@@", f.value);
            localStorage.setItem('albumid', _this.id.album_id);
            _this.router.navigate(['../images/', JSON.stringify(f.value)]);
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    PhotoComponent.prototype.rendertoImages = function (a) {
        console.log(a);
        this.router.navigate(['../images/', JSON.stringify(a)]);
    };
    PhotoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-photo',
            template: __webpack_require__(/*! ./photo.component.html */ "./src/app/photo/photo.component.html"),
            styles: [__webpack_require__(/*! ./photo.component.css */ "./src/app/photo/photo.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], PhotoComponent);
    return PhotoComponent;
}());



/***/ }),

/***/ "./src/app/profile.service.ts":
/*!************************************!*\
  !*** ./src/app/profile.service.ts ***!
  \************************************/
/*! exports provided: ProfileService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileService", function() { return ProfileService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileService = /** @class */ (function () {
    function ProfileService() {
        this.data = {};
    }
    ProfileService.prototype.setd = function (p) {
        console.log("PP", p);
        this.data = p;
    };
    ProfileService.prototype.getd = function () {
        console.log("PP", this.data);
        return this.data;
    };
    ProfileService.prototype.setData = function (x) {
        console.log("FILE", x);
        this.fileToupload = x;
    };
    ProfileService.prototype.getData = function () {
        console.log("FILE!!!", this.fileToupload);
        return this.fileToupload;
    };
    ProfileService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], ProfileService);
    return ProfileService;
}());



/***/ }),

/***/ "./src/app/profile/profile.component.css":
/*!***********************************************!*\
  !*** ./src/app/profile/profile.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\nh1{\n    margin-top: 30px;\n    text-align: center;\n}\n\nh6,form{\n    margin-top: 30px;\n    text-align: center;\n}\n\nform{\n    height: 400px;\n}\n\nh5{\n    font-size: 20px;\n    letter-spacing: 2px;\n}\n\ntextarea{\n    width: 80%;\n    border: none;\n    overflow: auto;\n    outline: none;\n    border-color: transparent;\n}\n\ninput:focus{\n    outline: none;\n    border-bottom: 1px solid #ff1556;\n}\n\ninput[type=text]{\n    width: 80%;\n    border-bottom: 1px solid #ff1556;\n    /* margin-left: 30px; */\n    overflow: auto;\n    outline: none;\n    border-top:none;\n    border-left:none;\n    border-right:none;\n}\n\n.img span{\n    float: left;\n    margin-left: 30px;\n    margin-bottom: 2%;\n}\n\nbutton{\n    width: 90%;\n    margin-top: 60px;\n    background-color: #ff1556;\n    color: white;\n}\n\ntextarea{\n    border:solid 1px grey;\n    position: relative;\n}\n\n.img{\n    margin-top: 20px;\n    color: #ff1556;\n    font-weight: bold;\n    margin-right: 105px;\n}\n\n.arrow{\nfloat: left;\nfont-size: 30px;\nline-height: 80px;\n}\n\n.fa-arrow-left,.fa-arrow-left:active{\ncolor: orange;\ntext-decoration: none;\n}\n\n#done{\n    background-color: #ff1556;\n    color: white;\n    border: #ff1556;\n    width: 100%;\n    height: 50px;\n    font-size: 25px;\n    font-weight: bold;  \n    position: absolute;\n    vertical-align: middle;\n    left: 0px;\n    position: fixed;\n    bottom: 0px;\n    text-decoration: none;\n}\n\nlabel{\n    padding: 50px;\n    background: white; \n    display: table;\n    color: #ff1556;\n     }\n\ninput[type=\"file\"] {\n    display: none;\n}"

/***/ }),

/***/ "./src/app/profile/profile.component.html":
/*!************************************************!*\
  !*** ./src/app/profile/profile.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-3 arrow\">\r\n            <a routerLink=\"/login\" class=\"fa fa-arrow-left\"></a>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h1>Welcome {{name}} !</h1>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h6>Let’s get you started!</h6>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <form  #imageForm=ngForm (ngSubmit)=\"OnSubmit(imageForm)\">\r\n                <input type=\"text\" name=\"name\" value={{name}} [(ngModel)]=\"name\" placeholder=\"User Name \" >\r\n                <h6></h6>\r\n                <input type=\"text\"  name=\"better_half\" placeholder=\"Name of your better half\" [(ngModel)]=\"pro.better_half\" >\r\n                <h6></h6>\r\n                <input type = \"text\" placeholder=\"About you both\" [(ngModel)]=\"pro.about\"  name=\"about\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-12\">\r\n                        <div class=\"img\">\r\n                            <label> \r\n                                <i class=\"fa fa-camera\"></i>   \r\n                                ADD A PHOTO\r\n                                <input type=\"file\"    \r\n                                #Image accept=\"image/*\" \r\n                                id = \"file\"\r\n                                (change)=\"handleFileInput($event.target.files)\">\r\n                            </label> \r\n                        </div>\r\n                        <!-- <div class=\"col-6\">\r\n                            <img  src=\"{{imageUrl}}\" alt=\"your image\" />\r\n                        </div> -->\r\n                    </div>\r\n                </div>    \r\n                <button id=\"done\" type=\"submit\">OK, PROCEED</button>\r\n            </form>   \r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n \r\n"

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../profile.service */ "./src/app/profile.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(http, router, activatedroute, profile) {
        this.http = http;
        this.router = router;
        this.activatedroute = activatedroute;
        this.profile = profile;
        this.Gurl = 'https://api.vowfest.com/accounts/login/'; // GOOGLE BUTTON
        this.pro = { better_half: '', about: '', name: '' };
        this.prof = { name: '', file: {} };
        this.data = [];
        this.fileToUpload = null;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.activatedroute.snapshot.params['data']) {
            this.data.push(JSON.parse(this.activatedroute.snapshot.params['data']));
            this.data.forEach(function (element) {
                console.log('DATA:', element.name);
                _this.name = element.name;
            });
        }
        else {
            console.log(JSON.parse(localStorage.getItem('userinfo')).name);
            this.name = JSON.parse(localStorage.getItem('userinfo')).name;
        }
    };
    ProfileComponent.prototype.handleFileInput = function (file) {
        var _this = this;
        this.fileToUpload = file.item(0);
        console.log('File to upload: ', this.fileToUpload);
        this.prof.file = this.fileToUpload;
        this.profile.setData(this.fileToUpload);
        // Show image preview
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.imageUrl = event.target.result;
            console.log(_this.imageUrl);
        };
        reader.readAsDataURL(this.fileToUpload);
    };
    ProfileComponent.prototype.OnSubmit = function (Image) {
        console.log(Image);
        this.userdata = {
            name: Image.value.name,
            better_half: Image.value.better_half,
            profile_photo: this.imageUrl
        };
        localStorage.setItem('userinfo', JSON.stringify(this.userdata));
        this.router.navigate(['/phonenumber', JSON.stringify(Image.value)]);
    };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/register/register.component.css":
/*!*************************************************!*\
  !*** ./src/app/register/register.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {font-family: Arial, Helvetica, sans-serif;}\r\n* {box-sizing: border-box}\r\n/* Full-width input fields */\r\ninput[type=text], input[type=password] {\r\n  width: 100%;\r\n  padding: 15px;\r\n  margin: 5px 0 22px 0;\r\n  display: inline-block;\r\n  border: none;\r\n  background: #f1f1f1;\r\n}\r\ninput[type=text]:focus, input[type=password]:focus {\r\n  background-color: #ddd;\r\n  outline: none;\r\n}\r\nhr {\r\n  border: 1px solid #ff1556;\r\n  margin-bottom: 25px;\r\n}\r\n/* Set a style for all buttons */\r\nbutton {\r\n  background-color: #ff1556;\r\n  color: white;\r\n  padding: 14px 20px;\r\n  margin: 8px 0;\r\n  border: none;\r\n  cursor: pointer;\r\n  width: 100%;\r\n  opacity: 0.9;\r\n}\r\nbutton:hover {\r\n  opacity:1;\r\n}\r\n/* Extra styles for the cancel button */\r\n.cancelbtn {\r\n  padding: 14px 20px;\r\n  background-color: #ff1556;\r\n}\r\n/* Float cancel and signup buttons and add an equal width */\r\n.cancelbtn, .signupbtn {\r\n  float: left;\r\n  width: 50%;\r\n}\r\n/* Add padding to container elements */\r\n.container {\r\n  padding: 16px;\r\n}\r\n/* Clear floats */\r\n.clearfix::after {\r\n  content: \"\";\r\n  clear: both;\r\n  display: table;\r\n}\r\n/* Change styles for cancel button and signup button on extra small screens */\r\n@media screen and (max-width: 300px) {\r\n  .cancelbtn, .signupbtn {\r\n     width: 100%;\r\n  }\r\n}"

/***/ }),

/***/ "./src/app/register/register.component.html":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html>\n<html>\n<body >\n  <!-- <a routerLink=\"../login\">Back</a> -->\n<form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" style=\"border:1px solid #ccc\">\n  <div class=\"container\">\n    <h1 class=\"text-center\">Sign Up</h1>\n    <p class=\"text-center\">Please fill in this form to create an account.</p>\n    <hr>\n    <label for=\"name\"><b>Name</b></label>\n    <input type=\"text\" placeholder=\"Enter Name\" name=\"name\" [(ngModel)]= \"name\" required>\n    <label for=\"email\"><b>Email</b></label>\n    <input type=\"text\" placeholder=\"Enter Email\" name=\"email\" [(ngModel)]= \"email\" required>\n    <label for=\"password\"><b>Password</b></label>\n    <input type=\"password\" placeholder=\"Enter Password\" name=\"password\" [(ngModel)]= \"password\" required>\n    <div class=\"clearfix\" >\n      <button type=\"button\" class=\"cancelbtn\"><a routerLink=\"/login\">Cancel</a></button>\n      <button type=\"submit\" class=\"signupbtn\">Sign Up</button>\n    </div>\n  </div>\n</form>\n\n</body>\n</html>\n"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(http, router, activatedroute) {
        this.http = http;
        this.router = router;
        this.activatedroute = activatedroute;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        console.log(JSON.stringify(this.activatedroute.snapshot.params['data']));
    };
    RegisterComponent.prototype.onSubmit = function (f) {
        var _this = this;
        if (this.activatedroute.snapshot.params['data'] == undefined) {
            localStorage.setItem('guest_invites', 'false');
            var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
            console.log(JSON.stringify(f.value));
            this.http.post('https://api.vowfest.com/createAccount', JSON.stringify(f.value), { headers: header }).subscribe(function (data) {
                console.log('response: ', data);
                _this.a = data;
                localStorage.setItem('userToken', _this.a.token);
                localStorage.setItem('weddingID', _this.a.wedding_id);
                _this.router.navigate(['../profile/', JSON.stringify(f.value)]);
                // alert('You are registered now');
            }, function (error) {
                _this.email_error = error.error.email;
                _this.username_error = error.error.username;
                _this.password1_error = error.error.password1;
                console.log(error);
                // this.router.navigate(['../error', JSON.stringify(error)]);
            });
        }
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/send-invitation/send-invitation.component.css":
/*!***************************************************************!*\
  !*** ./src/app/send-invitation/send-invitation.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.jumbotron{\r\n    background-color: transparent;\r\n    padding-bottom: 0%;\r\n}\r\nhr{\r\n    width:10%;\r\n    background-color: brown;\r\n}\r\n.jumbo{\r\n    margin-top: 25%;\r\n}\r\n.fa-plus-circle{\r\n    color: orange;\r\n    font-size: 20px;\r\n}\r\n.topnav {\r\noverflow: hidden;\r\nmargin-left: 5%;\r\ntext-align: center;\r\n}\r\n.topnav a {\r\nfloat: left;\r\ndisplay: block;\r\ncolor: black;\r\ntext-align: center;\r\npadding: 14px 16px;\r\ntext-decoration: none;\r\nfont-size: 17px;\r\nborder-bottom: 3px solid transparent;\r\n}\r\n.topnav a:hover {\r\nborder-bottom: 3px solid red;\r\n}\r\n.topnav a.active {\r\nborder-bottom: 3px solid red;\r\n}\r\n.send_text{\r\n    color: orange!important;\r\n}\r\n.image_grid{\r\n    width: 100%;\r\n    padding: 10%;\r\n\r\n}"

/***/ }),

/***/ "./src/app/send-invitation/send-invitation.component.html":
/*!****************************************************************!*\
  !*** ./src/app/send-invitation/send-invitation.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n    <div class=\"row\">\n    <div class=\"col-12\">\n      <app-navbar></app-navbar>\n    </div>\n    </div>\n    <div class=\"row\">\n    <div class=\"topnav\">\n      \n      <!-- <a routerLink=\"/invitation\"  [ngClass]=\"{ 'active': shouldShow, 'inactive': !shouldShow  }\">Invitation</a>\n      <a routerLink=\"/guest-list\"  [ngClass]=\"{  'inactive': !shouldShow  }\">Guest list</a>\n      <a routerLink=\"/account\"  [ngClass]=\"{  'inactive': !shouldShow  }\">Account</a>\n    -->\n    </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <a  class=\"send_text\"> Select all </a> \n        <a style=\"margin-left: 77%;\"><img src=\"../../assets/select.png\"></a> \n      </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-12\">\n            <div class=\"col-4\">  \n              <img class= \"image_grid\" src=\"https://cutt.ly/S1RKBX\">\n            </div>\n            <div class=\"col-4\">  \n              <img class= \"image_grid\" src=\"https://cutt.ly/S1RKBX\">\n            </div>\n            <div class=\"col-4\">\n              <img class= \"image_grid\" src=\"https://cutt.ly/S1RKBX\">\n            </div>\n        </div>\n    </div>\n    <div class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"jumbotron text-center jumbo\">\n           \n            <i class=\"fas fa-plus-circle\" type=\"button\" (click)=addpic()>ADD A PHOTO</i>\n        </div>\n    </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/send-invitation/send-invitation.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/send-invitation/send-invitation.component.ts ***!
  \**************************************************************/
/*! exports provided: SendInvitationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendInvitationComponent", function() { return SendInvitationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SendInvitationComponent = /** @class */ (function () {
    function SendInvitationComponent() {
    }
    SendInvitationComponent.prototype.ngOnInit = function () {
    };
    SendInvitationComponent.prototype.addpic = function () { };
    SendInvitationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-send-invitation',
            template: __webpack_require__(/*! ./send-invitation.component.html */ "./src/app/send-invitation/send-invitation.component.html"),
            styles: [__webpack_require__(/*! ./send-invitation.component.css */ "./src/app/send-invitation/send-invitation.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SendInvitationComponent);
    return SendInvitationComponent;
}());



/***/ }),

/***/ "./src/app/snapshot/snapshot.component.css":
/*!*************************************************!*\
  !*** ./src/app/snapshot/snapshot.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "img{\n    -o-object-fit: fill;\n       object-fit: fill;\n    zoom: 0%;\n    z-index: -1;\n    position: relative;\n}\ni{\n    font-size: 50px;\n    z-index: 1;\n    position: absolute;\n    bottom: 0;\n}"

/***/ }),

/***/ "./src/app/snapshot/snapshot.component.html":
/*!**************************************************!*\
  !*** ./src/app/snapshot/snapshot.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<img src={{cam.base64}} width=\"100%\" height=\"800\" />\n<div class=\"text-center\">\n    <i class=\"fas fa-check-square\" type=\"button\" (click)=img()></i>\n</div>"

/***/ }),

/***/ "./src/app/snapshot/snapshot.component.ts":
/*!************************************************!*\
  !*** ./src/app/snapshot/snapshot.component.ts ***!
  \************************************************/
/*! exports provided: SnapshotComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SnapshotComponent", function() { return SnapshotComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _camera_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../camera.service */ "./src/app/camera.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SnapshotComponent = /** @class */ (function () {
    function SnapshotComponent(route, http, activatedroute, camera) {
        this.route = route;
        this.http = http;
        this.activatedroute = activatedroute;
        this.camera = camera;
        this.data = [];
        this.cam = { base64: '' };
        this.fileToUpload = null;
        this.image = { image: '' };
    }
    SnapshotComponent.prototype.ngOnInit = function () {
        console.log(this.camera.image);
        this.activatedroute.snapshot.data;
        console.log(this.activatedroute.snapshot.params['image_data']);
        this.cam = this.camera.image;
        console.log(this.cam);
    };
    SnapshotComponent.prototype.img = function () {
        this.route.navigate(['../add-stories']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("canvas"),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], SnapshotComponent.prototype, "canvas", void 0);
    SnapshotComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-snapshot',
            template: __webpack_require__(/*! ./snapshot.component.html */ "./src/app/snapshot/snapshot.component.html"),
            styles: [__webpack_require__(/*! ./snapshot.component.css */ "./src/app/snapshot/snapshot.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _camera_service__WEBPACK_IMPORTED_MODULE_3__["CameraService"]])
    ], SnapshotComponent);
    return SnapshotComponent;
}());



/***/ }),

/***/ "./src/app/stories/stories.component.css":
/*!***********************************************!*\
  !*** ./src/app/stories/stories.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "*{\r\n    margin: 0%;\r\n    padding: 0%;\r\n}\r\n.clearfix {\r\n    display: block;\r\n    content: \"\";\r\n    clear: both;\r\n}\r\n.signoutbutton{\r\n    margin-left: 10%;\r\n          background-color: #ef0873;\r\n}\r\nul {\r\n    list-style-type: none;\r\n    margin-left: 15px;\r\n    padding-top: 20px;\r\n  }\r\nul li{\r\n    margin: 10px 0px;\r\n}\r\n.viweallbutton{\r\n    position: fixed;\r\n    bottom: 9%;\r\n    left: 0;\r\n    right: 0;\r\n    margin: 0 auto;\r\n    width: inherit;\r\n    padding: 5px 0;\r\n    margin-bottom: -1%;\r\n}\r\n.story{\r\n    margin-top: 20px;\r\n}\r\n.padd_15{\r\n    padding: 0 15px !important;\r\n}\r\n.row{\r\n    padding: 0 15px !important;\r\n}\r\n.padd_zero{\r\n    padding: 0px !important;\r\n}\r\n.col-8{\r\n    display: inline;\r\n    padding-top: 3%;\r\n}\r\n#ring{\r\n    float: right;\r\n    height: 30px;\r\n    width: 30px;\r\n\r\n    position: absolute;\r\n    right: 15%;\r\n    top: 15%;\r\n}\r\n.content{\r\n    height: -webkit-fit-content;\r\n    height: -moz-fit-content;\r\n    height: fit-content;\r\n    text-align: center;\r\n    margin: 30px;\r\n}\r\n#pro{\r\n    height:50px !important;\r\n    margin:5%;\r\n    border-radius: 50%;\r\n    width: 50px;\r\n    margin-top: 20%;\r\n}\r\np{\r\n    display: inline;\r\n}\r\n.updimg{\r\n    margin-top: 50px;\r\n    text-align: center;\r\n}\r\n.icon{\r\n    float: right;\r\n    padding-top: 10px;\r\n    /* margin-left: -15px; */\r\n    text-align: center;\r\n}\r\n/* .content{\r\n    margin-bottom: 20px;\r\n    margin-top: 10px;\r\n} */\r\n.para{\r\n    /* word-wrap: break-word; */\r\n    padding-top: 18px\r\n}\r\n.share{\r\n    float: left;\r\n    /* margin-left: -10%; */\r\n}\r\n.select{\r\n    margin-right: -20%;\r\n}\r\n.dropdown {\r\n    position: absolute;\r\n    display: inline-block;\r\n    /* right: 0.4em; */\r\n}\r\n.gap i{\r\n    font-size: 15px;\r\n   \r\n}\r\n.sp i{\r\n    font-size: 20px;\r\n    margin-left:10px;\r\n    margin-right: 10px;\r\n    margin-top:10px;\r\n}\r\n.dropdown-content {\r\n    display: none;\r\n    /* position: relative; */\r\n    margin-top: 50px;\r\n    background-color: wheat;\r\n    min-width: 160px;\r\n    overflow: auto;\r\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\r\n    z-index: 1;\r\n}\r\n.dropdown-content a {\r\n    color: black;\r\n    padding: 12px 16px;\r\n    text-decoration: none;\r\n    display: block;\r\n}\r\n.dropdown a:hover {background-color: black}\r\n.show {display:block;\r\nwidth: 30%;\r\nfloat: right;}\r\n.strip{\r\nheight: 90px;\r\n\r\n}\r\n* {\r\n    box-sizing: border-box;\r\n  }\r\n/* .column {\r\n    float: left;\r\n    width: 33.33%;\r\n    padding: 5px;\r\n  } */\r\n.gap{\r\n      padding-left: 2%;\r\n      padding-right: 2%;\r\n  \r\n      padding-top: 26px;\r\n      }\r\n/* Clearfix (clear floats) */\r\n.row::after {\r\n    content: \"\";\r\n    clear: both;\r\n    display: table;\r\n  }\r\nh6{\r\n    margin-top:4%;\r\n    margin-bottom: 4%;\r\n}\r\nh1 {\r\n                text-align: center;\r\n                text-transform: uppercase;\r\n                color: #4CAF50;\r\n              }\r\np {\r\n                /* text-indent: 50px; */\r\n                /* text-align: justify; */\r\n                letter-spacing: 0px;\r\n              }\r\na {\r\n                text-decoration: none;\r\n                color: #008CBA;\r\n              }\r\n/* .container {\r\n    background-color: rgb(184, 185, 185);\r\n    } */\r\n.event1{\r\n        color: white;\r\n        font-size: 15px;\r\n        text-align: center;\r\n        position: absolute;\r\n        bottom: 0;\r\n        left: 0;\r\n    }\r\n.event2{\r\n        color: white;\r\n        font-size: 15px;\r\n        text-align: center;\r\n        position: absolute;\r\n        bottom: 0;\r\n        right: 0;\r\n    }\r\n.mainevent{\r\n        color: white;\r\n        font-size: 20px;\r\n        text-align: center;\r\n        position: absolute;\r\n        bottom: 40%;\r\n        right: 35%;\r\n    }\r\nlabel{color: #ffd6a5}\r\n.displaycards{\r\n    background-color: rgba(128, 128, 128, 0.14);\r\n    height: 100%;\r\n    padding: 0px 0 15px !important;\r\n}\r\n.lates_update{\r\n    margin-top: 30px;\r\n}\r\n.propic{\r\n    margin-bottom: 20px;\r\n    padding: 0px 15px !important;\r\n}\r\n.w3-dropdown-content{\r\n        right: 0;\r\n        top: 30px;\r\n    }\r\n/* .w3-bar-block{\r\n        width: 10px !important;\r\n    } */\r\n.top{\r\n        margin-top: 18px;\r\n        margin-bottom: 0px;\r\n    }\r\n.text{\r\n    word-break: break-all;\r\n    max-width: 100%;\r\n  }\r\n.far{\r\n      font-size: 20px;\r\n      margin-top: 10px;\r\n  }\r\n.eventheading{\r\n    text-align: center;\r\n    color: white;\r\n    margin-left: 13%;\r\n   \r\n  }\r\n/* new css */\r\n.top_story{\r\n      height: 10px;\r\n  }\r\n.color{\r\n    background: linear-gradient(to right,#ff8d00,#ff1556);\r\n    height: 100px;\r\n    position: relative;\r\n    margin-top: 45%;\r\n}\r\n.msgicon_top_story{\r\n    padding-left: 28%;\r\n}\r\n.top_story img{\r\n    border-radius:5%;\r\n    max-height: 140px;\r\n}\r\n.date_color{\r\n    color: #ffd6a5 !important;\r\n}\r\n.comment_button{\r\n    text-align: right\r\n}\r\n.comment_button button{\r\n    margin-top: 13px;\r\n    padding: 4px 20px;\r\n}\r\n.comment_input{\r\n    text-indent: 10px;\r\n}\r\n.comment_P{\r\n    color:gray;\r\n    font-size: 16px;\r\n}\r\nhr{\r\n    color: black;\r\n}\r\nh3{\r\n    margin-left: 6%;\r\n}\r\n.nostory{\r\n    margin-top: 19%;\r\n    margin-bottom: 16%;\r\n    color: black;\r\n    text-align: center;\r\n}\r\n.no_event{\r\n    margin-left: 118%;\r\n    margin-bottom: 29%\r\n}\r\n/* //spinner */\r\n#spinner {\r\n\t-webkit-animation: frames 1s infinite linear;\r\n\tanimation: frames 1s infinite linear;\r\n\tbackground: transparent;\r\n\tborder: 1.75vw solid #FFF;\r\n\tborder-radius: 100%;\r\n\tborder-top-color: #DF691A;\r\n\twidth: 20vw;\r\n\theight: 20vw;\r\n\topacity: .6;\r\n\tpadding: 0;\r\n\tposition: absolute;\r\n\tz-index: 999;\r\n}\r\n@-webkit-keyframes frames {\r\n  0% {\r\n\t-webkit-transform: rotate(0deg);\r\n\ttransform: rotate(0deg);\r\n  }\r\n  100% {\r\n\t-webkit-transform: rotate(359deg);\r\n\ttransform: rotate(359deg);\r\n  }\r\n}\r\n@keyframes frames {\r\n  0% {\r\n\t-webkit-transform: rotate(0deg);\r\n\ttransform: rotate(0deg);\r\n  }\r\n  100% {\r\n\t-webkit-transform: rotate(359deg);\r\n\ttransform: rotate(359deg);\r\n  }\r\n}\r\n#pause {\r\n\tdisplay: block;\r\n\tbackground:\r\n\t\trgba(0, 0, 0, 0.66)\r\n\t\tno-repeat\r\n\t\t0 0;\r\n\twidth: 100%;\r\n\theight: 100%;\r\n\tposition: fixed;\r\n\tbottom: 0;\r\n\tleft: 0;\r\n\tz-index: 1000;\r\n}\r\n"

/***/ }),

/***/ "./src/app/stories/stories.component.html":
/*!************************************************!*\
  !*** ./src/app/stories/stories.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"col-12 \">\r\n\r\n    <!--Start bar line  -->\r\n        <div class=\"row col-12 \">\r\n            <div class=\"col-2\" >\r\n                <a routerLink=\"/dashboard/{{image_data}}\" >\r\n                    <img src=\"{{propic}}\" alt=\"\"  id=\"pro\" style=\"max-width:100%;height:auto;\">\r\n                </a>\r\n            </div>\r\n            <div class=\"col-8\">\r\n                <h6>{{name}} and {{better_half}}'s vow's</h6>        \r\n            </div>\r\n\r\n            <!-- <share-buttons [url]=\"'https://twitter.com'\" [showCount]=\"true\"></share-buttons> -->\r\n            <div class=\"col-2\">\r\n                <!-- <a   routerLink=\"/engagement\"> -->\r\n                <a   routerLink=\"/invitation\">\r\n                    <img src=\"../../assets/rings.png\" alt=\"\" id=\"ring\">\r\n                </a>\r\n            </div>\r\n        </div>\r\n    <!--End bar line  -->\r\n\r\n    <!-- Start Top story  -->\r\n        <div class=\"clearfix\"></div>\r\n        <div class=\" row col-12 \">\r\n            <p style=\"color:#ff1556; font-size: 15px; margin-top: 30px\">Top photos </p><br>\r\n        </div>\r\n        <div class=\" row col-12\">\r\n            <div class=\"col-6 top_story gap\" *ngFor=\"let i of image_data2  | slice:0:2;let i=index\">\r\n                \r\n                <div  class=\"row\"  style=\"border-radius: 15px;max-width:100%;\">\r\n                    <a  routerLink=\"/dashboard/{{image_data}}\">  \r\n                        <img src=\"{{i.photo_data}}\" style=\"max-width:100%;\"/> \r\n                    </a> \r\n                </div>\r\n               \r\n                <div class=\"row\" >\r\n                    <i class=\"far fa-heart col-6\" style=\"color:deeppink;\" (click)=\"setlike(i.photo_id)\"> <span>  {{i.num_likes}}</span></i>\r\n                    <i class=\"far fa-comment col-6 msgicon_top_story \" style=\"color:orange;\"> <span> {{i.num_comments}}</span></i>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- <div *ngIf = \"image_data.status == false\" class=\"nostory \">\r\n            <label>loading ......{{image_data.msg}}</label>\r\n            <img src=\"../../assets/lg.curve-bars-loading-indicator.gif\" style=\"max-width:20%;\"/> \r\n        </div>   -->\r\n    <!--End Top story   -->\r\n\r\n    <!-- Start Event going   -->\r\n        <div class=\"clearfix\"></div>\r\n        <div class=\"row col-12   color\">\r\n            <img src=\"../../assets/yellow_lace.png\" alt=\"Snow\" height=\"20px\" width=\"100%\">        \r\n            <div *ngIf=\"eventarray_length == 0\" class=\" row col-12 eventheading\" routerLink = \"../event-list\"> \r\n                <h3>{{eventdata.msg}}</h3> \r\n            </div> \r\n            <div class= \"row col-12 padd_zero\" *ngIf=\"eventarray_length > 0\">\r\n                <div class=\"col-4 text-center date_color\" *ngFor=\"let i of eventdata.event_list  | slice:0:1 ;let i=index\">  \r\n                    <label  class=\"small\" *ngIf = \"(i.date | date) === (now | date)\">Today</label>\r\n                    <label  class=\"small\" *ngIf = \"(i.date | date) > (now | date)\">Upcomming Event</label> \r\n                    <div>\r\n                        <div *ngIf = \"(i.date | date) === (now | date)\">{{i.date | date }}<br></div>\r\n                        <div *ngIf = \"(i.date | date) > (now | date)\">{{i.date | date }}<br></div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-4 text-center\">                                           \r\n                    <div *ngFor=\"let i of eventdata.event_list  | slice:0:1 ;let i=index\"  style=\"color : white\" >\r\n                        <div *ngIf = \"(i.date | date) === (now | date)\"> {{i.name}} <br></div>\r\n                        <div *ngIf = \"(i.date | date) > (now | date)\"> {{i.name}} <br></div>\r\n                    </div>                  \r\n                </div>\r\n                <div class=\"col-4 text-center date_color\" *ngFor=\"let i of eventdata.event_list  | slice:0:1 ;let i=index\" >\r\n                    <label class=\"small\" *ngIf = \"(i.date | date) === (now | date)\">Ongoing</label>\r\n                    <label class=\"small\" *ngIf = \"(i.date | date) > (now | date)\">Ongoing</label>\r\n  \r\n                        <div *ngIf = \"(i.date | date) === (now | date)\">\r\n                            <div *ngFor=\"let j of i.programe  | slice:0:1\">\r\n                                {{j.name}} <br>\r\n                            </div>\r\n                        </div>\r\n                        <div *ngIf = \"(i.date | date) > (now | date)\">\r\n                            <div *ngFor=\"let j of i.programe  | slice:0:1\">\r\n                                {{j.name}} <br>\r\n                            </div>\r\n                        </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div  *ngIf = \"No_event == true\" style=\"color : white\"  routerLink = \"../event-list\">\r\n                <div class=\"col-12 text-center no_event\">\r\n                    No Event Today \r\n                </div>\r\n            </div>\r\n        </div>\r\n    <!--End  Event going   -->\r\n\r\n    <!--Start story Time line -->\r\n        <div class=\"row lates_update\">\r\n            <div class=\"col-12 \">\r\n                <p style=\"color:#ff1556; font-size: 13px;\">Latest Update</p>\r\n            </div>\r\n        </div>\r\n        <!-- <div *ngIf = \"image_data.status == 'true'\"> -->\r\n            <div class=\"\" *ngFor=\"let i of image_data2 ;let i=index\">\r\n                    <div class=\"row col-12 propic\">\r\n                        <div class=\"col-2\">\r\n                          \r\n                            <img src= \"{{i.userphoto}}\" id=\"pro\" style=\"max-width:100%;height:auto;\" >\r\n                            \r\n                        </div>\r\n                        <div class=\"col-8 para\">\r\n                         <strong> {{i.username}}'s </strong> wedding festival has begun<br>\r\n                         <small class=\"small comment_P\"> {{i.time | date: 'medium'}} </small>\r\n                  \r\n                        </div>\r\n                      \r\n                        <div class=\"col-2 icon\">\r\n                            <div class=\"col-1 share\">\r\n                                <a class=\"\"><img src=\"../../assets/share.png\" alt=\"\"></a>\r\n                            </div>\r\n                            <div class=\"col-1 w3-dropdown-hover select\">\r\n                                <!-- <button class=\"w3-button\">&#8942;</button> -->\r\n                                <a><img src=\"../../assets/select.png\"></a>\r\n                                <div class=\"w3-dropdown-content w3-bar-block w3-border w3-card-4\">\r\n                                    <a class=\"w3-bar-item w3-button\">Edit</a>\r\n                                    <a  class=\"w3-bar-item w3-button\">Delete</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div  class=\" col-12\">\r\n                        <img class=\"col-12\" src=\"{{i.photo_data}}\" style=\"max-width:100%;height:auto;\"/>   \r\n                    </div>\r\n                    <div class=\"row col-12 displaycards\">\r\n                        <div class=\"row col-12 \">\r\n                            <div class=\"col-8\">\r\n                                <p  class=\"comment_P\"> {{i.story}}</p>\r\n                            </div>     \r\n                            <div class=\"col-4 text-right\">\r\n                                <i class=\"far fa-heart \" style=\"color:deeppink;\" (click)=\"setlike(i.photo_id)\"> \r\n                                    <span *ngIf = \"i.num_likes > 0\">  {{i.num_likes}}</span>\r\n                                </i>\r\n                                <i class=\"far fa-comment msgicon_top_story\" style=\"color:orange;\"  (click) = \"viewcomment(i.photo_id)\"  > \r\n                                    <span  *ngIf = \"i.num_likes > 0\"> {{i.num_comments}}</span>\r\n                                </i>                               \r\n                                    \r\n                            </div>     \r\n                        \r\n                        </div>\r\n                        \r\n                        <div class=\"col-12 comment_div\" *ngIf=\"comment_div == true && divid == i.photo_id\">\r\n                                <ul *ngFor=\"let comment of i.comments \" class=\"col-12\">\r\n                                    <li class=\"col-12\">  <strong> {{comment.username}}</strong> just commented  </li>\r\n                                    <li class=\"col-12\">  <p class=\"small\" style=\"margin-left:0%\">  {{comment.time | date}}</p>  </li>\r\n                                    <li class=\"col-12\">  <p style=\"padding-bottom: 1%;\"> {{comment.body}}</p></li>\r\n                                    <i class=\"fa fa-thumbs-up\" aria-hidden=\"true\" (click) = \" add_counts_of_like(comment.comment_id)\"></i>\r\n                                    {{comment.num_likes}}\r\n\r\n                                    <li class=\"col-12\">  <hr> </li> \r\n                                </ul>\r\n                                <br>\r\n                                <form class=\"row col-12\" (ngSubmit)=\"add_comment(com)\" #com=\"ngForm\">                        \r\n                                    <div class=\"col-8 form-group\">\r\n                                        <input type=\"hidden\"  name = \"photo_id\" id=\"photo_id\" [(ngModel)]=\"i.photo_id\" />\r\n                                        <input type=\"text\" class=\"form-control comment_input\" name = \"body\" id=\"Inputname\" [(ngModel)]=\"body\" />\r\n                                    </div>\r\n                                    <div class=\"col-4 form-group comment_button\">\r\n                                        \r\n                                        <button type=\"submit\" class=\"btn btn-primary small\">comment</button>\r\n                                    </div>    \r\n                                </form> \r\n                        </div>                     \r\n                    </div>\r\n            </div>\r\n        <!-- </div>     -->\r\n        <div class=\"clearfix\"></div>\r\n        <!-- <div *ngIf = \"image_data.status == false\" class=\"nostory\">\r\n            <label>loading ......{{image_data.msg}}</label>\r\n            <img src=\"../../assets/lg.curve-bars-loading-indicator.gif\" style=\"max-width:23%;\"/> \r\n        </div> -->\r\n     <!--End story Time line -->\r\n\r\n    <!-- <div class =\"row col-12 \">\r\n            <button routerLink = \"/allpost\" class=\"btn btn-secondary viweallbutton\"> View All</button>\r\n    </div> -->\r\n    <br><br><br><br><br><br>\r\n    <div class=\"clearfix\"></div>\r\n    <div id=\"pause\" class=\"d-flex align-items-center justify-content-center\" *ngIf = \"spinner == true\">\r\n        <div id=\"spinner\"></div>\r\n    </div>\r\n    <div class=\" row col-12\">\r\n            <app-navbar></app-navbar>\r\n        </div>\r\n</div>"

/***/ }),

/***/ "./src/app/stories/stories.component.ts":
/*!**********************************************!*\
  !*** ./src/app/stories/stories.component.ts ***!
  \**********************************************/
/*! exports provided: StoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoriesComponent", function() { return StoriesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _camera_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../camera.service */ "./src/app/camera.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var StoriesComponent = /** @class */ (function () {
    function StoriesComponent(datePipe, _sanitizer, http, router, activatedroute, camera) {
        this.datePipe = datePipe;
        this._sanitizer = _sanitizer;
        this.http = http;
        this.router = router;
        this.activatedroute = activatedroute;
        this.camera = camera;
        this.img = {};
        this.data = [];
        this.spinner = true;
        this.count = 0;
        this.edata = {};
        this.imagePath = [];
        this.id = { photo_id: '' };
        this.cam = {};
        this.eventdata = { event_list: [] };
        this.data1 = {};
        this.data2 = [];
        this.wedding_id = { wedding_id: '' };
        this.event_list = {};
        this.image_data = { status: '' };
        this.comment = { num_likes: '' };
        this.No_event = false;
        this.now = new Date();
        this.slides = [[]];
        this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
        //console.log(authToken)
        if (authToken == null) {
            this.router.navigate(['../register']);
        }
        this.name = localStorage.getItem('userinfo') ? JSON.parse(localStorage.getItem('userinfo')).name : localStorage.getItem('guest_name');
        this.better_half = localStorage.getItem('userinfo') ? JSON.parse(localStorage.getItem('userinfo')).better_half : "no Betterhalf yet";
        this.propic = JSON.parse(localStorage.getItem('userinfo')).profile_photo;
        this.wedding_id.wedding_id = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        this.geteventlist();
        this.getstory_list();
    }
    StoriesComponent.prototype.ngOnInit = function () {
        this.comment_div = false;
        this.now = new Date();
        console.log(this.now);
    };
    StoriesComponent.prototype.chunk = function (arr, chunkSize) {
        var R = [];
        for (var i = 0, len = arr.length; i < len; i += chunkSize) {
            R.push(arr.slice(i, i + chunkSize));
        }
        return R;
    };
    StoriesComponent.prototype.setlike = function (x) {
        var _this = this;
        this.id.photo_id = x;
        this.http.post("https://api.vowfest.com/photo/like/add", this.id, this.options).subscribe(function (data) {
            _this.getstory_list();
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    StoriesComponent.prototype.add_comment = function (c) {
        var _this = this;
        console.log(c.value);
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/add", c.value, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.getstory_list();
        }, function (error) {
            console.log(error);
            //  this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    StoriesComponent.prototype.viewcomment = function (x) {
        var _this = this;
        if (this.comment_div == false) {
            this.comment_div = true;
        }
        else {
            this.comment_div = false;
        }
        this.divid = x;
        this.http.post('https://api.vowfest.com/photo/comment/list', { photo_id: x }).subscribe(function (data) {
            _this.viewwcomments = data;
            console.log(_this.viewwcomments);
            _this.com = _this.viewwcomments.comments;
        }, function (error) {
            console.log(error);
            // this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    StoriesComponent.prototype.geteventlist = function () {
        var _this = this;
        this.wedding_id.wedding_id = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        ;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        this.http.post('https://api.vowfest.com/event/list', this.wedding_id, { headers: header }).subscribe(function (data) {
            _this.eventdata = data;
            console.log(_this.eventdata);
            if (_this.eventdata.status == false) {
                _this.eventarray_length = 0;
            }
            else {
                _this.eventarray_length = _this.eventdata.event_list.length;
                for (var _i = 0, _a = _this.eventdata.event_list; _i < _a.length; _i++) {
                    var i = _a[_i];
                    if (i.date === _this.datePipe.transform(_this.now, 'yyyy-MM-dd')) {
                        _this.No_event = false;
                        console.log("false");
                    }
                    // else{ 
                    //       this.No_event = true ;
                    //       console.log("true")
                    //       }
                }
            }
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    StoriesComponent.prototype.getstory_list = function () {
        var _this = this;
        this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        header.append('Access-Control-Allow-Origin', '*');
        this.http.post('https://api.vowfest.com/story/list', { 'wedding_id': this.wid }, { headers: header }).subscribe(function (data) {
            console.log('DATA RECIEVED: ', data);
            _this.image_data = data;
            _this.spinner = false;
            if (_this.image_data.status == true) {
                _this.image_data2 = _this.image_data.stories_list;
                console.log(_this.image_data2);
                if (_this.image_data2) {
                    for (var _i = 0, _a = _this.image_data2; _i < _a.length; _i++) {
                        var comment_array = _a[_i];
                        console.log(comment_array.comments.length);
                        if (comment_array.comments.length == 0) {
                            _this.comment_div = false;
                        }
                        else {
                            _this.comment_div = true;
                        }
                    }
                }
            }
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    StoriesComponent.prototype.add_counts_of_like = function (x) {
        var _this = this;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/like/add", { comment_id: x }, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.getstory_list();
            //this.view_counts_of_like(x);
        }, function (error) {
            console.log(error);
            //  this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    StoriesComponent.prototype.view_counts_of_like = function (x) {
        var _this = this;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
        ;
        header.append('Access-Control-Allow-Origin', '*');
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT' });
        this.http.post("https://api.vowfest.com/photo/comment/like/list", { comment_id: x }, { headers: this.headers }).subscribe(function (data) {
            console.log(data);
            _this.comment_like = data;
            _this.comment.num_likes = _this.comment_like.num_likes;
            console.log(_this.comment.num_likes);
        }, function (error) {
            console.log(error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
        });
    };
    StoriesComponent.prototype.signout = function () {
        localStorage.clear();
        this.router.navigate(['../login']);
    };
    StoriesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stories',
            template: __webpack_require__(/*! ./stories.component.html */ "./src/app/stories/stories.component.html"),
            styles: [__webpack_require__(/*! ./stories.component.css */ "./src/app/stories/stories.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _camera_service__WEBPACK_IMPORTED_MODULE_4__["CameraService"]])
    ], StoriesComponent);
    return StoriesComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\LENOVO\Desktop\vowfests\task\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map