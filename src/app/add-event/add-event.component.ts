import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { catchError, map, tap } from 'rxjs/operators';
@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {
  EventArray = [];
  eve={programe:[{name:'',time:''}]};
  data1=[];
  isCollapsed;
  add=[];
  headers;
  options;
  count=1;
  add_programe = {name:'', time:''};
  programe_array=[];
  constructor(private http: HttpClient,private router: Router) { }
  ngOnInit() {   
    var authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
    if(!authToken) 
    {  this.router.navigate(['../login']);
     }        
  }
  addevent(){

                const authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_wedding_id');
                const headers   = new Headers();
                this.headers    = new HttpHeaders({'Authorization': authToken});
                this.options    = new RequestOptions({headers: this.headers});

                headers.append('Accept', 'application/json');
                headers.append('Content-Type', 'application/json');
                headers.append('Authorization', authToken);
             
                
                console.log(this.eve)
                this.http.post('https://api.vowfest.com/event/add', this.eve, {headers: this.headers}).pipe(map(response => response
                )).subscribe((data)=>{
                                        console.log("DATA: ",data);
                                        this.data1.push(data);
                                        this.router.navigate(['../event-list']);
                },(error) =>{  
                                console.log(error)
                               // this.router.navigate(['../error', JSON.stringify(error)]);
                            } );
  }
  addprogram=()=>{
    console.log(this.count)
    for (var _i = 0; _i < this.count; _i++) {
      this.programe_array.push(this.add_programe)
      console.log( this.programe_array);
      this.eve.programe.push(this.add_programe);
      console.log( this.eve);
  }
  
   
   
  }

}