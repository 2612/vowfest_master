import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { Router,ActivatedRoute } from '@angular/router';
import { CameraService } from '../camera.service';

@Component({
  selector: 'app-add-stories',
  templateUrl: './add-stories.component.html',
  styleUrls: ['./add-stories.component.css'],
  
})
export class AddStoriesComponent implements OnInit {
spinner = false;
image={file:{},base64:''};
wid;
story_data:any={};
headers;
options;
story={photo_data:'',wedding_id:'',story:''}
formData: any = new FormData();
constructor(private http: HttpClient,private router: Router,private activatedroute: ActivatedRoute,private camera:CameraService) { }

  ngOnInit() {
    const authToken = localStorage.getItem('userToken') ||  localStorage.getItem('guest_invite_authtoken');
    this.headers    = new HttpHeaders({
                                         'Authorization': authToken,
                                         'Access-Control-Allow-Origin': '*'
                                        });
    this.options    = new RequestOptions({headers: this.headers});
    this.wid        = localStorage.getItem("weddingID") || localStorage.getItem("guest_wedding_id");
    this.story.wedding_id = this.wid;
    this.story_data       = this.camera.getimage();
    console.log("FILE: ",this.story_data);
  }
  addstory(){
          
           
            this.formData.append("photo_data",this.story_data.file);
            this.formData.append("wedding_id",this.wid);
            this.formData.append("story",this.story.story);
            const authToken = localStorage.getItem('userToken') ||  localStorage.getItem('guest_invite_authtoken');
            this.api();
            this.spinner = true;
  }
  api()
  {
          console.log(this.formData)
          this.http.post('https://api.vowfest.com/story/create',
          this.formData, this.options ).subscribe((res) => {
                    console.log(res);
                    if( localStorage.getItem('guest_invites') == 'true')
                      { this.router.navigate(['../stories']);
                        this.spinner = false;
                      }
                      else
                      { 
                              if(localStorage.getItem('login_type') == 'guest' ){
                                this.router.navigate(['../Guest_Dashboard']);
                              }else{
                                this.router.navigate(['../dashboard']);
                              }           
                      }
          },error=> {
                          console.log(error);  
                    });
  }
}