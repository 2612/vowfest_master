import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router} from '@angular/router';
import { RequestOptions } from '@angular/http';
@Component({
  selector: 'app-allpost',
  templateUrl: './allpost.component.html',
  styleUrls: ['../stories/stories.component.css']
})
export class AllpostComponent implements OnInit {
  comment = {num_likes:''};
  wid;
  image_data:any;
  image_data2:any;
  id = {photo_id: ''};
  comment_div:boolean;
  options;
  headers

  viewwcomments:any;
  com:any[];
  comment_like:any;
  constructor(private http: HttpClient, private router: Router,) {
    const authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
    this.headers    = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});
    this.options    = new RequestOptions({headers: this.headers});
  }

  ngOnInit() {
                this.comment_div = false;
                this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');

                const header = new HttpHeaders({'Content-Type': 'application/json'});
                header.append('Access-Control-Allow-Origin', '*');
                this.http.post('https://api.vowfest.com/story/list', {'wedding_id': this.wid},{headers : header}).subscribe((data) => {
                  console.log('DATA RECIEVED: ', data);
                  this.image_data  = data ;
                  this.image_data2 =  this.image_data.stories_list;
                  console.log( this.image_data2 );
              },(error) => {
                              console.log(error)
                              //this.router.navigate(['../error', JSON.stringify(error)]);
                            });
  }

  setlike(x){
    this.id.photo_id=x;
    this.http.post("https://api.vowfest.com/photo/like/add",this.id,this.options).subscribe((data)=>{
      this.getstory_list();
    },(error)=>{ 
                  console.log(error)
                  // this.router.navigate(['../error', JSON.stringify(error)]);
                })
  }
  add_comment(c){
    console.log(c.value);
    const header    = new HttpHeaders({'Content-Type': 'application/json'});
    header.append('Access-Control-Allow-Origin', '*');
    const authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    
    this.http.post("https://api.vowfest.com/photo/comment/add",c.value,{headers : this.headers}).subscribe((data)=>{
     console.log(data);
     this.getstory_list();
  },(error)=>{
                console.log(error)
                //this.router.navigate(['../error', JSON.stringify(error)]);
              })
  }
  divid;
  viewcomment(x){
  
    if( this.comment_div == false){
      this.comment_div = true;
    }else{
      this.comment_div = false;
    }
    this.divid = x;
    this.http.post('https://api.vowfest.com/photo/comment/list', {photo_id: x}).subscribe((data) => {
    this.viewwcomments = data;
    console.log(this.viewwcomments)
    this.com = this.viewwcomments.comments;
   
  },(error)=>{
                 console.log(error)
                // this.router.navigate(['../error', JSON.stringify(error)]);
              })
   
  }

  getstory_list(){
    this.wid     = localStorage.getItem('weddingID')  || localStorage.getItem('guest_wedding_id');
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    header.append('Access-Control-Allow-Origin', '*');
    this.http.post('https://api.vowfest.com/story/list', {'wedding_id': this.wid},{headers : header}).subscribe((data) => {
      console.log('DATA RECIEVED: ', data);
      this.image_data = data ;
        if( this.image_data.status == true){
                this.image_data2 =  this.image_data.stories_list;
                console.log( this.image_data2 );
                if(this.image_data2){
                  for (const comment_array of this.image_data2) {
                        
                    console.log(  comment_array.comments.length );
                      if(comment_array.comments.length == 0){
                            this.comment_div = false ;
                          }else{
                            this.comment_div = true;
                          }
                  }
                }
        }
         
  },(error) => { 
                  console.log(error)
                 //  this.router.navigate(['../error', JSON.stringify(error)]);
                });


  }

  add_counts_of_like(x){
    const header    = new HttpHeaders({'Content-Type': 'application/json'});
    const authToken =  localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
    header.append('Access-Control-Allow-Origin', '*');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    this.http.post("https://api.vowfest.com/photo/comment/like/add",{	comment_id: x},{headers : this.headers}).subscribe((data)=>{
     console.log(data);
    this.getstory_list();
    //this.view_counts_of_like(x);
  },(error)=>{
                console.log(error)
                //this.router.navigate(['../error', JSON.stringify(error)]);
              })

  }
  view_counts_of_like(x){
   
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    const authToken =  localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
    header.append('Access-Control-Allow-Origin', '*');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    this.http.post("https://api.vowfest.com/photo/comment/like/list",{	comment_id: x},{headers : this.headers}).subscribe((data)=>{
     console.log(data);
     this.comment_like = data;
     this.comment.num_likes = this.comment_like.num_likes;
     console.log( this.comment.num_likes )
  },(error)=>{
              console.log(error)
              //this.router.navigate(['../error', JSON.stringify(error)]);
            })
  }
}
