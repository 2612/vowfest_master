import { Component, ViewEncapsulation } from '@angular/core';
import {WebcamImage} from 'ngx-webcam';
@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css','../../node_modules/bootstrap/dist/css/bootstrap.min.css']
})
export class AppComponent {
  title = 'app';
 
}
