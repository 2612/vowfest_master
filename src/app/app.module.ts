import { BrowserModule } from '@angular/platform-browser';
import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http'; 
import { CommentModule } from 'ng2-comment';
import { DatePipe } from '@angular/common';
//import { MDBBootstrapModule } from 'angular-bootstrap-md';
//import { ToastrModule } from 'ngx-toastr';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
 
// import { NativeScriptModule } from "nativescript-angular/nativescript.module";
// // import {WebcamModule} from 'ngx-webcam';

// import { AppRoutingModule } from "./app.routing";
import { ShareButtonsModule } from 'ngx-sharebuttons';


import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
} from "angular-6-social-login-v2";
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { CalendarComponent } from './calendar/calendar.component';
import { GiftComponent } from './gift/gift.component';
import { GuestlistComponent } from './guestlist/guestlist.component';
import { InvitationComponent } from './invitation/invitation.component';
import { LoginComponent } from './login/login.component';
import { PhotoComponent } from './photo/photo.component';
import { ProfileComponent } from './profile/profile.component';
import { EngagementComponent } from './engagement/engagement.component';
import { GridGalleryComponent } from './grid-gallery/grid-gallery.component';
import { NavbarComponent } from './navbar/navbar.component';
import { GiftItemComponent } from './gift-item/gift-item.component';
import { GiftWishlistComponent } from './gift-wishlist/gift-wishlist.component';
import { GiftEditComponent } from './gift-edit/gift-edit.component';
import { AddEventComponent } from './add-event/add-event.component';
import { RegisterComponent } from './register/register.component';
import { DashbordComponent } from './dashbord/dashbord.component';
import { StoriesComponent } from './stories/stories.component';
import { EventListComponent } from './event-list/event-list.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { PhonenumberComponent } from './phonenumber/phonenumber.component';
import { AccountComponent } from './account/account.component';
import { AddStoriesComponent } from './add-stories/add-stories.component';
import { CameraComponent } from './camera/camera.component';
// import { WebCamModule } from 'ack-angular-webcam';
import {WebcamModule} from 'ngx-webcam';
import { SnapshotComponent } from './snapshot/snapshot.component';
import { ImagesComponent } from './images/images.component';
import { CommentComponent } from './comment/comment.component';
import { AllpostComponent } from './allpost/allpost.component';
import { CoupleProfileComponent } from './couple-profile/couple-profile.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactSettingComponent } from './contact-setting/contact-setting.component';
import { GuestProfileUpdateComponent } from './guest-profile-update/guest-profile-update.component';
import { ErrorComponent } from './error/error.component';
import { GuestDashboardComponent } from './guest-dashboard/guest-dashboard.component';
import { GuestSignupComponent } from './guest-signup/guest-signup.component';
import { SendInvitationComponent } from './send-invitation/send-invitation.component';



const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'phonenumber', component: PhonenumberComponent },
  { path: 'phonenumber/:data', component: PhonenumberComponent },
  { path: 'stories', component: StoriesComponent },
  { path: 'stories/:data', component: StoriesComponent },
  { path: 'profile', component: ProfileComponent},
  { path: 'profile/:data', component: ProfileComponent},
  { path: 'invitation_cards', component: CoupleProfileComponent},
  { path: 'allpost', component: AllpostComponent }, 
  { path: 'photo', component: PhotoComponent},
  { path: 'engagement', component: EngagementComponent}, 
  { path: 'grid', component: GridGalleryComponent},
  { path: 'calendar', component: CalendarComponent}, 
  { path: 'gift', component: GiftComponent},
  { path: 'gift-item', component: GiftItemComponent}, 
  { path: 'gift-lists', component: GiftWishlistComponent},
  { path: 'gift-edit/:data', component: GiftEditComponent}, 
  { path: 'add-event', component: AddEventComponent},
  { path: 'register', component: RegisterComponent},  
  { path: 'dashboard', component: DashbordComponent},
  { path: 'dashboard/:data', component: DashbordComponent},
  { path: 'edit-event', component: EditEventComponent},
  { path: 'gift-lists/:data', component: GiftWishlistComponent},
  { path: 'gift-edit/:data', component: GiftEditComponent},
  { path: 'event-list', component: EventListComponent},
  { path: 'event-list/:data', component: EventListComponent}, 
  { path: 'edit-event/:data', component: EditEventComponent},
  { path: 'invitation', component: InvitationComponent},
  { path: 'guest-list', component: GuestlistComponent}, 
  { path: 'account', component: AccountComponent},
  { path: 'add-stories', component: AddStoriesComponent},
  { path: 'add-stories/:data', component: AddStoriesComponent},
  { path: 'camera', component: CameraComponent},
  { path: 'snapshot/:data', component: SnapshotComponent},
  { path: 'snapshot', component: SnapshotComponent}, 
  { path: 'images', component: ImagesComponent},
  { path: 'images/:data', component: ImagesComponent},
  { path: 'comment', component: CommentComponent},
  { path: 'Contact-list', component: ContactListComponent},
  { path: 'Contact-Setting', component: ContactSettingComponent},
  { path: 'invite/:data', component: GuestSignupComponent},
  { path: 'Guest_Dashboard', component: GuestDashboardComponent},
  { path: 'Guest_Profile_Update', component: GuestProfileUpdateComponent},
  { path: 'error/:data', component: ErrorComponent},
  { path: 'SendInvitation', component: SendInvitationComponent}
 
  
]

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("939569453062-jkpvc37viho01u1c9hmkppt5pe15lpfd.apps.googleusercontent.com")
        }
         
      ])

  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    AllpostComponent,
    CoupleProfileComponent,
    CalendarComponent,
    GiftComponent,
    GuestlistComponent,
    InvitationComponent,
    LoginComponent,
    PhotoComponent,
    ProfileComponent,
    EngagementComponent,
    GridGalleryComponent,
    NavbarComponent,
    GiftItemComponent,
    GiftWishlistComponent,
    GiftEditComponent,
    AddEventComponent,
    RegisterComponent,
    DashbordComponent,
    StoriesComponent,
    EventListComponent,
    EditEventComponent,
    PhonenumberComponent,
    AddStoriesComponent,
    CameraComponent,
    SnapshotComponent,
    ImagesComponent,
    CommentComponent,
    AllpostComponent,
    CoupleProfileComponent,
    ContactListComponent,
    ContactSettingComponent,
    GuestProfileUpdateComponent,
    ErrorComponent,
    GuestDashboardComponent,
    GuestSignupComponent,
    SendInvitationComponent,
  ],
  imports: [
    SocialLoginModule,
    BrowserModule,
    AngularFontAwesomeModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    // WebCamModule,
    WebcamModule,
    HttpClientModule, // (Required) for share counts
    ShareButtonsModule.forRoot()

  ],
  providers : [
    DatePipe,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
    
  ],
  bootstrap: [AppComponent],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  
 }

// host user 
// name : jeetraj@gmail.com
// password : jeetraj123 

// guest user 
// name parv@gmail.com
// password : parv123 