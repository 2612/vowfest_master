import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { catchError, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  EventArray : any;
  wedding_id;
  eventdata=[];
  edata=[];
    
validate:boolean;
  constructor(private http: Http,private router: Router) { }

    ngOnInit(){
      var authToken =   localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');;
      if(!authToken) 
      {  this.router.navigate(['../login']); }        
    
        this.wedding_id = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
        this.http.post('https://api.vowfest.com/event/list',{"wedding_id":this.wedding_id}).pipe(map((res)=>res)).subscribe(( data )  =>  
        { 
          if(<any>data==null)
          {
            console.log("data is empty");
          }
          console.log(data);  
      },(error)=>{  console.log(error); 
                    //this.router.navigate(['../error', JSON.stringify(error)]);
                  });

      ///for guest user 
      let user_login_type:any ;
      user_login_type = JSON.parse(localStorage.getItem('userinfo')) ;
      user_login_type.login_type       = localStorage.getItem('login_type')
      console.log(user_login_type.login_type)
      if(user_login_type.login_type == 'guest'){
          this.validate = true;
          console.log( this.validate)
      }else{
          console.log( this.validate)
          this.validate = false;
      }            
    }
}