import { Component, OnInit, ViewChild,Output, ElementRef, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
// import { WebCamComponent } from 'ack-angular-webcam';
import {WebcamImage,WebcamInitError, WebcamUtil} from 'ngx-webcam';
import { Request, Http } from '@angular/http';
import { CameraService } from '../camera.service';
@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.css']
})

export class CameraComponent implements OnInit {
  click_to_open_file =true;
  chosefile = false;
  fileToUpload: Array<File> = [];
  img;
  formData: any = new FormData();
  im;
  wid;
  imagedata:any = {base64:"" , file:Array }
  imagedata2:any = {base64:"" , file:Array }
  story={photo_data:'',wedding_id:'',story:''}
  headers;
  @Output()
  public webcamImage: WebcamImage = null;

   
  handleImage2(webcamImage: WebcamImage) {
    this.webcamImage = webcamImage;
  }
  public ngOnInit(): void {
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      });
  }
    constructor(public http:Http, 
                private camera:CameraService,
                private router:Router){ }
    public pictureTaken = new EventEmitter<WebcamImage>();
    // toggle webcam on/off
    public showWebcam = true;
    public allowCameraSwitch = true;
    public multipleWebcamsAvailable = false;
    public deviceId: string;
    public videoOptions: MediaTrackConstraints = {
     // width: {ideal: 1024},
     // height: {ideal: 576}
    };
    public errors: WebcamInitError[] = [];
  
    // webcam snapshot trigger
    private trigger: Subject<void> = new Subject<void>();
    // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
    private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();
  
  
    public triggerSnapshot(): void {
      this.trigger.next();
    }
  
    public toggleWebcam(): void {
      this.showWebcam = !this.showWebcam;
    }
  
    public handleInitError(error: WebcamInitError): void {
      this.errors.push(error);
    }
  
    public showNextWebcam(directionOrDeviceId: boolean|string): void {
      // true => move forward through devices
      // false => move backwards through devices
      // string => move to device with given deviceId
      this.nextWebcam.next(directionOrDeviceId);
    }
  
    public handleImage(webcamImage: WebcamImage): void {
        // console.info('received webcam image $$$', webcamImage.imageAsDataUrl);
        //console.log(webcamImage);
        const date = new Date().valueOf();
        // Replace extension according to your media type
        //console.log(date)
         const imageName = date + '.jpeg';
        // const imageFile = new File([webcamImage.imageAsBase64], imageName)
        // this.formData.append('photos_data',imageFile, imageName)
        // this.formData.forEach(element => {
        //   console.log(element )
        //   this.imagedata2 = {base64: webcamImage.imageAsDataUrl, file: element}
        //   this.camera.setimage( this.imagedata2);
        //   console.log(this.imagedata2 )
        //  this.router.navigate(["../snapshot"])
        // });

        //this.pictureTaken.emit(webcamImage);

        let encodedBase64Picture : any ;
        var file: File = new File([webcamImage.imageAsBase64], imageName) ;
        var reader: FileReader = new FileReader();
        reader.onloadend = ( e ) => {
          console.log(e);
       
        encodedBase64Picture = reader.result;
        }
        console.log(file);
        this.imagedata2 = {base64: webcamImage.imageAsDataUrl, file: file,type: 'image/jpeg'}
            this.camera.setimage( this.imagedata2);
            console.log(this.imagedata2 )
             this.router.navigate(["../snapshot"])
        reader.readAsDataURL( file );
        // console.log(reader.readAsDataURL( file ));
        return encodedBase64Picture  ;
    }
  
    public cameraWasSwitched(deviceId: string): void {
      console.log('active device: ' + deviceId);
      this.deviceId = deviceId;
    }
  
    public get triggerObservable(): Observable<void> {
      return this.trigger.asObservable();
    }
  
    public get nextWebcamObservable(): Observable<boolean|string> {
      return this.nextWebcam.asObservable();
    }

handleFileInput(file: FileList) {
                                    for (let i = 0; i < file.length; i++) {
                                      const element = file[i];
                                      this.fileToUpload.push(element);
                                      const files: Array<File> = this.fileToUpload;
                                      const myReader: FileReader = new FileReader();
                                      myReader.onloadend = (e) => {
                                        this.img = myReader.result;
                                        this.formData.append('photos_data', files[i], files[i].name);
                                      };
                                      myReader.readAsDataURL(element);
                                    }
                                    this.chosefile = false;
                                    this.click_to_open_file = true;
}

addstory(f){
      this.chosefile = true;
      this.click_to_open_file = false;
      this.formData.forEach(element => {
                                        this.imagedata = {base64: this.img, file: element}
                                        this.camera.setimage( this.imagedata);
                                        console.log(this.imagedata )
                                        this.router.navigate(["../snapshot"])
      });
}

}