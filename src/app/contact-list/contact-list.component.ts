import { Component, OnInit, NgZone, Injectable, Optional  } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders} from '@angular/common/http';


declare var gapi: any;
@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  constructor(private router: Router ,private http: HttpClient) {  }
  wid;
  headers;
  msg:string
  emailmsg:string;
  public shouldShow = true;
  authConfig;
  userContacts;
  transformToMailListModel;
  onFailure;
  ngOnInit() {
    this.wid = localStorage.getItem('weddingID')  || localStorage.getItem('guest_wedding_id')
    this.msg = 'https://wa.me/?text=Hi ,We are excited to invite you for our wedding . Please sign up here "https://www.vowfest.com/invite/'+this.wid+'"  to be a part of the wedding fun . We are waiting,love  varushka , anushka'
    this.emailmsg = 'mailto:?subject=&body=Hi We are excited to invite you for our wedding . Please sign up here "https://www.vowfest.com/invite/'+this.wid+'"  to be a part of the wedding fun . We are waiting,love  varushka , anushka';
   // this.signIn()

    this.authConfig = {
      client_id: 'AIzaSyARvwirFktEIi_BTaKcCi9Ja-m3IEJYIRk',
      scope: 'https://www.googleapis.com/auth/contacts.readonly'
    };
  }
 
  contact_setting(){
    this.router.navigate(['../Contact-Setting']);
  }

  send_tocreate_guest(){
   
     var c = decodeURIComponent('?weddingid=' + this.wid) ;
     console.log(c);
     this.router.navigate(['../Contact-Setting']);
  }

  add(){
         
          const authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');;
          const headers = new Headers();
          headers.append('Accept', 'application/json');
          headers.append('Content-Type', 'application/json');
          headers.append('Authorization', authToken);
          this.headers = new HttpHeaders({'Authorization': authToken});
        
          this.http.post( 'https://api.vowfest.com/invitees/add',
          {
            "contacts_list": [
                {
                    "invited": true,
                    "name": "mahi",
                    "phone": "8839380364",
                    "rsvp": false
                },
                {
                    "invited": true,
                    "name": "lalit",
                    "phone": "9284858141",
                    "rsvp": false
                },
                {
                    "invited": true,
                    "name": "manasi",
                    "phone": "8600141828",
                    "rsvp": false
                }
            ]
        },{headers: this.headers}).subscribe(( data )  => {
                                                            console.log('response: ', data);
                                                          },
                                                error => {
                                                            console.log(error);   
                                                            //this.router.navigate(['../error', JSON.stringify(error)]);
                                                          }
                        );


        
    
  }
  //gggggggg
    // ngAfterViewInit(): void {
    //   setTimeout(() => this.signIn(), 1000);
    // }


    // signIn() {
    //   alert("dvd")
    //   this.googleContacts()
    // }

    // googleContacts() {
    //   gapi.client.setApiKey('AIzaSyARvwirFktEIi_BTaKcCi9Ja-m3IEJYIRk');
    //   gapi.auth2.authorize(this.authConfig, this.handleAuthorization);
    // }
    
    handleAuthorization = (authorizationResult) => {
      if (authorizationResult && !authorizationResult.error) {
        let url: string = "https://www.google.com/m8/feeds/contacts/default/thin?" +
           "alt=json&max-results=500&v=3.0&access_token=" +
           authorizationResult.access_token;
        console.log("Authorization success, URL: ", url);
        this.http.get<any>(url)
          .subscribe(
            response => {
              if (response.feed && response.feed.entry) {
                console.log(response.feed.entry);
              }
            }
          )
      }
    }
 
}
