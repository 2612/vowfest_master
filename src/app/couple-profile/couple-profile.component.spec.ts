import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoupleProfileComponent } from './couple-profile.component';

describe('CoupleProfileComponent', () => {
  let component: CoupleProfileComponent;
  let fixture: ComponentFixture<CoupleProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoupleProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoupleProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
