import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders,HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';

@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.css']
})
export class DashbordComponent implements OnInit {

  image_data2:any;
  wid;
  spinner = true ;
  image_data:any ={status: '' };
  comment_div:boolean;
  comment = {num_likes:''};
  id = {photo_id: ''};
  options;
  headers;
  viewwcomments:any;
  com:any[];
  comment_like:any;
  name;
  better_half;
  propic
  constructor(private http: HttpClient, private router: Router, private next: HttpHandler) {
    this.wid         = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    this.options = new RequestOptions({headers: this.headers});
   }

  ngOnInit() {
                  this.name          =  JSON.parse(localStorage.getItem('userinfo')).name || localStorage.getItem('guest_name')
                  this.better_half   =  localStorage.getItem('userinfo') ? JSON.parse(localStorage.getItem('userinfo')).better_half : "no Betterhalf yet";
                  this.propic        =  localStorage.getItem('userinfo') ? JSON.parse(localStorage.getItem('userinfo')).profile_photo : localStorage.getItem('guest_profile_photo')
                  this.comment_div = false;
                  this.getstory_list();
  }
  setlike(x){
    this.id.photo_id=x;
    this.http.post("https://api.vowfest.com/photo/like/add",this.id,this.options).subscribe((data)=>{
      this.getstory_list();
    },(error)=>{ 
                  console.log(error)
                 // this.router.navigate(['../error', JSON.stringify(error)]);
                })
  }
  add_comment(c){
    console.log(c.value);
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');

    header.append('Access-Control-Allow-Origin', '*');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    this.http.post("https://api.vowfest.com/photo/comment/add",c.value,{headers : this.headers}).subscribe((data)=>{
     console.log(data);
     this.getstory_list();
  },(error)=>{
                console.log(error)
                //this.router.navigate(['../error', JSON.stringify(error)]);
              })
  }
  divid;
  viewcomment(x){
  
    if( this.comment_div == false){
      this.comment_div = true;
    }else{
      this.comment_div = false;
    }
    this.divid = x;
    this.http.post('https://api.vowfest.com/photo/comment/list', {photo_id: x}).subscribe((data) => {
    this.viewwcomments = data;
    console.log(this.viewwcomments)
    this.com = this.viewwcomments.comments;
   
  },(error)=>{
                 console.log(error)
                 //this.router.navigate(['../error', JSON.stringify(error)]);
              })
   
  }
  getstory_list(){
    this.wid         = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    header.append('Access-Control-Allow-Origin', '*');
    this.http.post('https://api.vowfest.com/story/list', {'wedding_id': this.wid},{headers : header}).subscribe((data) => {
      console.log('DATA RECIEVED: ', data);
      console.log(this.image_data)
      this.image_data = data ;
      this.spinner = false ; 
      console.log(this.image_data)
      let delayedObservable = Observable.of(this.image_data).delay(5000);
      delayedObservable.subscribe(data => console.log(data));
      if( this.image_data.status == true){
                this.image_data2 =  this.image_data.stories_list;
                // for(let i= 0 ; i< this.image_data.stories_list.length ; i++ ){
                //   this.image_data2[i] = this.image_data.stories_list[i];
                //   console.log( this.image_data2[i])
                // }
                console.log( this.image_data2 );
                if(this.image_data2){
                  for (const comment_array of this.image_data2) {
                        
                      console.log(  comment_array.comments.length );
                      if(comment_array.comments.length == 0){
                            this.comment_div = false ;
                          }else{
                            this.comment_div = true;
                          }
                  }
                }
        }
         
  },(error) => {
                  console.log(error)
                 // this.router.navigate(['../error', JSON.stringify(error)]);
                });


  }
  add_counts_of_like(x){
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');

    header.append('Access-Control-Allow-Origin', '*');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    this.http.post("https://api.vowfest.com/photo/comment/like/add",{	comment_id: x},{headers : this.headers}).subscribe((data)=>{
     console.log(data);
    this.getstory_list();
    //this.view_counts_of_like(x);
  },(error)=>{
                console.log(error)
                //this.router.navigate(['../error', JSON.stringify(error)]);
              })

  }
  view_counts_of_like(x){
   
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');

    header.append('Access-Control-Allow-Origin', '*');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    this.http.post("https://api.vowfest.com/photo/comment/like/list",{	comment_id: x},{headers : this.headers}).subscribe((data)=>{
     console.log(data);
     this.comment_like = data;
     this.comment.num_likes = this.comment_like.num_likes;
     console.log( this.comment.num_likes )
  },(error)=>{
              console.log(error)
              //this.router.navigate(['../error', JSON.stringify(error)]);
            })
  }


}
