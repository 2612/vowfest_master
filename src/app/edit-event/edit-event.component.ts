import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { catchError, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit {
  eventdata:any;
  eve={programe:{}};
  prog;
  pr=[];
  eid={event_id:''};
  headers;
  options;
  programs_list = [{program_name:"",program_time:""}]
  constructor(private http: HttpClient,private router: Router,private activatedroute: ActivatedRoute) { }

  ngOnInit() {
  
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
   
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', authToken);
    this.headers = new HttpHeaders({'Authorization': authToken});
    this.options = new RequestOptions({headers: this.headers});
    this.eventdata = JSON.parse(this.activatedroute.snapshot.params['data']);
    console.log(this.eventdata)
    console.log(this.eventdata.name);
  }

  edit(eventedit)
  {
    console.log(eventedit.value)
    const eventupdate = {
      "event_id": eventedit.value.event_id,
        "event_name": eventedit.value.event_name,
        "event_date": eventedit.value.event_date,
        "event_venue": eventedit.value.event_venue,
        "programs_list": [
            {
                "program_name": eventedit.value.program_name,
                "program_time": eventedit.value.program_time
            }
        ]
    }
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
  
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
   
    headers.append('Authorization', authToken);
    this.headers = new HttpHeaders({'Authorization': authToken});
    this.eve.programe=JSON.stringify(this.eve.programe);
    this.http.post('https://api.vowfest.com/event/modify', eventupdate,{headers: this.headers}).subscribe((data)=>{
          console.log(data);
          this.router.navigate(['../event-list/']);
        },error=>{  
                    console.log(error);
                    //this.router.navigate(['../error', JSON.stringify(error)]);
                  })

 
  }

  delete(x){
  
    this.http.post('https://api.vowfest.com/event/delete',{event_id:x},{headers: this.headers}).subscribe((data)=>{
          console.log("event is deleted #######",data);
          this.router.navigate(['../event-list/']);
        },(error)=>{ console.log(error);  
                     //this.router.navigate(['../error', JSON.stringify(error)]);
          })
  }
}
