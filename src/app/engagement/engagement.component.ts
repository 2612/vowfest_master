import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpClient, HttpHeaders} from '@angular/common/http';
@Component({
  selector: 'app-engagement',
  templateUrl: './engagement.component.html',
  styleUrls: ['./engagement.component.css']
})
export class EngagementComponent implements OnInit {

    constructor(private http: Http,private router: Router,private httpclient: HttpClient) { }
    headers;
    about_us_data = {profile_photo:'' ,about:'',phone_num:'',better_half:''}
    about;
    formData: any = new FormData();
  ngOnInit()
   {
      // wedding/details
      this.user_about_details();
    }
 
     user_about_details(){
              
              
              const weddingID        = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
              const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
             
           
              const headers = new Headers();
            
              this.http.post('https://api.vowfest.com/wedding/details',{ "wedding_id": weddingID },{headers: this.headers}).subscribe( (data) => {    
                console.log(data.json());
                this.about_us_data = data.json();     
              },(error)=>{console.log(error); 
                //  this.router.navigate(['../error', JSON.stringify(error)]);
                });
     }

     update_aboutus(a){
 
      this.formData.append('about', a.value.about);
      this.formData.append('better_half', a.value.better_half);
      this.formData.append('phone_num', a.value.phone_num);
      // this.formData.append('profile_photo', a.value.profile_photo);
        
     
      const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
    
       const header = new HttpHeaders({'Authorization': authToken});
       this.httpclient.post('https://api.vowfest.com/updateProfile', this.formData,{headers: header}).subscribe( (data) => {    
        console.log(data);
        this.user_about_details()
      },(error)=>{  console.log(error); 
                    //  this.router.navigate(['../error', JSON.stringify(error)]);
        });
     }

}
