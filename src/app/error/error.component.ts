import { Component, OnInit } from '@angular/core';
import { Router ,ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  constructor(private activatedroute: ActivatedRoute) { }
  error:any;
  ngOnInit() {
   
    this.error = JSON.parse(this.activatedroute.snapshot.params['data'])
    console.log(this.error)
  }

}
