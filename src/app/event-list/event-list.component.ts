import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {
  eventdata;
  event_list
  edata=[];
  wedding_ids={wedding_id:''};
  validate:boolean;
  progress_bar_div = false;
  constructor(private http: HttpClient,private router: Router) { }

  ngOnInit() {
   
    this.wedding_ids.wedding_id=localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
    this.http.post('https://api.vowfest.com/event/list',this.wedding_ids).subscribe(( data )  =>  
    { 
      this.eventdata = data;
      console.log(this.eventdata)
    },(error)=>{    console.log(error)
                // this.router.navigate(['../error', JSON.stringify(error)]);
              })

              let user_login_type:any ;
              user_login_type = JSON.parse(localStorage.getItem('userinfo')) ;
              console.log(user_login_type.login_type)
                 if(user_login_type.login_type == 'guest'){
                     this.validate = true;
                     console.log( this.validate)
                 }else{
                     console.log( this.validate)
                     this.validate = false;
                 }
  }

  editevent(x){
    console.log("****************",this.edata);
    console.log("$$$$$$$$$%%%%%%",x);
   
        this.router.navigate(['../edit-event/',JSON.stringify(x)]);
        
 
    
  }

  show_progress_bar(){
    this.progress_bar_div = true ;
  }
}