import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-gift-edit',
  templateUrl: './gift-edit.component.html',
  styleUrls: ['./gift-edit.component.css']
})
export class GiftEditComponent implements OnInit {
  gift_data:any;
  giftID;
  headers
  
  constructor(private http: HttpClient,private router: Router,private activatedroute: ActivatedRoute) { }

  ngOnInit() {
    console.log(JSON.parse(this.activatedroute.snapshot.params['data']))
    this.gift_data = JSON.parse(this.activatedroute.snapshot.params['data'])
    
  }
  Edit_gift(x){
   
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
      const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
 
    headers.append('Authorization', authToken);
    this.headers = new HttpHeaders({'Authorization': authToken});
    this.http.post('https://api.vowfest.com/gift/modify',x.value ,{headers: this.headers}).subscribe((result: any) => {
      console.log(result);
     
      this.router.navigate(['../gift-lists']);
    }, error => {console.log('There was an error: ', error);
                // this.router.navigate(['../error', JSON.stringify(error)]);
              }
               );
  }


}
