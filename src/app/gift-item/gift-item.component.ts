import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { catchError, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-gift-item',
  templateUrl: './gift-item.component.html',
  styleUrls: ['./gift-item.component.css']
})
export class GiftItemComponent implements OnInit {
item={ gift_name:'',gift_link:''};
gift_name;
gift_link;
it;
headers;
options;

  constructor(private router:Router,private http: HttpClient) { }

  ngOnInit() {  
  }
  gift_item(a) {

                 

                  const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
                  this.headers = new HttpHeaders({'Authorization': authToken});
                  this.options = new RequestOptions({headers: this.headers});
                  this.http.post('https://api.vowfest.com/gift/add', a.value, {headers: this.headers})
                  .subscribe(data => {
                                        console.log(data)
                                        this.router.navigate(['../gift-lists']);
                  },error =>{  
                              // this.router.navigate(['../error', JSON.stringify(error)]);
                        });
  }

}
