import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftWishlistComponent } from './gift-wishlist.component';

describe('GiftWishlistComponent', () => {
  let component: GiftWishlistComponent;
  let fixture: ComponentFixture<GiftWishlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftWishlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftWishlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
