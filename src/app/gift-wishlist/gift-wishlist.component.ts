import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Http,Headers } from '@angular/http';
@Component({
  selector: 'app-gift-wishlist',
  templateUrl: './gift-wishlist.component.html',
  styleUrls: ['./gift-wishlist.component.css']
})
export class GiftWishlistComponent implements OnInit {
  data=[];
  showdata={name:'',link:''};
  gift;
  giftname=[];
  gifts=[];
  el1;

  constructor(private activatedroute: ActivatedRoute,private http: Http,private router: Router) { }

  ngOnInit() {
    const wedding_id = {'wedding_id':  localStorage.getItem('weddingID') };
   
    console.log(wedding_id);

      this.http.post('https://api.vowfest.com/gift/list', wedding_id).subscribe((data) => {
      
      this.giftname = data.json().gift_list;
      console.log( data.json());
      },error=>{  
        console.log( error);
        //this.router.navigate(['../error', JSON.stringify(error)]);
      });
  }

  editgift(x){
               
                this.router.navigate(['../gift-edit',JSON.stringify(x)]);
              
  }

  deletegift(a,x){
    console.log(a)
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
 
    const headers = new Headers({'Authorization': authToken});
    this.http.post('https://api.vowfest.com/gift/delete', {
      "gift_id": x
    },{headers: headers}).subscribe((data) => {
      console.log(data.json());
      this.giftname.splice(a, 1);
      },error=> {  
                  //this.router.navigate(['../error', JSON.stringify(error)]);
      });
  }
} 