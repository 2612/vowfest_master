import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-gift',
  templateUrl: './gift.component.html',
  styleUrls: ['./gift.component.css']
})
export class GiftComponent implements OnInit {
  GIftArray:any ={gift_list:[]};

  
validate:boolean;
  constructor(private http: HttpClient,private router: Router) { }

  ngOnInit() {   
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
   
    if(!authToken) 
    {this.router.navigate(['../login']);}        
    
    let user_login_type:any ;
     user_login_type = JSON.parse(localStorage.getItem('userinfo')) ;
     user_login_type.login_type       = localStorage.getItem('login_type')
     console.log(user_login_type.login_type)
        if(user_login_type.login_type == 'guest'){
            this.validate = true;
            console.log( this.validate)
        }else{
            console.log( this.validate)
            this.validate = false;
        }

          const wedding_id = {'wedding_id':  localStorage.getItem('weddingID') };
          console.log(wedding_id);
          this.http.post('https://api.vowfest.com/gift/list', wedding_id).subscribe((data) => {
            this.GIftArray = data;
            console.log(  this.GIftArray)
          },error=>{  
            console.log( error);
            //this.router.navigate(['../error', JSON.stringify(error)]);
          });

}
}