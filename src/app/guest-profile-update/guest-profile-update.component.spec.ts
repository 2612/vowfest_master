import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileUpdateComponent } from './guest-profile-update.component';

describe('GuestProfileUpdateComponent', () => {
  let component: GuestProfileUpdateComponent;
  let fixture: ComponentFixture<GuestProfileUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestProfileUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
