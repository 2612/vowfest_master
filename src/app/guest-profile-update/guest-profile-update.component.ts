
import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileService } from '../profile.service';
@Component({
  selector: 'app-guest-profile-update',
  templateUrl: './guest-profile-update.component.html',
  styleUrls: ['./guest-profile-update.component.css']
})
export class GuestProfileUpdateComponent implements OnInit {
  ProfileArray: any;
  pro = {relationship: '', phone_num: ''};
  prof = {name: '', file: {}};
  imageUrl;
  name;
  data = [];
  header;
  fileToUpload: File = null;
  userdata:{};
  formData: any = new FormData();
  phone_num;
  headers;
  options;
  wedding_data;
  file
  constructor(private http: HttpClient, private router: Router, private activatedroute: ActivatedRoute, private profile:ProfileService) { }

    ngOnInit() {

            }
    handleFileInput(file: FileList) {
                                        this.fileToUpload = file.item(0);
                                        console.log('File to upload: ', this.fileToUpload);
                                        this.prof.file = this.fileToUpload;
                                        this.profile.setData(this.fileToUpload);
                                        // Show image preview
                                        const reader = new FileReader();
                                        reader.onload = (event: any) => {
                                        this.imageUrl = event.target.result;
                                        };
                                        reader.readAsDataURL(this.fileToUpload);
    }
    OnSubmit(Image) {
          
            this.userdata = {
              relationship: Image.value.relationship ,
              phone_num: Image.value.phone_num,
              profile_photo:  this.imageUrl 
            };
            console.log(this.userdata)
            this.formData.append('relationship', Image.value.relationship);
            this.formData.append('phone_num', Image.value.phone_num);
            this.formData.append('profile_photo',  this.fileToUpload );
            localStorage.setItem( 'guest_profile_photo' , this.imageUrl );
            console.log( this.formData)
            this.file=this.profile.getData();
            const header = new HttpHeaders({'Authorization': localStorage.getItem('guest_invite_authtoken')});
         
          this.http.post('https://api.vowfest.com/guest/updateProfile', this.formData,{  headers: header } ).subscribe(
          data => {
                      console.log(data)
                      this.wedding_data = data
                      localStorage.setItem( 'guest_wedding_id' , this.wedding_data.wedding_id );
                      this.router.navigate(['../../Guest_Dashboard/'])
                  },error =>( console.log(error) ));
                            // this.router.navigate(['../../dashboard/'])
  
    }

}
