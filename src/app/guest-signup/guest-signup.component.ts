import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router ,ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-guest-signup',
  templateUrl: './guest-signup.component.html',
  styleUrls: ['./guest-signup.component.css']
})
export class GuestSignupComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private activatedroute: ActivatedRoute) { }
  invite_authtoken;
  
  user = {name:'',email:'',password:''}

  a:any;
  username;
  password_error;
  non_field_errors;
  msg;
 
  ngOnInit() {
                localStorage.setItem('login_type',"guest");
                console.log("dbd")
  }
  onSubmit(user) {
    console.log(user)
    console.log("dfds");
           localStorage.setItem( 'guest_name' , user.name);
            debugger
           const header = new HttpHeaders({'Content-Type': 'application/json'});
         
            this.http.post('https://api.vowfest.com/guest/createAccount', {
              email:user.email,
              password: user.password,
              name: user.name,
              wedding_id: this.activatedroute.snapshot.params['data'] 
              },{headers: header}).subscribe(( data )  => {
              console.log('response: ', data);
              this.invite_authtoken = data;
            
              localStorage.setItem( 'guest_invite_authtoken' , this.invite_authtoken.token );
              this.router.navigate(['../../Guest_Profile_Update']);
              
                },error => {
                              console.log(error);
                             // this.router.(['../error', JSON.stringify(error)]);
               });
    }

    loginonSubmit(f) {
      console.log(f.value);
      const header = new HttpHeaders({'Content-Type': 'application/json'});
      header.append('Access-Control-Allow-Origin', '*');
      this.http.post('https://api.vowfest.com/login',
          f.value, {headers: header}).subscribe(( data )  => {
                    this.a = data;
                    console.log(this.a)
                    localStorage.setItem('userToken', this.a.token);
                    localStorage.setItem('userinfo',JSON.stringify(this.a));
                    localStorage.setItem('weddingID', this.a.wedding_id);
                    if (this.a.wedding_id !== null ) {
                      
                      // this.router.navigate(['../dashboard/']);
                      this.router.navigate(['../stories/']);
                      
                    } else {
                              this.router.navigate(['../profile/', JSON.stringify(data)]);
                    }if (this.a.wedding_id === undefined) {
                                                            this.router.navigate(['../profile/']);
                                                          }
          },error => {  
                      
                        if(error.error.status == false)
                          {   
                            this.router.navigate(['../register']);
                          }else{  console.log(error)
                                  //this.router.navigate(['../error',JSON.stringify(error)]);
                          }
                        this.msg = error.error.msg;
          });
}  
}
