import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-guestlist',
  templateUrl: './guestlist.component.html',
  styleUrls: ['./guestlist.component.css']
})
export class GuestlistComponent implements OnInit {

  constructor(private router: Router) { }
  public shouldShow = true;
  ngOnInit() {
  }
  addpic(){
    this.router.navigate(['../Contact-list']);
  }
}
