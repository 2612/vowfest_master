
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { catchError, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {
data = {album_id: '', photos_data: ''};
album_id;
ProfileArray = [];
album = [];
image:any = {photos_list:''};
img;
options;
formData: any = new FormData();
album_images = [];
headers;
imageUrl = [];
fileToUpload: Array<File> = [];
photo_ui:boolean;
stories_ui:boolean;
validate:boolean;
  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
                this.stories_ui = false;
                this.photo_ui = true;
                this.album_id = JSON.parse(this.route.snapshot.paramMap.get('data'));
              
                console.log(this.album_id.album_id);
               
                const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
                this.headers = new HttpHeaders({'Authorization': authToken});
                this.options = new RequestOptions({headers: this.headers});
                  if (!authToken) {  this.router.navigate(['../login']);
                  }
    
               this.api();
                let user_login_type:any ;
                user_login_type = JSON.parse(localStorage.getItem('userinfo')) ;
                user_login_type.login_type       = localStorage.getItem('login_type')
                console.log(user_login_type)
                   if(user_login_type.login_type == 'guest'){
                       this.validate = true;
                       console.log( this.validate)
                   }else{
                           this.validate = false;
                   }
  }

  handleFileInput(file: FileList) {
    console.log('FILE: ', file);
    for (let i = 0; i < file.length; i++) {
      const element = file[i];

    // console.log('%%%%%%%%%%', element);
      this.fileToUpload.push(element);
      const files: Array<File> = this.fileToUpload;
      console.log('@@@###', files[i]);
      const myReader: FileReader = new FileReader();

      myReader.onloadend = (e) => {
        this.img = myReader.result;
        this.formData.append('photos_data', files[i], files[i].name);
        // tslint:disable-next-line:no-shadowed-variable
        this.formData.forEach(element => {
          console.log('@@@###@@@@', element);
        });
        // console.log( '@@@###@@@@', this.img);
      };
      myReader.readAsDataURL(element);
    }
  }

  onSubmit(f) {
 
                  this.formData.append('album_id',this.album_id.album_id);
                  this.formData.append('photos_data',this.fileToUpload);
                  console.log("PHOTO DATA: ",this.formData.get("photos_data"));
                  console.log(this.formData);
                 
                  this.http.post('https://api.vowfest.com/albums/photos/upload',
                  this.formData, {headers: this.headers}).pipe(map(response => response))
                      .subscribe(result => { console.log('######', result);
                        
                          this.http.post('https://api.vowfest.com/albums/photos/list',
                           {album_id:this.album_id.album_id} , this.options ).subscribe((res) => {
                                      console.log(res)
                                      this.image = res;
                                  console.log(this.image.photos_list);
                          
                          },error=> {console.log(error);  
                                    //this.router.navigate(['../error', JSON.stringify(error)]);
                                    });
                      },error=>{  console.log(error);  
                                //this.router.navigate(['../error', JSON.stringify(error)]);
                                });
                                 //debugger;

  }

  showDropdown() {
    document.getElementById("myDropdown").classList.toggle("show");
  }
  
  storigrid(){
    this.stories_ui = false;
    this.photo_ui = true;
  }

  photolist(){
    this.http.post('https://api.vowfest.com/albums/photos/list', {"album_id":this.album_id.album_id} , this.options ).subscribe(
      (data) => {
                  this.image = data;
                  console.log(this.image.photos_list);
                },(error)=>{console.log(error);});
  }

  photogrid(){
    this.stories_ui = true;
    this.photo_ui = false;
  }
api(){
  this.http.post('https://api.vowfest.com/albums/photos/list',
  {album_id:this.album_id.album_id} , this.options ).subscribe((res) => {
             console.log(res)
             this.image = res;
             
         console.log(this.image.photos_list);
 
 },error=> {console.log(error);  
           //this.router.navigate(['../error', JSON.stringify(error)]);
           });

}
}