import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css']
})
export class InvitationComponent implements OnInit {
  InviteArray = [];
  public shouldShow = true;
  photo_select_grid = false;
  constructor(private http: HttpClient,private router: Router) { }

  ngOnInit() {   
 
  }
  addpic(){
             if(this.photo_select_grid == false){
              this.photo_select_grid = true;
             }else{
              this.photo_select_grid = false;
             }
   
    //this.router.navigate(['../SendInvitation']);
  }
}