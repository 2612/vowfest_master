import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import {
  AuthService,
  GoogleLoginProvider,
  SocialLoginModule,
} from 'angular-6-social-login-v2';
import{ProfileService} from '../profile.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [SocialLoginModule, AuthService]
})
export class LoginComponent implements OnInit {

  non_field_errors;
  password_error;
  a:any;
  username;
  email;
  password;
  msg;
  userdata = {name:'' , email: '', password: ''};
  userdata_google_signin = { email: '', password: ''};
  constructor(private socialAuthService: AuthService, private http: HttpClient , private router: Router, private pro:ProfileService) { }
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
     if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } 
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);
        // Now sign-in with userData
        // ...
        this.userdata.name = userData.name;
        this.userdata.email = userData.email;
        this.userdata.password = userData.email;
        this.userdata_google_signin.password = userData.email;
        this.userdata_google_signin.email = userData.email
        this.signupwith_google();
            
      }
    );
  }

  ngOnInit() {
                  localStorage.clear();
  }
  onSubmit(f) {   
                //  f.value.email = this.userdata.email;
                //  f.value.password = this.userdata.password;
                //  f.value.name = this.userdata.name; 
                  console.log(f.value);
                  const header = new HttpHeaders({'Content-Type': 'application/json'});
                  header.append('Access-Control-Allow-Origin', '*');
                  this.http.post('https://api.vowfest.com/login',
                      f.value, {headers: header}).subscribe(( data )  => {
                                this.a = data;
                                console.log(this.a)
                                localStorage.setItem('userToken', this.a.token);
                                localStorage.setItem('userinfo',JSON.stringify(this.a));
                                localStorage.setItem('weddingID', this.a.wedding_id);
                                if (this.a.wedding_id !== null) {
                                         if(this.a.login_type == 'host'){
                                          this.router.navigate(['../stories/']);
                                         }else{
                                          this.router.navigate(['../dashboard/']);
                                         }

                                } else {
                                          this.router.navigate(['../profile/', JSON.stringify(data)]);
                                }if (this.a.wedding_id === undefined) {
                                                                        this.router.navigate(['../profile/']);
                                                                      }
                      },error => {  
                                  
                                    if(error.error.status == false)
                                      {   
                                        this.router.navigate(['../register']);
                                      }else{  console.log(error)
                                              //this.router.navigate(['../error',JSON.stringify(error)]);
                                      }
                                    this.msg = error.error.msg;
                      });
  }

  signupwith_google(){
    const header = new HttpHeaders({'Content-Type': 'application/json'});
                    
     console.log(this.userdata);
      this.http.post('https://api.vowfest.com/createAccount',  this.userdata,{headers: header}).subscribe(( data )  => {
        console.log('response: ', data);
          this.a = data;
          localStorage.setItem('userToken', this.a.token);
          localStorage.setItem('weddingID', this.a.wedding_id);
         this.router.navigate(['../profile/', JSON.stringify(this.userdata)]);
          alert('You are registered now');
          },error => {
               
               
                console.log(error);
                console.log(this.userdata_google_signin);
                const header = new HttpHeaders({'Content-Type': 'application/json'});
                header.append('Access-Control-Allow-Origin', '*');
                this.http.post('https://api.vowfest.com/login',
                {email:this.userdata_google_signin.email ,password: this.userdata_google_signin.email}, {headers: header}).subscribe(( data )  => {
                              this.a = data;
                              console.log(this.a)
                              localStorage.setItem('userToken', this.a.token);
                              localStorage.setItem('userinfo',JSON.stringify(this.a));
                              localStorage.setItem('weddingID', this.a.wedding_id);
                              if (this.a.wedding_id !== null ) {
                                
                                // this.router.navigate(['../dashboard/']);
                                this.router.navigate(['../stories/']);
                                
                              } else {
                                        this.router.navigate(['../profile/', JSON.stringify(data)]);
                              }if (this.a.wedding_id === undefined) {
                                                                      this.router.navigate(['../profile/']);
                                                                    }
                    },error => {  
                                
                                  if(error.error.status == false)
                                    {   
                                      this.router.navigate(['../register']);
                                    }else{  console.log(error)
                                            //this.router.navigate(['../error',JSON.stringify(error)]);
                                    }
                                  this.msg = error.error.msg;
                    });
      });
  }
}
