import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  eventdata;
  event_list
  edata=[];
  wedding_ids={wedding_id:''};
  constructor(private router: Router,private http: HttpClient) { }

  ngOnInit() {
  }
  rout(){
    if( localStorage.getItem('guest_invites') == 'true')
    { this.router.navigate(['../stories']);}
    else
    { this.router.navigate(['../dashboard']);}

  }
  calendar(){
                this.wedding_ids.wedding_id=localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
                this.http.post('https://api.vowfest.com/event/list',this.wedding_ids).subscribe(( data )  =>  
                { 
                  this.eventdata = data;
                  if(this.eventdata.status == true)
                  { this.router.navigate(['../event-list']);}else{
                    this.router.navigate(['../calendar']);
                  }
                },(error)=>{    console.log(error)
                            // this.router.navigate(['../error', JSON.stringify(error)]);
                          })

                  
  }
}
