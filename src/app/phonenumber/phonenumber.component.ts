import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestOptions } from '@angular/http';
import { ProfileService } from '../profile.service';
// import swal from 'sweetalert2';

@Component({
  selector: 'app-phonenumber',
  templateUrl: './phonenumber.component.html',
  styleUrls: ['./phonenumber.component.css']
})
export class PhonenumberComponent implements OnInit {
  phonenumber;
  name = [];
  file;
  xyz = [];
  formData: any = new FormData();
  fileToUpload: Array<File> = [];
  data = {profile_photo:'',name:'',about:'',better_half:'',phone_num:''};
  wedding_id;
  value={name:'',better_half:''};
  headers;
  headers2;
  options;
  better_half;
  username;
  spinner = false;
  //public toastr: ToastrService,
  constructor(private http: HttpClient, private router: Router, private activatedroute: ActivatedRoute,private profile:ProfileService) { }
  ngOnInit(){
                const authToken = localStorage.getItem('userToken');
                this.headers = new HttpHeaders({'Authorization': authToken});
                this.options = new RequestOptions({headers: this.headers});
              
                this.name.push(JSON.parse(this.activatedroute.snapshot.params['data']));
                console.log(this.name);
                this.name.forEach(element => {
                
                                                this.value.name=element.name;
                                                this.value.better_half=element.better_half;
                                                //this.formData.append('about', element.name);
                                                this.formData.append('about', element.about);
                                                this.formData.append('better_half', element.better_half);
                });
                this.file=this.profile.getData();
                console.log("!!!",this.file);
  }

  sub(){
        this.spinner = true;
        const guest_invite = localStorage.getItem( 'guest_invites');        
        this.formData.append('phone_num', this.data.phone_num);
        this.formData.append('profile_photo', this.file);
        const authToken = localStorage.getItem('userToken');
        const headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', authToken);
        this.headers = new HttpHeaders({'Authorization': authToken});
        this.http.post('https://api.vowfest.com/updateProfile', this.formData, {headers: this.headers}).subscribe(
                    data => {
                                this.wedding_id = (<any>data).wedding_id;
                                localStorage.setItem('weddingID', this.wedding_id);
                                this.spinner = false ; 
                                this.router.navigate(['/dashboard/'])
                                },error => {    console.log(error);             
                                          }
        );

      

      }

}
