
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { RequestOptions } from '@angular/http';
import { catchError, map, tap } from 'rxjs/operators';
@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {
  ProfileArray = [];
  album = [];
  image = [];
  options;
  a;
  id = {album_id: ''};
  headers;
  imageUrl;
  fileToUpload: File = null;
  album_name;
  validate:boolean;
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
         ///for guest user 
         let user_login_type:any = {login_type:''} ;
         console.log(localStorage.getItem('userinfo'))
         user_login_type = JSON.parse(localStorage.getItem('userinfo') )
         user_login_type.login_type       = localStorage.getItem('login_type')
         if(user_login_type.login_type == 'guest'){
             this.validate = true;
             console.log( this.validate)
         }else{
             console.log( this.validate)
             this.validate = false;
         }            
         ///end
             
                const wedding_id1        = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
                const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
             
                this.headers = new HttpHeaders({'Authorization': authToken});
                this.options = new RequestOptions({headers: this.headers});
                // if (!authToken) {  this.router.navigate(['../login']);
                //  }
                console.log('WEDDINGID: ', wedding_id1);
                this.http.post('https://api.vowfest.com/albums/list', {wedding_id: wedding_id1} ).subscribe((data) => {
                  console.log('@@@@@@@@@@@@@@@@@@@@@@', data);
                  this.image.push(data);
                  this.image.forEach(element => {
                    console.log(element.albums_list);
                    this.album = element.albums_list;
                  });
                  console.log('####', this.album);
                },error=>{ 
                            console.log(error);
                            //this.router.navigate(['../error', JSON.stringify(error)]);
                          });
  }

  handleFileInput(file: FileList) {
    console.log("FILEEEEEEEEEEEEEEEEEEE:",(file.item(0)))
    this.fileToUpload = file.item(0);
      // Show image preview
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.imageUrl = event.target.result;
      };
      reader.readAsDataURL(this.fileToUpload);
  }

  onSubmit(f) {

   
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
 
               
                 
                    this.headers = new HttpHeaders({'Authorization': authToken});
                    this.options = new RequestOptions({headers: this.headers});
                      // f.value.item="--";
                    f.value.cover_photo = this.imageUrl;
                    console.log(f.value.album_name);
                    console.log(f)
                    console.log('@@@@@@@@@@@', this.imageUrl);
                    const body = new FormData();
                    body.append('album_name', f.value.album_name);
                    body.append('cover_photo', this.fileToUpload);

                    const header = new HttpHeaders({'Content-Type': 'application/json'});
                   this.http.post('https://api.vowfest.com/albums/create',
                   body, this.options).pipe(map(response => response))
                        .subscribe(result => {
                         //this.id.weddingID=localStorage.getItem('wedding_id');
                          this.id.album_id = (<any>result).album_id;
                         //  this.id.album_name=f.value.album_name;
                         console.log("@@@@@@@@@@@@@@@@",result);
                        console.log("@@@@@@@@@@@@@@@@",f.value);
                                           localStorage.setItem('albumid' , this.id.album_id ) 
                    this.router.navigate(['../images/', JSON.stringify(f.value  )]);
                  },error=>{  console.log(error);
                            // this.router.navigate(['../error', JSON.stringify(error)]);
                    });

  }
  
  rendertoImages(a) {
    console.log(a);
    this.router.navigate(['../images/', JSON.stringify(a)]);
  }
}
