
import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { ProfileService } from '../profile.service';
import { RegisterComponent } from '../register/register.component';
import { send } from 'q';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  ProfileArray: any;
  Gurl = 'https://api.vowfest.com/accounts/login/'; // GOOGLE BUTTON
  pro = {better_half: '', about: '',name:''};
  prof = {name: '', file: {}};
  imageUrl;
  name;
  data = [];
  headers;
  fileToUpload: File = null;
  userdata:{}
  constructor(private http: Http, private router: Router, private activatedroute: ActivatedRoute, private profile:ProfileService) { }
  ngOnInit() {
              if(this.activatedroute.snapshot.params['data']){
                    this.data.push(JSON.parse(this.activatedroute.snapshot.params['data']));
                    this.data.forEach(element => {
                    console.log('DATA:', element.name);
                    this.name = element.name               
                      });
                }else{  
                        console.log(JSON.parse(localStorage.getItem('userinfo')).name);
                        this.name = JSON.parse(localStorage.getItem('userinfo')).name ;
              }
               
               
  }
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);
      console.log('File to upload: ', this.fileToUpload);
      
      this.prof.file = this.fileToUpload;
      this.profile.setData(this.fileToUpload);
      // Show image preview
      const reader = new FileReader();
      reader.onload = (event: any) => {
                                         this.imageUrl = event.target.result;
                                         console.log( this.imageUrl );
      };
      reader.readAsDataURL(this.fileToUpload);
  }

  OnSubmit(Image) {
                    console.log(Image)
                    this.userdata = {
                      name: Image.value.name ,
                      better_half: Image.value.better_half,
                      profile_photo:  this.imageUrl 
                    };
    
                    localStorage.setItem('userinfo',JSON.stringify( this.userdata));
                    this.router.navigate(['/phonenumber', JSON.stringify(Image.value)]);
  }
}
