import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Headers , Http , Response } from '@angular/http';
import { Router ,ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 
  email_error;
  username_error;
  password1_error;
  value;
  a;
  name;
  email;
  password;
  invite_authtoken:any;
  constructor(private http: HttpClient, private router: Router, private activatedroute: ActivatedRoute) { }

  ngOnInit() {
    console.log(JSON.stringify(this.activatedroute.snapshot.params['data']));
  }
  onSubmit(f) {
                if(this.activatedroute.snapshot.params['data'] == undefined){
                  localStorage.setItem( 'guest_invites' , 'false' );
                    const header = new HttpHeaders({'Content-Type': 'application/json'});
                    
                    console.log(JSON.stringify(f.value))
                      this.http.post('https://api.vowfest.com/createAccount', JSON.stringify(f.value),{headers: header}).subscribe(( data )  => {
                        console.log('response: ', data);
                          this.a = data;
                          localStorage.setItem('userToken', this.a.token);
                          localStorage.setItem('weddingID', this.a.wedding_id);
                          this.router.navigate(['../profile/', JSON.stringify(f.value)]);
                          // alert('You are registered now');
                          },error => {
                               
                                this.email_error = error.error.email;
                                this.username_error = error.error.username;
                                this.password1_error = error.error.password1;
                                console.log(error);
                               // this.router.navigate(['../error', JSON.stringify(error)]);
                      });
                }
  }
  
}

