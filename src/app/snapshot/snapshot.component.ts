import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { CameraService } from '../camera.service';

@Component({
  selector: 'app-snapshot',
  templateUrl: './snapshot.component.html',
  styleUrls: ['./snapshot.component.css']
})
export class SnapshotComponent implements OnInit {
data=[];
cam={ base64: ''};
imageUrl;
fileToUpload: File = null;
image={image:''};
@ViewChild("canvas")
public canvas: ElementRef;
constructor(private route:Router,private http: HttpClient,private activatedroute: ActivatedRoute, private camera:CameraService) { }

  ngOnInit() { 
                console.log(this.camera.image)
                this.activatedroute.snapshot.data; 
                console.log(this.activatedroute.snapshot.params['image_data'])
                this.cam=this.camera.image;
                console.log(this.cam)
              }
  img(){
    this.route.navigate(['../add-stories' ])
  }
}
