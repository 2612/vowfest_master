import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestOptions } from '@angular/http';
import { CameraService } from '../camera.service';
import { DomSanitizer } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.css']
})
export class StoriesComponent implements OnInit {
  
  img = {};
  data = [];
  spinner = true;
  value;
  headers;
  options;
  count = 0;
  name;
  edata = {};
  imagePath = [ ];
  propic;
  id = {photo_id: ''};
  time;
  cam = {};
  eventdata:any = {event_list:[]};
  eventarray_length:number;
  data1 = {};
  data2 = [];
  wid;
  wedding_id={wedding_id:''};
  event_list={};
  better_half;
  profile_photo;
  num_likes;
  num_comments;
  viewwcomments:any;
  com:any[];

  image_data:any ={status: '' };
  image_data2:any;
  comment_div:boolean;
  comment = {num_likes:''};
  comment_like:any;
  No_event = false;
  public now: Date = new Date();

  constructor(private datePipe: DatePipe ,private _sanitizer: DomSanitizer,private http: HttpClient, private router: Router, private activatedroute: ActivatedRoute, private camera: CameraService) { 
    this.wid         = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
    const authToken  = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
    this.headers     = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});
    this.options     = new RequestOptions({headers: this.headers});
    //console.log(authToken)
     if(authToken  == null){ this.router.navigate(['../register']);}

    this.name          =  localStorage.getItem('userinfo') ? JSON.parse(localStorage.getItem('userinfo')).name : localStorage.getItem('guest_name') 
    this.better_half   =  localStorage.getItem('userinfo') ? JSON.parse(localStorage.getItem('userinfo')).better_half : "no Betterhalf yet";
    this.propic        =  JSON.parse(localStorage.getItem('userinfo')).profile_photo 
   
    this.wedding_id.wedding_id   = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
    this.geteventlist();
    this.getstory_list();
  }

  ngOnInit() {
    this.comment_div = false;
    this.now = new Date();
    console.log(this.now);
  }

  divid;
  slides: any = [[]];
  chunk(arr, chunkSize){
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }
  setlike(x){
    this.id.photo_id=x;
    this.http.post("https://api.vowfest.com/photo/like/add",this.id,this.options).subscribe((data)=>{
      this.getstory_list();
    },(error)=>{ 
                  console.log(error)
                 // this.router.navigate(['../error', JSON.stringify(error)]);
                })
  }
  add_comment(c){
    console.log(c.value);
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    const authToken = localStorage.getItem('userToken')  || localStorage.getItem('guest_invite_authtoken');
    header.append('Access-Control-Allow-Origin', '*');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    this.http.post("https://api.vowfest.com/photo/comment/add",c.value,{headers : this.headers}).subscribe((data)=>{
     console.log(data);
     this.getstory_list();
  },(error)=>{
                console.log(error)
              //  this.router.navigate(['../error', JSON.stringify(error)]);
              })
  }
  viewcomment(x){
  
    if( this.comment_div == false){
      this.comment_div = true;
    }else{
      this.comment_div = false;
    }
    this.divid = x;
    this.http.post('https://api.vowfest.com/photo/comment/list', {photo_id: x}).subscribe((data) => {
    this.viewwcomments = data;
    console.log(this.viewwcomments)
    this.com = this.viewwcomments.comments;
   
  },(error)=>{
                 console.log(error)
                // this.router.navigate(['../error', JSON.stringify(error)]);
              })
   
  }
  geteventlist(){
    this.wedding_id.wedding_id=localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');;
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    header.append('Access-Control-Allow-Origin', '*');

    this.http.post('https://api.vowfest.com/event/list',this.wedding_id,{headers : header}).subscribe(( data )  =>  
    {  
      this.eventdata = data;
      console.log(this.eventdata );
      if(this.eventdata.status == false){
        this.eventarray_length = 0;
      }else{
             this.eventarray_length = this.eventdata.event_list.length ;
             for (let i of this.eventdata.event_list) {
          
              if(i.date === this.datePipe.transform(this.now, 'yyyy-MM-dd') ){
                    this.No_event = false ;
                    console.log("false")
              }
              
              // else{ 
              //       this.No_event = true ;
              //       console.log("true")
              //       }
        }
       
    }  
    },(error) => {
                    console.log(error)
                    //this.router.navigate(['../error', JSON.stringify(error)]);
                  })
  }
  getstory_list(){
    this.wid = localStorage.getItem('weddingID') || localStorage.getItem('guest_wedding_id');
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    header.append('Access-Control-Allow-Origin', '*');
    this.http.post('https://api.vowfest.com/story/list', {'wedding_id': this.wid},{headers : header}).subscribe((data) => {
      console.log('DATA RECIEVED: ', data);
      this.image_data = data ;
      this.spinner = false;
        if( this.image_data.status == true){
                this.image_data2 =  this.image_data.stories_list;
                console.log( this.image_data2 );
                if(this.image_data2){
                  for (const comment_array of this.image_data2) {
                        
                    console.log(  comment_array.comments.length );
                      if(comment_array.comments.length == 0){
                            this.comment_div = false ;
                          }else{
                            this.comment_div = true;
                          }
                  }
                }
        }
         
  },(error) => {
                  console.log(error)
                  //this.router.navigate(['../error', JSON.stringify(error)]);
                });


  }
  add_counts_of_like(x){
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    const authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');
    header.append('Access-Control-Allow-Origin', '*');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    this.http.post("https://api.vowfest.com/photo/comment/like/add",{	comment_id: x},{headers : this.headers}).subscribe((data)=>{
     console.log(data);
    this.getstory_list();
    //this.view_counts_of_like(x);
  },(error)=>{
                console.log(error)
              //  this.router.navigate(['../error', JSON.stringify(error)]);
              })

  }
  view_counts_of_like(x){
   
    const header = new HttpHeaders({'Content-Type': 'application/json'});
    const authToken = localStorage.getItem('userToken') || localStorage.getItem('guest_invite_authtoken');;
    header.append('Access-Control-Allow-Origin', '*');
    this.headers = new HttpHeaders({'Authorization': authToken, 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'});

    this.http.post("https://api.vowfest.com/photo/comment/like/list",{	comment_id: x},{headers : this.headers}).subscribe((data)=>{
     console.log(data);
     this.comment_like = data;
     this.comment.num_likes = this.comment_like.num_likes;
     console.log( this.comment.num_likes )
  },(error)=>{
              console.log(error)
              //this.router.navigate(['../error', JSON.stringify(error)]);
            })
  }
  signout(){
    localStorage.clear();
    this.router.navigate(['../login']);
  }
 
}